# Idée générale: 

* Organiseur de musiques / films **local**, avec éventuellement aspect "social" (2ème itération)
* Fouille dans les fichiers locaux
* Permet de tagger les fichiers (métadata)
* Gère les fichiers musicaux classiques (.mp3 et .wav)

# Features

## Première itération 

- [ ] Recherche de musiques
- [x] Ajout de dossier de stockage
- [ ] Playlists
- [ ] Gestion des favoris
- [ ] Metadata
- [x] Player de musique
- [ ] "Edition de musique" = recouper la musique, accélérer le tempo 

## Deuxième itération

* Ajout de films et livres
* Proposition de nouvelles musiques/films en fonction de ce que l'utilisateur apprécie
* Mise en réseau = "socialisation de l'outil" (like, commentaire, ...)

# But technique 

* Meilleures performances possibles, théoriquement un logiciel qui tourne à 60FPS sur un ordinateur moyen/faible
* Logiciel utilisable (petite réflexion nécéssaire au niveau de l'UX)
* Facilement extensible au niveau des médias stockés

