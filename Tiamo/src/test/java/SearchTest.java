import model.recherche.Filter;
import model.recherche.SearchFilter;
import model.recherche.SingleSearchManager;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class SearchTest {

    private SingleSearchManager searchManager;

    public SearchTest() {
        this.searchManager = SingleSearchManager.getInstance();
    }

    @Test
    public void FilterToSQL() {
        Filter f = new Filter("rise", new ArrayList<>(Arrays.asList(SearchFilter.TITRE)));
        Assert.assertEquals(String.join("\n",
                "SELECT m.id_musique FROM musique m",
                "WHERE m.libelle_musique LIKE '%rise%';"
                ),
                f.ToSQL()
        );

        Filter f_2 = new Filter("Orelsan", new ArrayList<>(Arrays.asList(SearchFilter.ARTISTE, SearchFilter.TITRE)));
        Assert.assertEquals(
                String.join("\n",
                        "SELECT m.id_musique FROM musique m",
                        "JOIN artiste ar ON ar.id_artiste = m.id_artiste",
                        "WHERE ar.nom_artiste LIKE '%Orelsan%'",
                        "OR m.libelle_musique LIKE '%Orelsan%';"),
                f_2.ToSQL()
        );
    }
}
