import model.FactoryMusic;
import model.Oeuvre;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FactoryMusicTest {

    @Test
    public void createOeuvreTest()
    {
        FactoryMusic factoryMusic = new FactoryMusic();
        factoryMusic.setFile(new File("src/test/resources/files/Robin Hustin x Tobimorrow - Light It Up (feat. Jex).mp3"));
        try
        {
            Oeuvre oeuvre = factoryMusic.createOeuvre(factoryMusic.getFile().getName(), 0);
        }
        catch (Exception exception)
        {
            if(exception.getMessage().contains("Musique non définie"))
            {
                fail("File est null dans la classe FactoryMusic");
            }
            else if (exception.getMessage().contains("Type de Musique non trouvée par rapport à l'extension du fichier"))
            {
                fail("Extension de fichier non connu");
            }
        }
    }
}
