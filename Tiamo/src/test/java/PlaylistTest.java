import model.Music;
import model.Oeuvre;
import model.Playlist;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class PlaylistTest {

    private Playlist playlist;
    private File file;

    public PlaylistTest() throws Exception {
        this.playlist = new Playlist("Test playlist");
        this.file = new File("src/test/resources/files/Robin Hustin x Tobimorrow - Light It Up (feat. Jex).mp3");
    }

    @Test
    public void addPlaylistOeuvreTest() throws Exception {
        Music music = new Music(file);
        this.playlist.addPlaylist(music);
        Assert.assertEquals("Test de musique présente dans la playlist", this.playlist.search(music), music);
    }

    @Test
    public void addPlaylistListOeuvreTest() throws Exception {
        this.playlist = new Playlist("Test playlist");
        Music music = new Music(file);
        List<Oeuvre> list = new ArrayList<>();
        list.add(music);
        this.playlist.addPlaylist(list);
        Assert.assertEquals("Test de musique présente dans la playlist", this.playlist.search(music), music);
    }

    @Test
    public void searchOeuvreInPlaylist() throws Exception {

        this.playlist = new Playlist("Test playlist");
        Music music = new Music(file);
        playlist.getCollection().add(music);
        Assert.assertEquals("Recherche de l'oeuvre dans la playlist", playlist.search(music), music);
    }

    @Test
    public void deleteOeuvrePlaylist() throws Exception {
        this.playlist = new Playlist("Test playlist");
        Music music = new Music(file);
        playlist.getCollection().add(music);
        playlist.deleteOeuvre(music);
        Assert.assertEquals("Oeuvre supprimée", playlist.search(music), null);
    }
}
