import model.AbstractFactory;
import model.FactoryMusic;
import model.ListeOeuvres;
import model.Music;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;

public class ListeOeuvresTest {

    private ListeOeuvres listeOeuvres;

    public ListeOeuvresTest() {
        AbstractFactory factoryMusic = new FactoryMusic();
        this.listeOeuvres = ListeOeuvres.getInstance();
    }

    private void init() {
        AbstractFactory factoryMusic = new FactoryMusic();
        this.listeOeuvres = ListeOeuvres.getInstance();
    }

    @Test
    public void createOeuvreTest() {
        init();
        File music = new File("src/test/resources/files/Robin Hustin x Tobimorrow - Light It Up (feat. Jex).mp3");

        try {
            listeOeuvres.createOeuvre(music.getName(), music, 0);
        } catch (Exception exception) {
            fail(exception.getMessage());
        }
        Assert.assertEquals("Le fichier n'est pas dans la liste d'oeuvre", true, listeOeuvres.searchOeuvreByFileName(music.getName())!=null);
    }

    @Test
    public void createOeuvreListTest() {
        init();
        List<File> listFiles = new ArrayList<File>();
        File music1 = new File("src/test/resources/files/Robin Hustin x Tobimorrow - Light It Up (feat. Jex).mp3");
        File music2 = new File("src/test/resources/files/Lost Sky - Dreams.mp3");
        listFiles.add(music1);
        listFiles.add(music2);

        List<Music> listMusic = new ArrayList<Music>();
        listMusic.add(new Music(music1));
        listMusic.add(new Music(music2));

        try {
            listeOeuvres.createOeuvre(music1.getName(), listFiles.get(0), 0);
        } catch (Exception exception) {
            fail(exception.getMessage());
        }
        Assert.assertEquals("Tous les fichiers ne sont pas dans la liste d'oeuvre", listeOeuvres.getOeuvreList().containsAll(listMusic), true);
    }

    @Test
    public void helloTest() {

    }
}
