package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.UserConnecte;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;
import java.util.ResourceBundle;

public class MenuPageController implements Initializable {

    @FXML
    public AnchorPane bar;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void handleClickOnSettings(MouseEvent mouseEvent) {

        try {
            BorderPane parent = (BorderPane) bar.getParent();
            parent.setCenter(FXMLLoader.load(getClass().getResource("/fxml/settings.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleClickOnAccount(MouseEvent mouseEvent) {

        try {
            BorderPane parent = (BorderPane) bar.getParent();
            UserConnecte userConnecte = UserConnecte.getInstance();
            try {
                userConnecte.getToken();
            }
            catch (ConnectException e) {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/fxml/login.fxml"));
                Scene scene = new Scene(fxmlLoader.load(), 600, 400);
                Stage stage = new Stage();
                stage.setTitle("Connexion");
                stage.setScene(scene);
                stage.showAndWait();
            }
            try {
                userConnecte.getToken();
                parent.setCenter(FXMLLoader.load(getClass().getResource("/fxml/account.fxml")));
            }
            catch(ConnectException e) {
                handleClickOnHome(mouseEvent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleClickOnHome(MouseEvent mouseEvent) {
        try {
            BorderPane parent = (BorderPane) bar.getParent();
            parent.setCenter(FXMLLoader.load(getClass().getResource("/fxml/list_music.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleClickOnFavoris(MouseEvent mouseEvent) {

        try {
            BorderPane parent = (BorderPane) bar.getParent();
            parent.setCenter(FXMLLoader.load(getClass().getResource("/fxml/favoris.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleClickOnPlaylist(MouseEvent mouseEvent) {

        try {
            BorderPane parent = (BorderPane) bar.getParent();
            parent.setCenter(FXMLLoader.load(getClass().getResource("/fxml/playlist.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
