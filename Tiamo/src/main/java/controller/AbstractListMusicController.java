package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;
import model.recherche.Filter;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractListMusicController implements Initializable{
        /**
         * Liste JavaFX de musiques
         */
        @FXML
        public TableView listmusics;

        /**
         * Colonne de titre
         */
        @FXML
        public TableColumn titleColumn;

        /**
         * Colonne d'auteur
         */
        @FXML
        public TableColumn authorColumn;

        /**
         * Colonne d'album
         */
        @FXML
        public TableColumn albumColumn;

        /**
         * Colonne de durée
         */
        @FXML
        public TableColumn durationColumn;

        /**
         * Colonne de durée
         */
        @FXML
        public TableColumn genreColumn;

        /**
         * Permet de gérer la musique
         */
        protected MusicPlayer mp;

    static ObservableList<Oeuvre> selectedOeuvre;

    protected URL url = null;


    /**
     * Liste des musiques
     */
    ArrayList<Oeuvre> list = new ArrayList<Oeuvre>();

    /**
     * Gère le double-click sur un élément de la liste
     *
     * @param mouseEvent Evènement généré
     */
    @FXML
    public void handleMouseDoubleClick(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2 && listmusics.getSelectionModel().getSelectedItem() != null) {
            Music test = (Music) listmusics.getSelectionModel().getSelectedItem();
            mp.setAutomaticReading(true);
            mp.setRandomReading(false);
            mp.play(test);
            System.out.println("clicked on " + listmusics.getSelectionModel().getSelectedItem());
        }
    }

    /**
     * Gère le double-click sur un élément de la liste
     *
     * @param mouseEvent Evènement généré
     */
    @FXML
    public void handleAddFavourite(MouseEvent mouseEvent) {
        ObservableList<Oeuvre> selectedFiles = listmusics.getSelectionModel().getSelectedItems();
        if(!selectedFiles.isEmpty()) {
            for (Oeuvre oeuvre : selectedFiles) {
                try {
                    Playlist.createOrGetFav().addPlaylist(oeuvre);
                    try {
                        Notification.Notifier.INSTANCE.notifySuccess("Succès", "Musique ajoutée aux favoris : " + oeuvre.getMetaData().getMusicName());
                    }catch(Exception e) {
                        Notification.Notifier.INSTANCE.notifySuccess("Succès", "Musique ajoutée aux favoris : " + oeuvre.getFile().getName());
                    }
                } catch (Exception e) {
                    try {
                        Notification.Notifier.INSTANCE.notifyError("Erreur", "La musique " + oeuvre.getMetaData().getMusicName() + " est déjà présente dans les favoris");
                    } catch (Exception e1) {
                        Notification.Notifier.INSTANCE.notifyError("Erreur", "Musique " + oeuvre.getFile().getName() + " est déjà présente dans les favoris");
                    }
                }
            }
        }
        else {
            Notification.Notifier.INSTANCE.notifyInfo("Information", "Selectionnez une ou plusieurs musiques pour ajouter aux favoris");
        }
    }

    @FXML
    public abstract void suppress();

    /**
     * Fonction de suppression de musique du controller FXML
     */
    public void modifyMusic() {
        if(!listmusics.getSelectionModel().getSelectedItems().isEmpty()) {
            if(listmusics.getSelectionModel().getSelectedItems().size() > 1) {
                Notification.Notifier.INSTANCE.notifyInfo("Information", "On ne peut modifier qu'une musique à la fois");
            }
            else {
                selectedOeuvre = listmusics.getSelectionModel().getSelectedItems();

                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(getClass().getResource("/fxml/modifyMusic.fxml"));
                    Scene scene = new Scene(fxmlLoader.load(), 462, 333);
                    Stage stage = new Stage();
                    stage.setTitle("Modification des données de la musique");
                    stage.setScene(scene);
                    stage.showAndWait();
                    initialize(url, null);

                } catch (IOException e) {
                    Logger logger = Logger.getLogger(getClass().getName());
                    logger.log(Level.SEVERE, "Failed to create new Window.", e);
                }
            }
        }
        else {
            Notification.Notifier.INSTANCE.notifyInfo("Information", "Selectionnez une musique la modifier");
        }
    }

    /**
     * Fonction d'ajout d'une musique à une playlist du controller FXML
     */
    public void handleLikeMusic() {
        if(!listmusics.getSelectionModel().getSelectedItems().isEmpty()) {
            selectedOeuvre = listmusics.getSelectionModel().getSelectedItems();
            for(Oeuvre oeuvre : selectedOeuvre) {
                try {
                    ResultSet resultSet = DB.getInstance().getStatement().executeQuery("select id_musique_server from musique where id_musique = " + Music.getIdMusicFromPathInBDD(oeuvre.getFile().getAbsolutePath()) + ";");
                    int idMusicServer = 0;
                    while(!resultSet.isClosed() && resultSet.next()) {
                        idMusicServer = resultSet.getInt("id_musique_server");
                    }
                    if(idMusicServer == 0) {
                        Notification.Notifier.INSTANCE.notifyInfo("Info", "Il faut autofill la musique pour pour la liker : " + oeuvre.getMetaData().getMusicName());
                        break;
                    }
                    if (oeuvre.getMetaData().isLiked()) {
                        CustomHttpRequest customHttpRequest = new CustomHttpRequest();
                        HashMap<String, String> headerHTTP = new HashMap<>();
                        try {
                            headerHTTP.put("Authorization", UserConnecte.getInstance().getToken());
                        }
                        catch(ConnectException e) {
                            Notification.Notifier.INSTANCE.notifyError("Erreur", "Vous n'est pas connecté !");
                        }
                        JSONObject jsonReturned = customHttpRequest.httpPost(APIEndPoint.UNLIKE_MUSIC, headerHTTP, idMusicServer);
                        if(jsonReturned.get("status").equals("success")){
                            oeuvre.getMetaData().setLiked(false);
                            try {
                                Notification.Notifier.INSTANCE.notifySuccess("Succès", "Like enlevé : " + oeuvre.getMetaData().getMusicName());
                            } catch (Exception e) {
                                Notification.Notifier.INSTANCE.notifySuccess("Succès", "Like enlevé : " + oeuvre.getFile().getName());
                            }
                        }
                        else
                        {
                            System.err.println("impossible de se connecter au server pour enlever le like à la musique : " + oeuvre.getFile().getAbsolutePath());
                            try {
                                Notification.Notifier.INSTANCE.notifyError("Erreur", "Impossible enlever un like pour la musique " + oeuvre.getMetaData().getMusicName());
                            } catch (Exception e1) {
                                Notification.Notifier.INSTANCE.notifyError("Erreur", "Impossible enlever un like pour la musique " + oeuvre.getFile().getName());
                            }
                        }
                    } else {
                        CustomHttpRequest customHttpRequest = new CustomHttpRequest();
                        HashMap<String, String> headerHTTP = new HashMap<>();
                        try {
                        headerHTTP.put("Authorization", UserConnecte.getInstance().getToken());
                        }
                        catch(ConnectException e) {
                            Notification.Notifier.INSTANCE.notifyError("Erreur", "Vous n'est pas connecté !");
                        }
                        JSONObject jsonReturned = customHttpRequest.httpPost(APIEndPoint.LIKE_MUSIC, headerHTTP, idMusicServer);
                        if(jsonReturned.get("status").equals("success")){
                            oeuvre.getMetaData().setLiked(true);
                            try {
                                Notification.Notifier.INSTANCE.notifySuccess("Succès", "Like ajouté : " + oeuvre.getMetaData().getMusicName());
                            }catch (Exception e) {
                                Notification.Notifier.INSTANCE.notifySuccess("Succès", "Like ajouté : " + oeuvre.getFile().getName());
                            }
                        }
                        else
                        {
                            System.err.println("impossible de se connecter au server pour ajouter le like à la musique : " + oeuvre.getFile().getAbsolutePath());
                            try {
                                Notification.Notifier.INSTANCE.notifyError("Erreur", "Impossible d'ajouter un like pour la musique " + oeuvre.getMetaData().getMusicName());
                            } catch (Exception e1) {
                                Notification.Notifier.INSTANCE.notifyError("Erreur", "Impossible d'ajouter un like pour la musique " + oeuvre.getFile().getName());
                            }
                        }
                    }
                } catch (Exception e) {
                    try {
                        Notification.Notifier.INSTANCE.notifyError("Erreur", "Impossible d'ajouter / enlever un like pour la musique " + oeuvre.getMetaData().getMusicName());
                    } catch (Exception e1) {
                        Notification.Notifier.INSTANCE.notifyError("Erreur", "Impossible d'ajouter / enlever un like pour la musique " + oeuvre.getFile().getName());
                    }
                    e.printStackTrace();
                }
            }
        }
        else {
            Notification.Notifier.INSTANCE.notifyInfo("Information", "Selectionnez une ou plusieurs musiques pour ajouter / enlever un like");
        }
    }

    /**
     * Fonction d'initialisation du controller FXML
     *
     * @param url            URL du document FXML
     * @param resourceBundle Etat des ressources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listmusics.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        titleColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Oeuvre, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Oeuvre, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                try {
                    return new SimpleStringProperty(p.getValue().getMetaData().getMusicName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return new SimpleStringProperty(p.getValue().getFile().getName());
            }
        });

        authorColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Oeuvre, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Oeuvre, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                try {
                    return new SimpleStringProperty(p.getValue().getMetaData().getArtistList().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    return new SimpleStringProperty("Pas d'information");
                }
            }
        });

        albumColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Oeuvre, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Oeuvre, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                try {
                    return new SimpleStringProperty(p.getValue().getMetaData().getAlbumList().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    return new SimpleStringProperty("Pas d'information");
                }
            }
        });

        durationColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Oeuvre, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Oeuvre, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                try {
                    int duree = p.getValue().getMetaData().getModifiedMusicDurationSeconds();
                    if(p.getValue().getMetaData().getMusicDurationSeconds() != p.getValue().getMetaData().getModifiedMusicDurationSeconds()) {
                        //Si jamais le temps de début ou fin de la musique a été modifié
                        return new SimpleStringProperty(BottomBarController.durationFormat(duree) + " ~");
                    }
                    else {
                        return new SimpleStringProperty(BottomBarController.durationFormat(duree));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return new SimpleStringProperty("Pas d'information");
                }
            }
        });

        genreColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Oeuvre, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Oeuvre, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                try {
                    String genreString = p.getValue().getMetaData().getMusicKindList().get(0).toString();

                    return new SimpleStringProperty(genreString);
                } catch (Exception e) {
                    return new SimpleStringProperty("");
                }
            }
        });
    }

    /**
     * A appeler quand on effectue un changement de filtre sur les données à afficher
     *
     * @param f Le filtre à appliquer
     */
    public void onSearch(Filter f) {
        listmusics.getItems().clear();
        try {
            ResultSet rs = DB.getInstance().getStatement().executeQuery(f.ToSQL());
            List<Oeuvre> lo = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt("id_musique");
                try {
                    lo.add(Music.searchMusicByIdInDB(id));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            this.setMusics(lo);
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Change la liste JavaFX d'oeuvres à afficher
     *
     * @param oeuvres Les nouvelles oeuvres à afficher
     */
    public void setMusics(List<Oeuvre> oeuvres) {
        ObservableList<Oeuvre> test = FXCollections.observableArrayList(oeuvres);
        listmusics.setItems(test);
    }

    public static ObservableList<Oeuvre> getSelectedOeuvre()
    {
        return selectedOeuvre;
    }



}


