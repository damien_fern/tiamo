package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import model.ListeOeuvres;
import model.Music;
import model.SearchOeuvres;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {

    @FXML
    public AnchorPane settingsView;

    @FXML
    public ListView listDir;

    @FXML
    public ListView listIgnoredMusics;

    @FXML
    public void suppress(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Supprimer ces dossiers de recherches");
        alert.setHeaderText("Êtes-vous sûr de vouloir supprimer ces dossiers de recherches de Tiamo ?");
        ObservableList<File> selectedFiles = listDir.getSelectionModel().getSelectedItems();
        String list = "Ces dossiers ne seront pas supprimés de votre ordinateur.\n\n Liste des dossiers : \n";
        for (File file :
                selectedFiles) {
            list += "- "+file.getAbsolutePath()+"\n";
        }
        alert.setContentText(list);


        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            SearchOeuvres.getInstance().removeSearchableDirUrl(selectedFiles);
            refreshListDir();
            System.out.println("File deleted!");
        } else if (option.get() == ButtonType.CANCEL) {
            System.out.println("Cancelled!");
        } else {
            System.out.println("-");
        }
    }

    @FXML
    public void suppressFromIgnoredList(){
        ObservableList<String> selectedIgnoredMusics = listIgnoredMusics.getSelectionModel().getSelectedItems();
        for(String path : selectedIgnoredMusics) {
            Music.removeFromIgnoredList(path);
            Notification.Notifier.INSTANCE.notifySuccess("Succès", "La musique n'est plus ignorée : " + path + " ");
        }
        refreshListIgnoredMusics();

    }

    @FXML
    public void addFolderRecherche(){
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Ajouter un dossier de recherche");
        File home = new File(System.getProperty("user.home"));
        chooser.setInitialDirectory(home);
        Stage stage = (Stage) settingsView.getScene().getWindow();
        File selectedDirectory = chooser.showDialog(stage);
        System.out.println(selectedDirectory.getAbsolutePath());
        SearchOeuvres.getInstance().addSearchableDirUrl(selectedDirectory);

        ListeOeuvres lo = ListeOeuvres.getInstance();

        refreshListDir();
    }

    private ObservableList<File> getRechercheList(){
        ArrayList<File> liste = (ArrayList<File>) SearchOeuvres.getInstance().getSearchableDirUrl();
        ObservableList<File> listDirForView = FXCollections.observableArrayList(liste);
        return listDirForView;
    }

    private void refreshListDir()
    {
        listDir.setItems(getRechercheList());
    }

    private void refreshListIgnoredMusics() {
        ArrayList<String> liste = Music.getListPathIgnoredMusics();
        ObservableList<String> listDirForView = FXCollections.observableArrayList(liste);
        this.listIgnoredMusics.setItems(listDirForView);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listDir.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        refreshListDir();
        listIgnoredMusics.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        refreshListIgnoredMusics();
    }
}
