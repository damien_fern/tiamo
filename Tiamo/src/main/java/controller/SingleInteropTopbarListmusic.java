package controller;

import javafx.fxml.Initializable;
import model.recherche.SingleSearchManager;

public class SingleInteropTopbarListmusic {
    private static SingleInteropTopbarListmusic ourInstance = new SingleInteropTopbarListmusic();

    public static SingleInteropTopbarListmusic getInstance() {
        return ourInstance;
    }

    private TopBarController topBarController;
    private AbstractListMusicController listMusicController;

    private SingleInteropTopbarListmusic() {
        this.topBarController = null;
        this.listMusicController = null;
    }

    public void setTopBarController(TopBarController tbc) {
        this.topBarController = tbc;
    }


    public void setListMusicController(AbstractListMusicController lmc) {
        this.listMusicController = lmc;
    }

    public void applyFilter() {
        SingleSearchManager ssm = SingleSearchManager.getInstance();
        if (this.listMusicController != null) {
            this.listMusicController.onSearch(ssm.GenerateFilter());
        }
    }
}
