package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Album;

import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class CreateAlbumController implements Initializable{

    @FXML
    public TextField nameFieldText;

    @FXML
    public DatePicker releaseDateDatePicker;

    @FXML
    public Button okButton;

    @FXML
    public Button cancelButton;

    @FXML
    public void validModif() {
        Album album = null;
        if(!"".equals(nameFieldText.getText())) {
            try {
                album = new Album(nameFieldText.getText());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(releaseDateDatePicker.getValue() != null && !"".equals(releaseDateDatePicker.getValue())) {
                try {
                    album.setReleaseDate(Date.from(releaseDateDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Notification.Notifier.INSTANCE.notifySuccess("Succès", "Album " + nameFieldText.getText() + " créé");
            Stage stage = (Stage) okButton.getScene().getWindow();
            stage.close();
            return;
        }
        Notification.Notifier.INSTANCE.notifyError("Erreur", "Le nom de l'album est obligatoire");
    }

    @FXML
    public void cancel() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Fonction d'initialisation du controller FXML
     *
     * @param url            URL du document FXML
     * @param resourceBundle Etat des ressources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.okButton.setOnMouseClicked(e -> this.validModif());
        this.cancelButton.setOnMouseClicked(e -> this.cancel());
    }
}


