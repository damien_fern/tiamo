package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import model.ListeOeuvres;
import model.MusicPlayer;
import model.Oeuvre;
import model.Playlist;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class PlaylistController extends AbstractListMusicController {

    /**
     * ComboBox de toutes les playlists
     */
    @FXML
    public ComboBox playlistComboBox;

    @FXML
    public TextField newPlaylistName;

//    @FXML
//    public ImageView addPlaylistButton;

    @FXML
    public ImageView deletePlaylistButton;

    Playlist selectedPlaylist = null;

    /**
     * Fonction de suppression de musique des favoris du controller FXML
     */
    @Override
    public void suppress() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        ObservableList<Oeuvre> selectedFiles = listmusics.getSelectionModel().getSelectedItems();
        boolean isPluriel = selectedFiles.size() > 1;
        alert.setTitle("Supprimer cette musique de Tiamo ?");
        alert.setHeaderText("Êtes-vous sûr de vouloir supprimer "+ (isPluriel ? "ces" : "cette") +" musique"+ (isPluriel ? "s" : "") +" de Tiamo ?\n" +
                "Elle"+ (isPluriel ? "s" : "") +" "+ (isPluriel ? "seront" : "sera") +" ignorée"+ (isPluriel ? "s" : "") +" lors des prochaines recherches dans les répertoires définis.\n" +
                "Vous pourrez toujours "+ (isPluriel ? "les " : "l'") +"enlever des musiques ignorées pour que la recherche "+ (isPluriel ? "les" : "la") +" trouve de nouveau.");
        String list = " "+ (isPluriel ? "Ces" : "Cette") +" musique"+ (isPluriel ? "s" : "") +" ne "+ (isPluriel ? "seront" : "sera") +" pas supprimée"+ (isPluriel ? "s" : "") +" de votre ordinateur.\n\n Liste des musiques : \n";
        for (Oeuvre oeuvre : selectedFiles) {
            try {
                list += "- "+oeuvre.getMetaData().getMusicName()+"\n";
            } catch (Exception e) {
                list += "- "+oeuvre.getFile().getName()+"\n";
            }
        }
        alert.setContentText(list);


        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            for(Oeuvre oeuvre : selectedFiles) {
                try {
                    selectedPlaylist.deleteOeuvre(oeuvre);
                } catch (Exception e) {}
            }
            try {
                listmusics.setItems(FXCollections.observableArrayList(selectedPlaylist.getCollection()));
            } catch (Exception e) {}
            System.out.println("File deleted!");
        } else if (option.get() == ButtonType.CANCEL) {
            System.out.println("Cancelled!");
        } else {
            System.out.println("-");
        }
    }


    @FXML
    public void displayTextFieldHideCombo() {
        playlistComboBox.setVisible(false);
        newPlaylistName.setVisible(true);
        newPlaylistName.requestFocus();
//        addPlaylistButton.setVisible(false);
        deletePlaylistButton.setVisible(false);
    }

    @FXML
    public void createNewPlaylist(javafx.scene.input.KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            try {
                ListeOeuvres.getInstance().createEmptyPlaylist(newPlaylistName.getText());
                Notification.Notifier.INSTANCE.notifySuccess("Succès", "Playlist " + newPlaylistName.getText() + " créée");
                playlistComboBox.getItems().add(newPlaylistName.getText());
                playlistComboBox.setValue(newPlaylistName.getText());
                playlistComboBox.setVisible(true);
                newPlaylistName.clear();
                newPlaylistName.setVisible(false);
//                addPlaylistButton.setVisible(true);
                deletePlaylistButton.setVisible(true);

            } catch (Exception e) {
                Notification.Notifier.INSTANCE.notifyError("Erreur", "Impossible de créer la playlist \"" + newPlaylistName.getText() + "\"");
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void deletePlaylist() {
        if(playlistComboBox.getValue() != null && !playlistComboBox.equals("")) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Suppression playlist");
            alert.setHeaderText("Voulez vous vraiment supprimer cette playlist ?");

            Optional<ButtonType> option = alert.showAndWait();
            if (option.get() == ButtonType.OK) {
                ListeOeuvres.getInstance().deletePlaylist((String) playlistComboBox.getValue());
                playlistComboBox.getItems().remove(playlistComboBox.getValue());
                if(!playlistComboBox.getItems().isEmpty()) {
                    playlistComboBox.setValue(playlistComboBox.getItems().get(0));
                }
                else
                {
                    playlistComboBox.setVisible(false);
                    deletePlaylistButton.setVisible(false);
                }
            } else if (option.get() == ButtonType.CANCEL) {
                System.out.println("Cancelled!");
            } else {
                System.out.println("-");
            }
        }
    }

    /**
     * Fonction pour jouer les musiques des favoris du controller FXML
     */
    @FXML
    public void playPlaylist() {
        try {
            if(!Playlist.createOrGetFav().getCollection().isEmpty()) {
                if (mp.isRandomReading()) {
                    mp.play(null);
                } else {
                    mp.play(Playlist.createOrGetFav().getCollection().get(0));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Fonction d'initialisation du controller FXML
     *
     * @param url            URL du document FXML
     * @param resourceBundle Etat des ressources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.url = url;
        SingleInteropTopbarListmusic.getInstance().setListMusicController(this);

        ListeOeuvres lo = ListeOeuvres.getInstance();
        lo.setOeuvreList(new ArrayList<Oeuvre>());

        playlistComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                //playlistComboBox.setItems(playlistComboBox.getItems().sorted());
                selectedPlaylist = ListeOeuvres.getInstance().searchPlaylistByName(t1);
                try {
                    list = (ArrayList) selectedPlaylist.getCollection();
                    ObservableList<Oeuvre> test = FXCollections.observableArrayList(list);
                    listmusics.setItems(test);
                    mp = MusicPlayer.getInstance();
                    mp.setReadingList(list);
                    parentInitialize(url, resourceBundle);
                } catch (Exception e) {
                    list = new ArrayList<>();
                    ObservableList<Oeuvre> test = FXCollections.observableArrayList(list);
                    listmusics.setItems(test);
                    mp = MusicPlayer.getInstance();
                    mp.setReadingList(list);
                    parentInitialize(url, resourceBundle);
                }
            }
        });

        if(!ListeOeuvres.getInstance().getPlaylistList().isEmpty()) {
            ListeOeuvres.getInstance().getPlaylistList().sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
            for(Playlist playlist : ListeOeuvres.getInstance().getPlaylistList()) {
                playlistComboBox.getItems().add(playlist.getName());
            }
            playlistComboBox.setValue(ListeOeuvres.getInstance().getPlaylistList().get(0).getName());
            playlistComboBox.setVisible(true);
            deletePlaylistButton.setVisible(true);
        }
        else {
            list = new ArrayList<>();
            ObservableList<Oeuvre> test = FXCollections.observableArrayList(list);
            super.listmusics.setItems(test);
            super.mp = MusicPlayer.getInstance();
            super.mp.setReadingList(list);

            super.initialize(url, resourceBundle);
        }
    }

    private void parentInitialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }
}
