package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.*;
import org.controlsfx.control.RangeSlider;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.*;

public class ModifyMetaController implements Initializable{

    private URL url = null;

    @FXML
    public Label musicNameLabel;

    @FXML
    public TextField libelleFieldText;

    @FXML
    public ComboBox typeCombo;

    @FXML
    public ComboBox artistCombo;

    @FXML
    public ComboBox albumCombo;

    @FXML
    public Button okButton;

    @FXML
    public Button cancelButton;

    @FXML
    public RangeSlider rangeSlider;

    @FXML
    public ImageView addArtistButton;

    @FXML
    public ImageView addAlbumButton;

    @FXML
    public ImageView deleteArtistButton;

    @FXML
    public ImageView deleteAlbumButton;

    @FXML
    public ImageView ModifyArtistButton;
    
    @FXML
    public ImageView ModifyAlbumButton;

    @FXML
    public Button autoFillButton;

    private static String artistName;
    private static String albumName;
    private Oeuvre oeuvreToModify;
    private int initialLowValue = 0;
    private int initialHightValue = 0;


    @FXML
    public void validModif() {
        if(!"".equals(libelleFieldText.getText())) {
            try {
                oeuvreToModify.getMetaData().setMusicName(libelleFieldText.getText());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(typeCombo.getValue() != null && !"".equals(typeCombo.getValue())) {
            try {
                for (EnumMusicKind emk : EnumMusicKind.values()) {
                    if (emk.toString().equals(typeCombo.getValue())) {
                        try {
                            oeuvreToModify.getMetaData().getMusicKindList().clear();
                            try {
                                oeuvreToModify.getMetaData().removeAllMusicKindInBDD();
                            }catch (Exception e) {}
                            oeuvreToModify.getMetaData().addMusicKind(emk);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                oeuvreToModify.getMetaData().removeMusicKind();
            } catch (Exception e) {
            }
        }
        if(artistCombo.getValue() != null &&!"".equals(artistCombo.getValue())) {
            try {
                oeuvreToModify.getMetaData().getArtistList().clear();
                oeuvreToModify.getMetaData().addArtist(Artist.createArtistFromDB((String) artistCombo.getValue()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                oeuvreToModify.getMetaData().removeArtist();
            } catch (Exception e) {}
        }
        if(albumCombo.getValue() != null && !"".equals(albumCombo.getValue())) {
            try {
                oeuvreToModify.getMetaData().getAlbumList().clear();
                oeuvreToModify.getMetaData().addAlbum(Album.createAlbumFromDB((String) albumCombo.getValue()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                oeuvreToModify.getMetaData().removeAlbum();
            } catch (Exception e) {
            }
        }
        if(initialLowValue != rangeSlider.getLowValue()) {
            try {
                oeuvreToModify.getMetaData().setStartAtInSeconds((int)rangeSlider.getLowValue());
                MusicPlayer.getInstance().stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(initialHightValue != rangeSlider.getHighValue()) {
            try {
                oeuvreToModify.getMetaData().setEndAtInSeconds((int) rangeSlider.getHighValue());
                MusicPlayer.getInstance().stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void cancel() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void createArtist() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/createArtist.fxml"));
            fxmlLoader.setController(new CreateArtistController());
            Scene scene = new Scene(fxmlLoader.load(), 462, 333);
            Stage stage = new Stage();
            stage.setTitle("Création d'un artiste");
            stage.setScene(scene);
            stage.showAndWait();
            artistName = fxmlLoader.<CreateArtistController>getController().nameFieldText.getText();
            initialize(url, null);

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }

    @FXML
    public void deleteAlbum() {
        if(albumCombo.getValue() != null &&!"".equals(albumCombo.getValue())) {
            try {
                Album.createAlbumFromDB((String) albumCombo.getValue()).deleteInDBIfExist();
                albumCombo.getItems().remove(albumCombo.getValue());
                albumCombo.setValue("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void createAlbum() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/createAlbum.fxml"));
            fxmlLoader.setController(new CreateAlbumController());
            Scene scene = new Scene(fxmlLoader.load(), 462, 244);
            Stage stage = new Stage();
            stage.setTitle("Création d'un album");
            stage.setScene(scene);
            stage.showAndWait();
            initialize(url, null);

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }

    @FXML
    public void deleteArtist() {
        if(artistCombo.getValue() != null &&!"".equals(artistCombo.getValue())) {
            try {
                Artist.createArtistFromDB((String) artistCombo.getValue()).deleteInDBIfExist();
                artistCombo.getItems().remove(artistCombo.getValue());
                artistCombo.setValue("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void modifyArtist() {
        if(this.artistCombo.getValue() == null || this.artistCombo.getValue().equals("")) {
            Notification.Notifier.INSTANCE.notifyError("Erreur", "Veuillez selectionner un artiste pour le modifier");
            return;
        }
        artistName = (String) this.artistCombo.getValue();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/createArtist.fxml"));
            fxmlLoader.setController(new ModifyArtistController());
            Scene scene = new Scene(fxmlLoader.load(), 462, 333);
            Stage stage = new Stage();
            stage.setTitle("Modification d'un artiste");
            stage.setScene(scene);
            stage.showAndWait();
            initialize(url, null);

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }

    @FXML
    public void modifyAlbum() {
        if(this.albumCombo.getValue() == null || this.albumCombo.getValue().equals("")) {
            Notification.Notifier.INSTANCE.notifyError("Erreur", "Veuillez selectionner un album pour le modifier");
            return;
        }
        albumName = (String) this.albumCombo.getValue();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/fxml/createAlbum.fxml"));
            fxmlLoader.setController(new ModifyAlbumController());
            Scene scene = new Scene(fxmlLoader.load(), 462, 333);
            Stage stage = new Stage();
            stage.setTitle("Modification d'un album");
            stage.setScene(scene);
            stage.showAndWait();
            initialize(url, null);

        } catch (IOException e) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }

    /**
     * Fonction d'initialisation du controller FXML
     *
     * @param url            URL du document FXML
     * @param resourceBundle Etat des ressources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.url = url;
        try {
            this.oeuvreToModify = AbstractListMusicController.getSelectedOeuvre().get(0);
            try {
                musicNameLabel.setText(oeuvreToModify.getMetaData().getMusicName());
            } catch (Exception e) {
                musicNameLabel.setText(oeuvreToModify.getFile().getName());
            }
            try {
                libelleFieldText.setText(oeuvreToModify.getMetaData().getMusicName());
            } catch (Exception e) {
                libelleFieldText.setText(oeuvreToModify.getFile().getName());
            }

            typeCombo.setVisibleRowCount(5);
            typeCombo.getItems().clear();
            typeCombo.getItems().add("");
            Arrays.stream(EnumMusicKind.values()).sorted().forEach(kind -> typeCombo.getItems().add(kind.toString()));
            if (!oeuvreToModify.getMetaData().getMusicKindList().isEmpty()) {
                typeCombo.setValue(oeuvreToModify.getMetaData().getMusicKindList().get(0).toString());
            }

            artistCombo.setVisibleRowCount(5);
            artistCombo.getItems().clear();
            artistCombo.getItems().add("");
            Artist.createAllArtistFromDB().stream().forEach(artist -> artistCombo.getItems().add(artist.getName()));
            if(artistName != null) {
                artistCombo.setValue(artistName);
            }
            else if (!oeuvreToModify.getMetaData().getArtistList().isEmpty()) {
                artistCombo.setValue(oeuvreToModify.getMetaData().getArtistList().get(0).getName());
            }
            albumCombo.setVisibleRowCount(5);
            albumCombo.getItems().clear();
            albumCombo.getItems().add("");
            Album.createAllAlbumFromDB().stream().forEach(album -> albumCombo.getItems().add(album.getName()));
            if(albumName != null) {
                albumCombo.setValue(albumName);
            }
            else if (!oeuvreToModify.getMetaData().getAlbumList().isEmpty()) {
                albumCombo.setValue(oeuvreToModify.getMetaData().getAlbumList().get(0).getName());
            }

            this.initialLowValue = this.oeuvreToModify.getMetaData().getStartAtInSeconds();
            this.initialHightValue = this.oeuvreToModify.getMetaData().getEndAtInSeconds();
            rangeSlider.setMax(this.oeuvreToModify.getMetaData().getMusicDurationSeconds());
            rangeSlider.setMin(0);
            rangeSlider.setHighValue(this.oeuvreToModify.getMetaData().getEndAtInSeconds());
            rangeSlider.setLowValue(this.oeuvreToModify.getMetaData().getStartAtInSeconds());
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void autoFillMusique()
    {
        String typeTiamo = null;
        String artisteTiamo= null;
        String albumTiamo= null;


        String titleTiamo = libelleFieldText.getText();
        if (typeCombo.getValue() != null) {
            typeTiamo = typeCombo.getValue().toString();
        }

        if (artistCombo.getValue() != null) {
            artisteTiamo = artistCombo.getValue().toString();
        }

        if (albumCombo.getValue() != null) {
            albumTiamo = albumCombo.getValue().toString();
        }
        JSONObject musiqueJson;
        try {
            int idMusicServer = getIdMusicByTitleFromServer(titleTiamo);
             musiqueJson = getMusicInformationFromServer(idMusicServer);
            DB.getInstance().getStatement().executeUpdate("UPDATE musique set id_musique_server = "+idMusicServer+" where id_musique="+Music.getIdMusicInBDD(musiqueJson.getString("titre")));
        } catch (ConnectException e) {
            Notification.Notifier.INSTANCE.notifyError("Erreur", "Il faut être connecté pour autofill");
            //e.printStackTrace();
            return;
        } catch (JSONException e) {
            Notification.Notifier.INSTANCE.notifyError("Erreur", "Cette musique n'est pas présente sur le serveur");
            //e.printStackTrace();
            return;
        } catch (Exception e) {
            Notification.Notifier.INSTANCE.notifyError("Erreur", "Musique non présente en BDD");
            e.printStackTrace();
            return;
        }


        //Remplissage titre
        if(musiqueJson.getString("titre") != null)
            libelleFieldText.setText(musiqueJson.getString("titre"));
        else
        {
            if (titleTiamo == null)
                libelleFieldText.setText("");
            else
                libelleFieldText.setText(titleTiamo);
        }

        //Remplissage Genre
        {
            for (EnumMusicKind emk : EnumMusicKind.values())
            {
                if (emk.toString().equals( ((JSONObject) musiqueJson.getJSONArray("genres").get(0)).getString("libelle") ))
                {
                    typeCombo.setValue(emk.toString());
                    break;
                }
            }
             if (typeCombo.getValue() == null)
            {
                if (typeTiamo == null)
                    typeCombo.setValue("");
                else
                    typeCombo.setValue(typeTiamo);
            }
        }


        //Remplissage artist
        String nameArtist = musiqueJson.getJSONObject("artiste").getString("libelle");
        Artist artist = new Artist(nameArtist);
        if(nameArtist != null)
            artistCombo.setValue(artist.getName());
        else
        {
            if (artisteTiamo == null)
                artistCombo.setValue("");
            else
                artistCombo.setValue(nameArtist);
        }

        //Remplissage Album
        String libelleAlbum = musiqueJson.getJSONObject("album").getString("libelle");
        Album album = new Album(libelleAlbum);

        if(libelleAlbum != null)
            albumCombo.setValue(album.getName());
        else
        {
            if (albumTiamo == null)
                albumCombo.setValue("");
            else
                albumCombo.setValue(libelleAlbum);
        }



    }

    public int getIdMusicByTitleFromServer(String title) throws ConnectException, JSONException {
        int idMusicServer = 0;
        CustomHttpRequest chr = new CustomHttpRequest();
        UserConnecte userConnecte = UserConnecte.getInstance();
        HashMap<String, String> headerHTTP = new HashMap<>();
        headerHTTP.put("Authorization", userConnecte.getToken());
        JSONObject jsonReturned = chr.httpGet(APIEndPoint.GET_MUSIC_ID, headerHTTP, title);
        idMusicServer = (int)jsonReturned.getInt("id");

//        JSONObject retour = new JSONObject("{\"id\":1,\"music\":{\"album\":{\"id\":1,\"libelle\":\"JOYCA - Reprise\"},\"artiste\":{\"id\":1,\"libelle\":\"JOYCA\"},\"genres\":[{\"id\":20,\"libelle\":\"Electro Clubbing\"}],\"titre\":\"niska - Parodie\"}}");

//        idMusicServer = (int)retour.getInt("id");


        return idMusicServer;
    }

    public JSONObject getMusicInformationFromServer(int idMusic) throws ConnectException {
        CustomHttpRequest chr = new CustomHttpRequest();
        UserConnecte userConnecte = UserConnecte.getInstance();
        HashMap<String, String> headerHTTP = new HashMap<>();
        headerHTTP.put("Authorization", userConnecte.getToken());
        JSONObject jsonReturned = chr.httpGet(APIEndPoint.GET_MUSIC_INFO, headerHTTP, idMusic);

        return jsonReturned;
    }

    public static String getArtistName() {
        return artistName;
    }

    public static String getAlbumName() {
        return albumName;
    }
}


