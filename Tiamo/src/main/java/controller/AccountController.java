package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.ConnectException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class AccountController implements Initializable {


    @FXML
    public Label labelLogin;

    private User userToShow;

    @FXML
    public SplitPane accountFX;

    /**
     * Colonne de titre
     */
    @FXML
    public TableColumn<User, String> tableColumnFriendId;

    @FXML
    public TableColumn<User, String> tableColumnFriendUsername;

    @FXML
    public Label labelDate;

    @FXML
    public TextField addFriendTextField;

    @FXML
    public TableView<User> tableViewFriends;

    @FXML
    public TableView<String> tableViewLikes;

    @FXML
    public TableColumn<String, String> tableColumnMusicTitle;

    @FXML
    public void handleMouseDoubleClick(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2 && tableViewFriends.getSelectionModel().getSelectedItem() != null) {
            User userchanged = new User();
            userchanged.setId(tableViewFriends.getSelectionModel().getSelectedItem().getId());
            userToShow = userchanged;
            initialize(null, null);
        }
    }

    public void init()
    {
        if(userToShow == null)
        {
            userToShow = UserConnecte.getInstance();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        CustomHttpRequest customHttpRequest = new CustomHttpRequest();
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            hashMap.put("Authorization", UserConnecte.getInstance().getToken());
        } catch (ConnectException e) {
        }
        APIEndPoint USER = APIEndPoint.GET_USER;
        JSONObject jsonObject;
        init();
        if(userToShow.getClass().equals(UserConnecte.class))
        {
            jsonObject = customHttpRequest.httpGet(USER, hashMap);
        }
        else
        {
            jsonObject = customHttpRequest.httpGet(USER, hashMap, ""+userToShow.getId());
        }
        String stringFriends = customHttpRequest.httpGetNoJson(APIEndPoint.GET_FRIENDSHIPS, hashMap);

        labelLogin.setText(jsonObject.getString("libelle_user"));
        labelDate.setText(jsonObject.getString("date_creation"));

        ArrayList<User> friends = new ArrayList<>();

        try {
            JSONArray listFriends = new JSONArray(stringFriends);
            for (int i = 0; i < listFriends.length(); i++) {
                JSONObject userJson = listFriends.getJSONObject(i);
                User user = new User();
                user.setId((int)userJson.get("id"));
                user.setUsername(userJson.get("libelle").toString());
                friends.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ObservableList<User> test = FXCollections.observableArrayList(friends);

        ArrayList<String> likeArray = new ArrayList<>();
        Object[] arrayList = jsonObject.getJSONArray("musics_liked").toList().toArray();
        for (Object object :
                arrayList) {
            HashMap testing = (HashMap) object;
            likeArray.add((String) testing.get("libelle"));
        }
        ObservableList<String> likes = FXCollections.observableArrayList(likeArray);
        tableViewLikes.setItems(likes);
        tableViewFriends.setItems(test);

        tableColumnFriendId.setCellValueFactory(p -> {
            // p.getValue() returns the Person instance for a particular TableView row
            try {
                return new SimpleStringProperty(""+p.getValue().getId());
            } catch (Exception e) {
                e.printStackTrace();
                return new SimpleStringProperty("Pas d'information");
            }
        });
        tableColumnFriendUsername.setCellValueFactory(p -> {
            // p.getValue() returns the Person instance for a particular TableView row
            try {
                return new SimpleStringProperty(""+p.getValue().getUsername());
            } catch (Exception e) {
                e.printStackTrace();
                return new SimpleStringProperty("Pas d'information");
            }
        });

        tableColumnMusicTitle.setCellValueFactory(p -> {
            // p.getValue() returns the Person instance for a particular TableView row
            try {
                return new SimpleStringProperty(p.getValue());
            } catch (Exception e) {
                e.printStackTrace();
                return new SimpleStringProperty("Pas d'information");
            }
        });


        System.out.println(jsonObject);
    }

    public void addFriend(MouseEvent mouseEvent) {
        String loginFriend = addFriendTextField.getText();
        CustomHttpRequest customHttpRequest = new CustomHttpRequest();
        HashMap<String, String> map = new HashMap<>();
        try {
            map.put("Authorization", UserConnecte.getInstance().getToken());
        } catch (ConnectException e) {
        }
        JSONObject jsonReturned = customHttpRequest.httpPost(APIEndPoint.NEW_FRIENDSHIP, map, loginFriend);
        userToShow = null;
        Notification.Notifier.INSTANCE.notifySuccess("Succès", "Vous avez ajouté un ami.");
        initialize(null,null);
    }

    public void disconnection(MouseEvent mouseEvent) {
        try {
            UserConnecte.getInstance().removeTokenAndDate();
            BorderPane parent = (BorderPane) accountFX.getParent();
            parent.setCenter(FXMLLoader.load(getClass().getResource("/fxml/list_music.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
