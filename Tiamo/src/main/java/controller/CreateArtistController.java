package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Artist;

import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class CreateArtistController implements Initializable{

    @FXML
    public TextField nameFieldText;

    @FXML
    public DatePicker birthdateDatePicker;

    @FXML
    public TextField countryFieldText;

    @FXML
    public TextField musicGroupFieldText;

    @FXML
    public Button okButton;

    @FXML
    public Button cancelButton;

    @FXML
    public void validModif() {
        Artist artist = null;
        if(!"".equals(nameFieldText.getText())) {
            try {
                artist = new Artist(nameFieldText.getText());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(birthdateDatePicker.getValue() != null && !"".equals(birthdateDatePicker.getValue())) {
                try {
                    artist.setDateOfBirth(Date.from(birthdateDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(countryFieldText.getText() != null &&!"".equals(countryFieldText.getText())) {
                try {
                    artist.setCountry(countryFieldText.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(musicGroupFieldText.getText() != null && !"".equals(musicGroupFieldText.getText())) {
                try {
                    artist.setMusicGroup(musicGroupFieldText.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Notification.Notifier.INSTANCE.notifySuccess("Succès", "Artiste " + nameFieldText.getText() + " créé");
            Stage stage = (Stage) okButton.getScene().getWindow();
            stage.close();
            return;
        }
        Notification.Notifier.INSTANCE.notifyError("Erreur", "Le nom de  l'artiste est obligatoire");
    }

    @FXML
    public void cancel() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Fonction d'initialisation du controller FXML
     *
     * @param url            URL du document FXML
     * @param resourceBundle Etat des ressources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.okButton.setOnMouseClicked(e -> this.validModif());
        this.cancelButton.setOnMouseClicked(e -> this.cancel());
    }
}


