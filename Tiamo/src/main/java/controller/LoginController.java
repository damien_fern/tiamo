package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.APIEndPoint;
import model.CustomHttpRequest;
import model.UserConnecte;
import org.json.JSONObject;

import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML
    public TextField loginTextField;
    @FXML
    public TextField passwdTextField;

    public void createUser(MouseEvent mouseEvent)
    {
        String login = this.loginTextField.getText();
        String passwd = this.passwdTextField.getText();
        CustomHttpRequest customHttpRequest = new CustomHttpRequest();
        HashMap<String, String> headerHTTP = new HashMap<>();
        headerHTTP.put("User", login);
        headerHTTP.put("Pass", passwd);
        JSONObject jsonReturned = customHttpRequest.httpPost(APIEndPoint.CREATE_USER, headerHTTP);
        if(jsonReturned.get("status") != null && jsonReturned.get("status").equals("success")){
            String token = jsonReturned.get("token").toString();
            UserConnecte currentUser = UserConnecte.getInstance();
            currentUser.updateToken(token);
            currentUser.setUsername(login);
            currentUser.setDateCreation(new Date());
            Notification.Notifier.INSTANCE.notifySuccess("Succès", "Votre compte a été crée avec succès et vous êtes maintenant connecté.");
            closeStage();
        }
        else
        {
            Notification.Notifier.INSTANCE.notifyError("Error", "Error");
        }
    }

    public void connect(MouseEvent mouseEvent)
    {
        String login = this.loginTextField.getText();
        String passwd = this.passwdTextField.getText();
        CustomHttpRequest customHttpRequest = new CustomHttpRequest();
        HashMap<String, String> headerHTTP = new HashMap<>();
        headerHTTP.put("User", login);
        headerHTTP.put("Pass", passwd);
        JSONObject jsonReturned = customHttpRequest.httpGet(APIEndPoint.CREATE_TOKEN, headerHTTP);
        if((int) jsonReturned.get("code") >= 200 && (int) jsonReturned.get("code") <= 299 ){
            String token = jsonReturned.get("token").toString();
            UserConnecte currentUser = UserConnecte.getInstance();
            currentUser.updateToken(token);
            currentUser.setUsername(login);
            Notification.Notifier.INSTANCE.notifySuccess("Succès", "Vous êtes maintenant connecté.");
            closeStage();
        }
        else
        {
            Notification.Notifier.INSTANCE.notifyError("Error", "Error");
        }
    }

    private void closeStage(){
        Stage stage = (Stage) loginTextField.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
