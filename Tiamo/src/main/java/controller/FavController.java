package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import model.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class FavController extends AbstractListMusicController {

    /**
     * Fonction de suppression de musique des favoris du controller FXML
     */
    @Override
    public void suppress() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        ObservableList<Oeuvre> selectedFiles = listmusics.getSelectionModel().getSelectedItems();
        boolean isPluriel = selectedFiles.size() > 1;
        alert.setTitle("Supprimer cette musique de Tiamo ?");
        alert.setHeaderText("Êtes-vous sûr de vouloir supprimer "+ (isPluriel ? "ces" : "cette") +" musique"+ (isPluriel ? "s" : "") +" de Tiamo ?\n" +
                "Elle"+ (isPluriel ? "s" : "") +" "+ (isPluriel ? "seront" : "sera") +" ignorée"+ (isPluriel ? "s" : "") +" lors des prochaines recherches dans les répertoires définis.\n" +
                "Vous pourrez toujours "+ (isPluriel ? "les " : "l'") +"enlever des musiques ignorées pour que la recherche "+ (isPluriel ? "les" : "la") +" trouve de nouveau.");
        String list = " "+ (isPluriel ? "Ces" : "Cette") +" musique"+ (isPluriel ? "s" : "") +" ne "+ (isPluriel ? "seront" : "sera") +" pas supprimée"+ (isPluriel ? "s" : "") +" de votre ordinateur.\n\n Liste des musiques : \n";
        for (Oeuvre oeuvre : selectedFiles) {
            try {
                list += "- "+oeuvre.getMetaData().getMusicName()+"\n";
            } catch (Exception e) {
                list += "- "+oeuvre.getFile().getName()+"\n";
            }
        }
        alert.setContentText(list);


        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            for(Oeuvre oeuvre : selectedFiles) {
                try {
                    Playlist.createOrGetFav().deleteOeuvre(oeuvre);
                } catch (Exception e) {}
            }
            try {
                listmusics.setItems(FXCollections.observableArrayList(Playlist.createOrGetFav().getCollection()));
            } catch (Exception e) {}
            System.out.println("File deleted!");
        } else if (option.get() == ButtonType.CANCEL) {
            System.out.println("Cancelled!");
        } else {
            System.out.println("-");
        }
    }

    /**
     * Fonction pour jouer les musiques des favoris du controller FXML
     */
    @FXML
    public void playPlaylist() {
        try {
            if(!Playlist.createOrGetFav().getCollection().isEmpty()) {
                if (mp.isRandomReading()) {
                    mp.play(null);
                } else {
                    mp.play(Playlist.createOrGetFav().getCollection().get(0));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Fonction d'initialisation du controller FXML
     *
     * @param url            URL du document FXML
     * @param resourceBundle Etat des ressources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.url = url;
        SingleInteropTopbarListmusic.getInstance().setListMusicController(this);
        try {
            ListeOeuvres lo = ListeOeuvres.getInstance();
            lo.setOeuvreList(new ArrayList<Oeuvre>());
            list = (ArrayList) Playlist.createOrGetFav().getCollection();

        } catch (Exception e) {
            e.printStackTrace();
        }

        ObservableList<Oeuvre> test = FXCollections.observableArrayList(list);
        super.listmusics.setItems(test);
        super.mp = MusicPlayer.getInstance();
        super.mp.setReadingList(list);

        super.initialize(url, resourceBundle);
    }
}
