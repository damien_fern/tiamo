package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Artist;

import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class ModifyArtistController implements Initializable{
    @FXML
    public TextField nameFieldText;

    @FXML
    public DatePicker birthdateDatePicker;

    @FXML
    public TextField countryFieldText;

    @FXML
    public TextField musicGroupFieldText;

    @FXML
    public Button okButton;

    @FXML
    public Button cancelButton;

    @FXML
    public Label nameLabel;

    private Artist artist;

    @FXML
    public void validModif() {
        if(!"".equals(nameFieldText.getText())) {
            if(birthdateDatePicker.getValue() != null && !"".equals(birthdateDatePicker.getValue())) {
                try {
                    artist.setDateOfBirth(Date.from(birthdateDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(countryFieldText.getText() != null &&!"".equals(countryFieldText.getText())) {
                try {
                    artist.setCountry(countryFieldText.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(musicGroupFieldText.getText() != null && !"".equals(musicGroupFieldText.getText())) {
                try {
                    artist.setMusicGroup(musicGroupFieldText.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                artist.setName(nameFieldText.getText());
            } catch (Exception e) {
                e.printStackTrace();
            }
            Notification.Notifier.INSTANCE.notifySuccess("Succès", "Artiste " + nameFieldText.getText() + " créé");
            Stage stage = (Stage) okButton.getScene().getWindow();
            stage.close();
            return;
        }
        Notification.Notifier.INSTANCE.notifyError("Erreur", "Le nom de  l'artiste est obligatoire");
    }

    @FXML
    public void cancel() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

        /**
         * Fonction d'initialisation du controller FXML
         *
         * @param url            URL du document FXML
         * @param resourceBundle Etat des ressources
         */
        @Override
        public void initialize(URL url, ResourceBundle resourceBundle) {
            nameLabel.setText("Modif Artist");
            try {
                this.artist = Artist.createArtistFromDB(ModifyMetaController.getArtistName());
            } catch (Exception e) {
                this.artist = null;
                return;
            }
                this.nameFieldText.setText(artist.getName());
                if(artist.getDateOfBirth() != null) {
                    this.birthdateDatePicker.setValue(artist.getDateOfBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                }
                if(artist.getCountry() != null) {
                    this.countryFieldText.setText(artist.getCountry());
                }
                if(artist.getMusicGroup() != null) {
                    this.musicGroupFieldText.setText(artist.getMusicGroup());
                }

            this.okButton.setOnMouseClicked(e -> this.validModif());
            this.cancelButton.setOnMouseClicked(e -> this.cancel());
        }
}
