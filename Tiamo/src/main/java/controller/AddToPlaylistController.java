package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AddToPlaylistController implements Initializable{
    /**
     * Liste JavaFX de musiques
     */
    @FXML
    public ComboBox playlistComboBox;

    @FXML
    public Button closeButton;

    /**
     * Permet de gérer la musique
     */
    protected MusicPlayer mp;

    Playlist selectedPlaylist = null;


    /**
     * Fonction d'initialisation du controller FXML
     *
     * @param url            URL du document FXML
     * @param resourceBundle Etat des ressources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        if(!ListeOeuvres.getInstance().getPlaylistList().isEmpty()) {
            ListeOeuvres.getInstance().getPlaylistList().sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
            playlistComboBox.setVisibleRowCount(3);
            for(Playlist playlist : ListeOeuvres.getInstance().getPlaylistList()) {
                playlistComboBox.getItems().add(playlist.getName());
            }
        }

        playlistComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                selectedPlaylist = ListeOeuvres.getInstance().searchPlaylistByName(t1);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/list_music.fxml"));
                try {
                    Parent root = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                AllMusicController searchPanelController = loader.getController();//Get access to the Controller

                ObservableList<Oeuvre> oeuvreList = searchPanelController.getSelectedOeuvre();
                for(Oeuvre oeuvre : oeuvreList) {
                    try {
                        selectedPlaylist.addPlaylist(oeuvre);
                        Notification.Notifier.INSTANCE.notifySuccess("Succès", "Musique " + oeuvre.getMetaData().getMusicName() + " ajoutée avec succès à la playlist " + selectedPlaylist.getName());
                    } catch (Exception e) {
                        try {
                            Notification.Notifier.INSTANCE.notifyError("Erreur", "La musique " + oeuvre.getMetaData().getMusicName() + " est déjà présente dans la playlist " + selectedPlaylist.getName());
                        } catch (Exception e1) {
                            Notification.Notifier.INSTANCE.notifyError("Erreur", "Musique " + oeuvre.getFile().getName() + " est déjà présente dans la playlist " + selectedPlaylist.getName());
                        }
                        e.printStackTrace();
                    }
                }

                Stage stage = (Stage) closeButton.getScene().getWindow();
                stage.close();
            }
        });
    }
}