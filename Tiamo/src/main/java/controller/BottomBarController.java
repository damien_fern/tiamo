package controller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import model.*;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class BottomBarController implements Initializable {

    private MusicPlayer player;

    @FXML
    public Label currentSonglabel;
    @FXML
    public Label currentSongDurationLabel;
    @FXML
    public Label currentSongTimeLabel; // TODO : AJOUTER LABEL EN COURS ET DUREE DE LA MUSIQUE DANS LE FXML @Ben
    @FXML
    public ImageView playButton;
    @FXML
    public Slider slider;

    private Timer timer;

    public void tooglePlayCurrent() {
        player.togglePlayPauseCurrent();
    }

    private Oeuvre current;

    public BottomBarController() {
        try {
            current = null;
            player = MusicPlayer.getInstance();
            player.addPropertyChangeListener(new PropertyChangeListener() {
                public void propertyChange(PropertyChangeEvent event) {
                    Oeuvre oeuvre = (Oeuvre) event.getNewValue();
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                if(event.getOldValue() == PlayerState.PLAY || event.getOldValue() == PlayerState.RESUME) {
                                    try {
                                        current = oeuvre;
                                        currentSonglabel.setText(oeuvre.getMetaData().getMusicName());
                                        if (event.getOldValue() == PlayerState.PLAY) {
                                            initSlider();
                                        }
                                        switchAffichagePLayPauseButton(player.getFileState());
                                    } catch (Exception e) {
                                        currentSonglabel.setText("Erreur : récupération du nom de fichier");
                                        e.printStackTrace();
                                    }
                                }
                                else if(event.getOldValue() == PlayerState.PAUSE) {
                                    switchAffichagePLayPauseButton(player.getFileState());
                                }
                                else if(event.getOldValue() == PlayerState.STOP) {
                                    switchAffichagePLayPauseButton(player.getFileState());
                                    try {
                                        initSlider();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    currentSongTimeLabel.setText("0m0s");
                                }
                                else {
                                    System.err.println("Mode de lecture inconnu");
                                }
                            }
                        });
                }
            });
        } catch (Exception exception) {
            System.err.println(exception.getMessage());
        }
    }

    private void initSlider() throws Exception {
        Oeuvre currentOeuvre = player.getCurrentOeuvre();
        stopSlider();
        slider.setValue(0);
        slider.setMax(currentOeuvre.getMetaData().getModifiedMusicDurationSeconds());
        currentSongDurationLabel.setText(durationFormat((int) slider.getMax()));
    }

    private void stopSlider(){
        try
        {
            timer.cancel();
        }
        catch (IllegalStateException exception)
        {
            System.err.println(exception.getMessage());
        }
        catch (NullPointerException nullException){

        }
    }

    private void playSlider(){
        timer = new Timer(true);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                slider.increment();
                Platform.runLater(() -> {
                    currentSongTimeLabel.setText(durationFormat((int)slider.getValue()));
                });
            }
        }, 0, 1000);
    }

    public void previousSong() {
        player.playPreviusOeuvre();
    }

    public void nextSong() {
        player.playNextOeuvre();
    }

    public void switchAffichagePLayPauseButton(PlayerState status) {
        try {
            FileInputStream inputstream;
            if (status.equals(PlayerState.PLAY) || status.equals(PlayerState.RESUME)) {
                playSlider();
                inputstream = new FileInputStream("Tiamo/src/main/resources/images/pause.png");
            } else {
                stopSlider();
                inputstream = new FileInputStream("Tiamo/src/main/resources/images/play.png");
            }
            Image pause = new Image(inputstream);
            playButton.setImage(pause);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        slider.setBlockIncrement(1);
        currentSonglabel.setText("Aucune musique en écoute");
        currentSongTimeLabel.setText("0m0s");
        slider.setOnMouseReleased((MouseEvent event) -> {
            player.goTo((int)slider.getValue());
        });
        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                if((double)newValue >= slider.getMax()) {
                    try {
                        player.stopMusicPlayer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public static String durationFormat(int duree)
    {
        int numberOfHours = (duree % 86400 ) / 3600 ;
        int numberOfMinutes = ((duree % 86400 ) % 3600 ) / 60;
        int numberOfSeconds = ((duree % 86400 ) % 3600 ) % 60;
        String dureeString = "";
        dureeString += (numberOfHours > 1 ? numberOfHours+"h" : "");
        dureeString += numberOfMinutes+"m"+numberOfSeconds+"s";

        return dureeString;
    }

}
