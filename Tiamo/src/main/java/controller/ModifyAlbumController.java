package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Album;

import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class ModifyAlbumController implements Initializable{

    @FXML
    public TextField nameFieldText;

    @FXML
    public DatePicker releasedateDatePicker;

    @FXML
    public Button okButton;

    @FXML
    public Button cancelButton;

    @FXML
    public Label nameLabel;

    private Album album;

    @FXML
    public void validModif() {
        if(!"".equals(nameFieldText.getText())) {
            if(releasedateDatePicker.getValue() != null && !"".equals(releasedateDatePicker.getValue())) {
                try {
                    album.setReleaseDate(Date.from(releasedateDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                album.setName(nameFieldText.getText());
            } catch (Exception e) {
                e.printStackTrace();
            }
            Notification.Notifier.INSTANCE.notifySuccess("Succès", "Album " + nameFieldText.getText() + " créé");
            Stage stage = (Stage) okButton.getScene().getWindow();
            stage.close();
            return;
        }
        Notification.Notifier.INSTANCE.notifyError("Erreur", "Le nom de  l'album est obligatoire");
    }

    @FXML
    public void cancel() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

        /**
         * Fonction d'initialisation du controller FXML
         *
         * @param url            URL du document FXML
         * @param resourceBundle Etat des ressources
         */
        @Override
        public void initialize(URL url, ResourceBundle resourceBundle) {
            nameLabel.setText("Modif Album");
            try {
                this.album = Album.createAlbumFromDB(ModifyMetaController.getAlbumName());
            } catch (Exception e) {
                this.album = null;
                return;
            }
            this.nameFieldText.setText(album.getName());
            if(album.getReleaseDate() != null) {
                this.releasedateDatePicker.setValue(album.getReleaseDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            }

            this.okButton.setOnMouseClicked(e -> this.validModif());
            this.cancelButton.setOnMouseClicked(e -> this.cancel());
        }
}
