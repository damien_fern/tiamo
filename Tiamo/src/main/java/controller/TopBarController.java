package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import model.recherche.SearchFilter;
import model.recherche.SingleSearchManager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TopBarController implements Initializable {

    /**
     * Le contrôleur "topbar"
     */
    @FXML
    public HBox topBar;

    /**
     * Bouton de filtrage par titre
     */
    @FXML
    public Button btnFilterTitre;

    /**
     * Bouton de filtrage par album
     */
    @FXML
    public Button btnFilterAlbum;

    /**
     * Bouton de filtrage par genre
     */
    @FXML
    public Button btnFilterGenre;

    /**
     * Bouton de filtrage par artiste
     */
    @FXML
    public Button btnFilterArtiste;

    /**
     * TextField stockant le format à rechercher
     */
    @FXML
    public TextField textField;

    /**
     * Gère le retour sur la première page quand on clique sur le logo de l'application
     *
     * @param mouseEvent L'évènement généré
     */
    public void handleClickOnLogo(MouseEvent mouseEvent) {
        try {
            BorderPane parent = (BorderPane) topBar.getParent();
            parent.setCenter(FXMLLoader.load(getClass().getResource("/fxml/list_music.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Appelé quand on clique sur un des filtres. Maintient le côté graphique de
     * l'application en mettant les bonnes classes CSS sur les boutons. Passe également
     * l'information au back-end pour qu'on puisse gérer le filtrage correctement
     *
     * @param ae L'action de click généré
     */
    @FXML
    public void onToggleFilter(ActionEvent ae) {
        Node n = (Node) ae.getSource();

        if (!n.getStyleClass().contains("selected")) {
            n.getStyleClass().add("selected");
        } else {
            n.getStyleClass().remove("selected");
        }

        String filterType = (String) n.getUserData();
        SingleSearchManager.getInstance().toggleFilter(SearchFilter.byName(filterType));
        SingleInteropTopbarListmusic.getInstance().applyFilter();
    }


    /**
     * Gère l'initialisation du contrôleur FXML
     *
     * @param url            URL du fichier FXML
     * @param resourceBundle Etat des resources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        SingleInteropTopbarListmusic.getInstance().setTopBarController(this);
        this.textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                SingleSearchManager.getInstance().setName(t1);
                SingleInteropTopbarListmusic.getInstance().applyFilter();
            }
        });
    }
}