package controller;

import eu.hansolo.enzo.notification.Notification;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AllMusicController extends AbstractListMusicController {

    /**
     * Fonction de suppression de musique du controller FXML
     */

    @Override
    public void suppress() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        ObservableList<Oeuvre> selectedFiles = listmusics.getSelectionModel().getSelectedItems();
        boolean isPluriel = selectedFiles.size() > 1;
        alert.setTitle("Supprimer cette musique de Tiamo ?");
        alert.setHeaderText("Êtes-vous sûr de vouloir supprimer "+ (isPluriel ? "ces" : "cette") +" musique"+ (isPluriel ? "s" : "") +" de Tiamo ?\n" +
                            "Elle"+ (isPluriel ? "s" : "") +" "+ (isPluriel ? "seront" : "sera") +" ignorée"+ (isPluriel ? "s" : "") +" lors des prochaines recherches dans les répertoires définis.\n" +
                            "Vous pourrez toujours "+ (isPluriel ? "les " : "l'") +"enlever des musiques ignorées pour que la recherche "+ (isPluriel ? "les" : "la") +" trouve de nouveau.");
        String list = " "+ (isPluriel ? "Ces" : "Cette") +" musique"+ (isPluriel ? "s" : "") +" ne "+ (isPluriel ? "seront" : "sera") +" pas supprimée"+ (isPluriel ? "s" : "") +" de votre ordinateur.\n\n Liste des musiques : \n";
        for (Oeuvre oeuvre : selectedFiles) {
            try {
                list += "- "+oeuvre.getMetaData().getMusicName()+"\n";
            } catch (Exception e) {
                list += "- "+oeuvre.getFile().getName()+"\n";
            }
        }
        alert.setContentText(list);
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
            for(Oeuvre oeuvre : selectedFiles) {
                try {
                    ListeOeuvres.getInstance().deleteOeuvre(oeuvre);
                } catch (Exception e) {}
            }
            try {
                listmusics.setItems(FXCollections.observableArrayList(ListeOeuvres.getInstance().getOeuvreList()));
            } catch (Exception e) {}
            System.out.println("File deleted!");
        } else if (option.get() == ButtonType.CANCEL) {
            System.out.println("Cancelled!");
        } else {
            System.out.println("-");
        }
    }

    /**
     * Fonction d'ajout d'une musique à une playlist du controller FXML
     */
    public void addPlaylist() {
        if(!listmusics.getSelectionModel().getSelectedItems().isEmpty()) {
            selectedOeuvre = listmusics.getSelectionModel().getSelectedItems();
            ComboBox playlistComboBox = new ComboBox();
            if (!ListeOeuvres.getInstance().getPlaylistList().isEmpty()) {
                ListeOeuvres.getInstance().getPlaylistList().sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
                for (Playlist playlist : ListeOeuvres.getInstance().getPlaylistList()) {
                    playlistComboBox.getItems().add(playlist.getName());
                }
            }

            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/fxml/addToPlaylist.fxml"));
                /*
                 * if "fx:controller" is not set in fxml
                 * fxmlLoader.setController(NewWindowController);
                 */
                Scene scene = new Scene(fxmlLoader.load(), 400, 100);
                Stage stage = new Stage();
                stage.setTitle("Add music in playlist");
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        }
        else {
            Notification.Notifier.INSTANCE.notifyInfo("Information", "Selectionnez une ou plusieurs musiques pour ajouter aux playlists");
        }
    }

    /**
     * Fonction d'initialisation du controller FXML
     *
     * @param url            URL du document FXML
     * @param resourceBundle Etat des ressources
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.url = url;

//        listmusics.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection)->{
//            if(newSelection != null)
//            {
//                selectedOeuvre = (Oeuvre)newSelection;
//            }
//        });

        SingleInteropTopbarListmusic.getInstance().setListMusicController(this);
        try {
            ListeOeuvres lo = ListeOeuvres.getInstance();
            lo.setOeuvreList(new ArrayList<Oeuvre>());
            try {
                Music.updateMusic();
            } catch (Exception e) {
                System.out.println("PAS DE REPERTOIRE");
                e.printStackTrace();
            }
            list = (ArrayList<Oeuvre>) lo.getOeuvreList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ObservableList<Oeuvre> test = FXCollections.observableArrayList(list);
        super.listmusics.setItems(test);
        super.mp = MusicPlayer.getInstance();
        super.mp.setReadingList(list);

        super.initialize(url, resourceBundle);
    }
}
