package model.recherche;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Filter {

    /**
     * Nom sur lequel filtrer
     */
    private final String name;

    /**
     * Tous les filtres (JOIN + WHERE) que l'on va appliquer sur la recherche
     */
    private final List<SearchFilter> filters;

    /**
     * Constructeur de la classe Filtre
     *
     * @param name    String sur laquelle on va filtrer
     * @param filters Elements sur lesquels filtrer
     */
    public Filter(String name, List<SearchFilter> filters) {
        this.name = name;
        this.filters = filters;
    }

    /**
     * Fonction de mapping pour JOIN
     *
     * @param f SearchFilter en entrée
     * @return la partie JOIN formatée correctement
     */
    private static String apply_join(SearchFilter f) {
        ArrayList<String> joins = FilterJoin(f);
        assert joins != null;
        if (joins.size() == 0) {
            return "";
        }

        String join_query = "JOIN " + joins.get(0) + " ON " + joins.get(1);
        if (joins.size() > 2) {
            join_query += "\nJOIN " + joins.get(2) + " ON " + joins.get(3);
        }
        return join_query;
    }


    /**
     * Fonction de mapping pour WHERE
     *
     * @param f SearchFilter en entrée
     * @return la partie WHERE formatée correctement
     */
    private String apply_where(SearchFilter f) {
        String arg = FilterWhere(f);
        return arg + " LIKE '%" + this.name + "%'";
    }

    /**
     * Consomme le filtre pour le transformer en requête SQL
     * <p>
     * Exemple de requête générée :
     * SELECT m.id_musique FROM musique m
     * JOIN artiste ar ON m.id_artiste = ar.id_artiste
     * JOIN album al ON al.id_album = m.id_album
     * JOIN musique_genre mg ON mg.id_musique = m.id_musique
     * JOIN genre g ON g.id_genre = mg.id_genre
     * WHERE g.libelle_genre = 'GENRE'
     * OR al.nom_album = 'ALBUM'
     * OR ar.nom_artiste = 'ARTISTE'
     * OR m.libelle_musique = 'MUSIQUE';
     * </p>
     *
     * @return Un String contenant la requête SQL générée
     */
    public String ToSQL() {

        // Par défaut, on cherche par titre
        if (this.filters.isEmpty()) {
            this.filters.add(SearchFilter.TITRE);
        }

        List<String> join =
                this
                        .filters
                        .stream()
                        .map(Filter::apply_join)
                        .filter(f -> !f.equals(""))
                        .collect(Collectors.toList());

        List<String> where =
                this
                        .filters
                        .stream()
                        .map(this::apply_where)
                        .collect(Collectors.toList());

        StringBuilder query = new StringBuilder("SELECT m.id_musique FROM musique m\n");
        for (String s : join) {
            query.append(s).append('\n');
        }
        query.append("WHERE ").append(where.get(0));
        if (where.size() > 1) {
            for (int i = 1; i < where.size(); i++) {
                query.append('\n').append("OR ").append(where.get(i));
            }
        }

        return query.append(";").toString();
    }

    /**
     * Génère les arguments pour la partie JOIN de la requête
     *
     * @param sf Le filtre à extraire en JOIN
     * @return Un JOIN à ajouter à la requête SQL
     */
    private static final ArrayList<String> FilterJoin(SearchFilter sf) {
        switch (sf) {
            case ALBUM:
                return new ArrayList<>(Arrays.asList("album al", "al.id_album = m.id_album"));
            case ARTISTE:
                return new ArrayList<>(Arrays.asList("artiste ar", "ar.id_artiste = m.id_artiste"));
            case GENRE:
                return new ArrayList<>(Arrays.asList(
                        "musique_genre mg",
                        "mg.id_musique = m.id_musique",
                        "genre g",
                        "g.id_genre = mg.id_genre"
                ));
            case TITRE:
                return new ArrayList<>(Arrays.asList());
        }
        return null;
    }

    private static final String FilterWhere(SearchFilter sf) {
        switch (sf) {
            case ALBUM:
                return "al.nom_album";
            case TITRE:
                return "m.libelle_musique";
            case ARTISTE:
                return "ar.nom_artiste";
            case GENRE:
                return "g.libelle_genre";
        }
        return null;
    }


}