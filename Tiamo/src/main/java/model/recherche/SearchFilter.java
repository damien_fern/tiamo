package model.recherche;

/**
 * Représente un type de données par lequel on peut filtrer les résultats de recherche
 */
public enum SearchFilter {
    TITRE,
    ALBUM,
    ARTISTE,
    GENRE;

    /**
     * Obtient un SearchFilter par sa représentation en String
     *
     * @param s La représentation en chaine de caractères du filtre
     * @return Le filtre concerné
     */
    public static SearchFilter byName(String s) {
        switch (s) {
            case "Genre":
                return SearchFilter.GENRE;
            case "Artiste":
                return SearchFilter.ARTISTE;
            case "Album":
                return SearchFilter.ALBUM;
            case "Titre":
                return SearchFilter.TITRE;
            default:
                return null;
        }
    }
}
