package model.recherche;

import java.util.ArrayList;
import java.util.List;

/**
 * Manager permettant de gérer les différents filtres à appliquer pour la recherche d'oeuvres
 *
 * @author TIAMO
 */
public final class SingleSearchManager {
    /**
     * Instance unique, privée de la classe SearchManager
     */
    private static volatile SingleSearchManager instance = null;

    /**
     * Nom de l'oeuvre qu'on cherche à filtrer
     */
    private String name;

    /**
     * Filtres à appliquer sur la recherche (Genre, Artiste, etc...)
     */
    private List<SearchFilter> filters;

    /**
     * Change le texte de la recherche
     *
     * @param name Le texte à rechercher dans la base de données
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Change les filtres en place, les activant ou désactivant en
     * fonction de leur présence ou non permet de choisir ce sur
     * quoi on va filtrer
     *
     * @param sf Filtre à toggle
     */
    public void toggleFilter(SearchFilter sf) {
        if (this.filters.contains(sf)) {
            this.filters.remove(sf);
        } else {
            this.filters.add(sf);
        }
    }

    /**
     * Constructeur privé afin d'empêcher l'instanciation. On cherche à
     * instancier notre propre
     */
    private SingleSearchManager() {
        this.name = "";
        this.filters = new ArrayList<>();
    }

    /**
     * Génère le filtre à partir des informations actuelles du SearchManager
     *
     * @return
     */
    public Filter GenerateFilter() {
        return new Filter(this.name, this.filters);
    }

    /**
     * Implémentation du pattern Singleton
     *
     * @return L'instance unique de la classe SingleSearchManager
     */
    public final static SingleSearchManager getInstance() {
        if (instance != null) {
            return SingleSearchManager.instance;
        }

        synchronized (SingleSearchManager.class) {
            if (SingleSearchManager.instance == null) {
                SingleSearchManager.instance = new SingleSearchManager();
            }
        }
        return SingleSearchManager.instance;
    }

}


