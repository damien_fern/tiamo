package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Artist {
    private String name;
    private Date dateOfBirth;
    private String country;
    private String musicGroup;

    public Artist(String name) {
        this.name = name;
        try {
            this.createInDBIfNotExist();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getIdArtist(String artistName) throws Exception {
        ResultSet rsArtist = DB.getInstance().getStatement().executeQuery("SELECT * from artiste where nom_artiste='" + artistName + "';");
        int idArtist = 0;
        try {
            idArtist = rsArtist.getInt("id_artiste");
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        if(idArtist == 0) {
            throw new Exception("L'artiste \"" + artistName + "\" n'existe pas en BDD");
        }
        return idArtist;
    }

    public int createInDBIfNotExist() throws Exception {
        try {
            return getIdArtist(this.name);
        } catch (Exception e) {}
        try {
            String querySQL = "insert into artiste(";
            String fieldValue = " values(";
            boolean isFirst = true;
            if(this.getName() != null){
                querySQL += "nom_artiste";
                fieldValue += "'"+this.getName()+"'";
                isFirst = false;
            }
            if(this.getDateOfBirth() != null){
                if(!isFirst) {
                    querySQL += ", ";
                    fieldValue += ", ";
                }
                isFirst = false;
                SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
                querySQL += "date_naissance_artiste";
                fieldValue += "'" + formater.format(this.dateOfBirth) + "'";
            }
            if(this.getCountry() != null){
                if(!isFirst) {
                    querySQL += ", ";
                    fieldValue += ", ";
                }
                isFirst = false;
                querySQL+= "pays_artiste ";
                fieldValue += "'" + this.getCountry() + "'";
            }
            if(this.getMusicGroup() != null){
                if(!isFirst) {
                    querySQL += ", ";
                    fieldValue += ", ";
                }
                isFirst = false;
                querySQL += "groupe_musique_artiste";
                fieldValue += "'"+this.getMusicGroup()+"'";
            }
            querySQL += ")";
            fieldValue += ")";
            DB.getInstance().getStatement().executeUpdate(querySQL + fieldValue);
            ResultSet rsNewArtist = DB.getInstance().getStatement().executeQuery("SELECT * from artiste where nom_artiste='" + this.getName() + "';");
            return rsNewArtist.getInt("id_artiste");
        } catch (SQLException e) {
            throw new Exception("Impossible de créer l'artiste \"" + this.name + "\" en BDD" + e.getMessage());
        }
    }

    public static ArrayList<Artist> createAllArtistFromDB() {
        ArrayList<Artist> artistsList = new ArrayList<>();
        try {
            ResultSet rsArtist = DB.getInstance().getStatement().executeQuery("SELECT * FROM artiste");
            if(!rsArtist.isClosed()) {
                while(rsArtist.next()) {
                    Artist artist = null;
                    if(rsArtist.getString("nom_artiste") != null && !rsArtist.getString("nom_artiste").equals("")) {
                        artist = new Artist(rsArtist.getString("nom_artiste"));
                        if(rsArtist.getString("date_naissance_artiste") != null && !rsArtist.getString("date_naissance_artiste").equals("")) {
                            try {
                                artist.setDateOfBirth(new SimpleDateFormat("yyyy-MM-dd").parse(rsArtist.getString("date_naissance_artiste")));
                            } catch (Exception e) {}
                        }
                        if (rsArtist.getString("pays_artiste") != null && !rsArtist.getString("pays_artiste").equals("")) {
                            try {
                                artist.setCountry(rsArtist.getString("pays_artiste"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (rsArtist.getString("groupe_musique_artiste") != null && !rsArtist.getString("groupe_musique_artiste").equals("")) {
                            try {
                                artist.setMusicGroup(rsArtist.getString("groupe_musique_artiste"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        artistsList.add(artist);
                    }
                    else {
                        continue;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return artistsList;
    }

    public static Artist createArtistFromDB(String name) throws Exception {
        Artist artist = null;
        try {
            ResultSet rsArtist = DB.getInstance().getStatement().executeQuery("SELECT * FROM artiste WHERE nom_artiste='"+name+"';");
            if(!rsArtist.isClosed()) {
                while(rsArtist.next()) {
                    artist = new Artist(rsArtist.getString("nom_artiste"));
                    if(rsArtist.getString("date_naissance_artiste") != null && !rsArtist.getString("date_naissance_artiste").equals("")) {
                        try {
                            artist.setDateOfBirth(new SimpleDateFormat("yyyy-MM-dd").parse(rsArtist.getString("date_naissance_artiste")));
                        } catch (Exception e) {}
                    }
                    if (rsArtist.getString("pays_artiste") != null && !rsArtist.getString("pays_artiste").equals("")) {
                        try {
                            artist.setCountry(rsArtist.getString("pays_artiste"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (rsArtist.getString("groupe_musique_artiste") != null && !rsArtist.getString("groupe_musique_artiste").equals("")) {
                        try {
                            artist.setMusicGroup(rsArtist.getString("groupe_musique_artiste"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return artist;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new Exception("Artiste nom trouvé");
    }

    public void deleteInDBIfExist() throws Exception {
        int idArtist = 0;
        try {
            idArtist = getIdArtist(this.name);
        } catch (Exception e) {
            return;
        }
        try {
            DB.getInstance().getStatement().executeUpdate("DELETE FROM artiste WHERE id_artiste = " + idArtist + ";");
        } catch (SQLException e) {
            throw new Exception("Impossible de supprimer l'artiste \"" + this.name + "\" en BDD : " + e.getMessage());
        }
    }

    public String getName() {
        return name;
    }

    private void modifyNameBDD(String newName) throws Exception {
        int idArtist = 0;
        try {
            idArtist = getIdArtist(this.name);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier le nom de l'artiste en BDD : " + e.getMessage());
        }
        try {
            DB.getInstance().getStatement().executeUpdate("UPDATE artiste SET nom_artiste = '"+newName+"' WHERE id_artiste = " + idArtist);
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier le nom de l'artiste en BDD : " + e.getMessage());
        }
    }

    public void setName(String newName) throws Exception {
        if(!newName.equals(this.getName())) {
            this.modifyNameBDD(newName);
            this.name = newName;
        }
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    private void modifyDateOfBirthBDD(Date newDateOfBirth) throws Exception {
        int idArtist = 0;
        try {
            idArtist = getIdArtist(this.name);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier la date de naissance de l'artiste en BDD : " + e.getMessage());
        }
        try {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            DB.getInstance().getStatement().executeUpdate("UPDATE artiste SET date_naissance_artiste = '"+formater.format(newDateOfBirth)+"' WHERE id_artiste = " + idArtist);
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier la date de naissance de l'artiste en BDD : " + e.getMessage());
        }
    }

    public void setDateOfBirth(Date newDateOfBirth) throws Exception {
        if(!newDateOfBirth.equals(this.getDateOfBirth())) {
            this.modifyDateOfBirthBDD(newDateOfBirth);
            this.dateOfBirth = newDateOfBirth;
        }
    }

    public String getCountry() {
        return country;
    }

    private void modifyCountryBDD(String newCountry) throws Exception {
        int idArtist = 0;
        try {
            idArtist = getIdArtist(this.name);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier le pays de l'artiste en BDD : " + e.getMessage());
        }
        try {
            DB.getInstance().getStatement().executeUpdate("UPDATE artiste SET pays_artiste = '"+newCountry+"' WHERE id_artiste = " + idArtist);
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier le pays de l'artiste en BDD : " + e.getMessage());
        }
    }

    public void setCountry(String newCountry) throws Exception {
        if(!newCountry.equals(this.getCountry())) {
            this.modifyCountryBDD(newCountry);
            this.country = newCountry;
        }
    }

    public String getMusicGroup() {
        return musicGroup;
    }

    private void modifyMusicGroupBDD(String newMusicGroup) throws Exception {
        int idArtist = 0;
        try {
            idArtist = getIdArtist(this.name);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier le nom du groupe de l'artiste : " + e.getMessage());
        }
        try {
            DB.getInstance().getStatement().executeUpdate("UPDATE artiste SET groupe_musique_artiste = '"+newMusicGroup+"' WHERE id_artiste = " + idArtist);
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier le nom du groupe de l'artiste : " + e.getMessage());
        }
    }

    public void setMusicGroup(String newMusicGroup) throws Exception {
        if(!newMusicGroup.equals(this.getMusicGroup())) {
            this.modifyMusicGroupBDD(newMusicGroup);
            this.musicGroup = newMusicGroup;
        }
    }

    public String toString() {
        String toString = this.getName();
        if(dateOfBirth != null) {
            toString += " - " + new SimpleDateFormat("dd/MM/yyyy").format(this.dateOfBirth) + " - ";
        }
        if(this.getCountry() != null) {
            toString += " - " + this.getCountry();
        }
        if(this.getMusicGroup() != null) {
            toString += " - " + this.getMusicGroup();
        }
        return toString;
    }
}
