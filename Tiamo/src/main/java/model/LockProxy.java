package model;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class LockProxy<L extends Lock> implements Lock {

    // The actual lock.
    private volatile ReentrantLock lock;

    public LockProxy() {
        // Trap the lock we are proxying.
        this.lock = new ReentrantLock();
    }

    @Override
    public void lock() {
        // Proxy it.
        lock.lock();
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        // Proxy it.
        lock.lockInterruptibly();
    }

    @Override
    public boolean tryLock() {
        // Proxy it.
        if(this.lock.getHoldCount() > 0) {
            return false;
        }
        return lock.tryLock();
    }

    @Override
    public boolean tryLock(long l, TimeUnit tu) throws InterruptedException {
        // Proxy it.
        boolean returnStatement = lock.tryLock(l, tu);
        if(returnStatement && this.lock.getHoldCount() > 1) {
            lock.unlock();
            return false;
        }
        return returnStatement;
    }

    @Override
    public void unlock() {
        // Proxy it.
        try {
            lock.unlock();
        }catch (IllegalMonitorStateException e) {
            forceUnlock();
        }
    }

    @Override
    public Condition newCondition() {
        // Proxy it.
        return lock.newCondition();
    }

    // Extra functionality to unlock from any thread.
    private void forceUnlock() {
        // Actually just replace the perhaps locked lock with a new one.
        // Kinda like a clone. I expect a neater way is around somewhere.
        lock = new ReentrantLock();
    }
}
