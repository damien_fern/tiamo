package model;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.concurrent.TimeUnit;

/**
 * Recherche les oeuvres en fonction des extensions souhaités dans les répertoires souhaités
 * @author Flo
 *
 */
public final class SearchOeuvres {
	
	private List<File> musiqueTab;
	private List<String> extensionsFile;
	private List<File> searchableDirUrl;
	private static SearchOeuvres instance = null;

	/**
	 * Déclare un SearchOeuvres avec les entensions sous forme d'enum voulues
	 * @param extensionsTab liste des entensions voulues sous forme d'enum
	 */
	private SearchOeuvres (List<OeuvreType> extensionsTab) {
		this.musiqueTab = new ArrayList<>();
		extensionsFile = new ArrayList<>();
		for(OeuvreType oeuvreType : extensionsTab) {
			extensionsFile.add(oeuvreType.getType());
		}
		this.searchableDirUrl = new ArrayList<>();
		this.addDirFromBDD();
	}

	/**
	 * Ajoute dans le tableau des répertoires à rechercher tous les répertoires contenus en BDD
	 */
	private void addDirFromBDD() {
		ResultSet rsSearchableDir = null;
		try {
			rsSearchableDir = DB.getInstance().getStatement().executeQuery("SELECT * FROM repertoireDeRecherche");
			while(rsSearchableDir.next()) {
				this.addSearchableDirUrl(new File(rsSearchableDir.getString("path")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Méthode permettant de renvoyer une instance de la classe Singleton
	 * @return Retourne l'instance du singleton.
	 */
	public final static SearchOeuvres getInstance() {
		if (SearchOeuvres.instance == null) {
			synchronized(ListeOeuvres.class) {
				if (SearchOeuvres.instance == null) {
					SearchOeuvres.instance = new SearchOeuvres(Arrays.asList(EnumMusic.MP3, EnumMusic.WAV));
				}
			}
		}
		return SearchOeuvres.instance;
	}

	/**
	 * Ajoute le répertoire en parametre dans la table des repertoire de recherche en BDD
	 * @param dir
	 */
	private void addSearchableDirUrlInBDD(File dir) {
		try {
			ResultSet rsSearchableDir = DB.getInstance().getStatement().executeQuery("SELECT * from repertoireDeRecherche where path='" + dir.getAbsolutePath() + "';");
			while(rsSearchableDir.next()) {
				if(dir.getAbsolutePath().equals(rsSearchableDir.getString("path"))) {
					return;
				}
			}
			DB.getInstance().getStatement().executeUpdate("insert into repertoireDeRecherche(path) values('"+dir+"')");
		} catch (SQLException e) {
			System.err.println("Impossible d'ajouter un nouveau repertoire de recherche : " + e.getMessage());
		}
	}

	/**
	 * Ajoute le répertoire en paramètre dans le tableau des répertoires recherchés
	 * @param dir
	 */
	public void addSearchableDirUrl(File dir) {
		if(dir.exists()) {
			if(!this.searchableDirUrl.contains(dir)) {
				this.searchableDirUrl.add(dir);
				this.addSearchableDirUrlInBDD(dir);
			}
		}
		else {
			System.err.println("Impossible d'ajouter de repertoire : il n'existe pas !");
		}
	}

	/**
	 * Ajoute la liste de répertoire en paramètre dans le tableau des répertoires recherchés
	 * @param dirTab
	 */
	public void addSearchableDirUrl(List<File> dirTab) {
		for(File dir : dirTab) {
			addSearchableDirUrl(dir);
		}
	}

	/**
	 * Supprime le répertoire en paramètre de la BDD
	 * @param dir
	 */
	private void removeSearchableDirUrlInBDD(File dir) {
		try {
			DB.getInstance().getStatement().executeUpdate("DELETE FROM repertoireDeRecherche WHERE path = '" + dir.getAbsolutePath() + "';");
		} catch (SQLException e) {
			System.err.println("Impossible de supprimer ce repertoire de recherche : " + e.getMessage());
		}
	}

	/**
	 * Supprime le répertoire en paramètre de la liste des répertoire à rechercher
	 * @param dir
	 */
	public void removeSearchableDirUrl(File dir) {
		if(this.searchableDirUrl.contains(dir)) {
			this.searchableDirUrl.remove(dir);
			this.removeSearchableDirUrlInBDD(dir);
		}
	}

	/**
	 * Supprime la liste de répertoire en paramètre de la liste des répertoire à rechercher
	 * @param dirTab
	 */
	public void removeSearchableDirUrl(List<File> dirTab) {
		for(File dir : dirTab) {
			removeSearchableDirUrl(dir);
		}
	}

	/**
	 * Retourne une copie de la liste des répertoires à rechercher
	 * @return
	 */
	public List<File> getSearchableDirUrl() {
		return new ArrayList<>(this.searchableDirUrl);
	}

	/**
	 * Retourne le tableau des extensions recherchées
	 * @return List<String>
	 */
	public List<String> getExtensions() {
		return extensionsFile;
	}
	
	/**
	 * Défini le tableau des extensions recherchées
	 * @param extensionsFileTab
	 */
	public void setExtensionsFile(List<String> extensionsFileTab) {
		this.extensionsFile = extensionsFileTab;
	}

	/**
	 * Ajoute l'extension saisie en param au tableau des extensions recherchables 
	 * @param extension : extension
	 */
	public void addExtensionForSearch(String extension) {
		if(this.extensionsFile.contains(extension)) {
			System.out.println("Extension déjà présente");
			return;
		}
		this.extensionsFile.add(extension);
	}
	
	/**
	 * Supprime l'extension en param du tableau des extensions recherchables
	 * @param extension : extension
	 */
	public void removeExtension (String extension) {
		if(!this.extensionsFile.contains(extension)) {
			System.out.println("Extension non présente donc impossible à supprimer");
			return;
		}
		this.extensionsFile.remove(extension);
	}
	
	/**
	 * Ajoute toutes les musiques trouvées dans le répertoire mis en param dans le tableau en attribut
	 * @param dir : répertoire
	 */
	private void listDesirableFiles(File dir) { //Affiche que les fichiers présents dans l'url en param
	      File files[] = dir.listFiles(new FilenameFilter()
			{ 
				@Override
		        public boolean accept(File dir, String name)
		        {
					for(String extension : extensionsFile) {
						if(name.endsWith("."+extension))
						{
							return true;
						}
					}
					return false;
		        }
			}
		  );
	      if(files == null || files.length == 0) {
	    	  //System.out.println("Aucune musique trouve  l'endroit spécifié !");
	    	  return;
	      }
	      for(File file : files)
	      {
	    	  if(file.isFile()) {
	    		  //System.out.println(file.getName());
	    		  this.musiqueTab.add(file);
	    	  }
	      }
	   }

	/**
	 * Recherche tous les sous-répertoires du répertoire placé en param
	 * @param dir : répertoire
	 */
   private void listDirectory(File dir)
   {
      File subDirectory[] = dir.listFiles(new FileFilter()  //Met tous les sous répertoires dans subDirectory
      { 
         @Override
         public boolean accept(File pathname) 
         { 
            return pathname.isDirectory();
         }
      });

      //System.out.println("\nDirectory of " + dir.getAbsolutePath());
      listDesirableFiles(dir); //Affiche tous les fichiers avec l'extension voulue présents dans l'url en param

      for(File fl : subDirectory) //rappelle la fct recusivement pour chaque sous repertoire
      {
         listDirectory(fl);
      }
   }
   
   /**
    * Recherche toutes les oeuvres dans les dossiers placés dans le tab en param
    * @return List<File> : FichiersTrouvées
    */
	public List<File> lookForOeuvre() {
		this.musiqueTab.clear(); //Vider le tableau avec toutes les oeuvres trouvées
		for(File urlRepo : this.searchableDirUrl) {
			if(!urlRepo.exists()) {
				System.err.println("Le dossier \"" + urlRepo.getAbsolutePath() + "\" dans lequel des musiques doivent être recherchées n'existe pas sur votre ordinateur");
				continue;
			}
			listDirectory(urlRepo);
		}
		return this.musiqueTab;
	}

	/**
	 * Recherche toutes les oeuvres dans les dossiers placés dans le tab en param
	 * @return List<File> : FichiersTrouvées
	 */
	public List<File> lookForOeuvre(File searchDir) {
		this.musiqueTab.clear(); //Vider le tableau avec toutes les oeuvres trouvées
		if(!searchDir.exists()) {
			System.err.println("Le dossier \"" + searchDir.getAbsolutePath() + "\" dans lequel des musiques doivent être recherchées n'existe pas sur votre ordinateur");
		}
		listDirectory(searchDir);
		return this.musiqueTab;
	}

	public String toString() {
		if(this.musiqueTab.isEmpty()) {
			return "Aucune oeuvre trouvée";
		}
		else {
			String description = "Oeuvres : \n";
			for(File oeuvre : this.musiqueTab) {
				description += "\t- " + oeuvre.getName() + "\n";
			}
			return description;
		}
	}

	public static void deleteDirById(int idSearchDir) {
		try {
			ResultSet rsDir = DB.getInstance().getStatement().executeQuery("SELECT * FROM repertoireDeRecherche WHERE id_repertoire = "+idSearchDir+";");
			// read the result set
			if(!rsDir.isClosed()) {
				List<Integer> tabIdDirToDelete = new ArrayList();
				while(rsDir.next()) {
						tabIdDirToDelete.add(rsDir.getInt("id_repertoire"));
				}
				for(int idDirToDelete : tabIdDirToDelete) {
					DB.getInstance().getStatement().executeUpdate("DELETE FROM repertoireDeRecherche WHERE id_repertoire = " + idDirToDelete + ";");
				}
			}
		} catch(SQLException e) {
			System.err.println(e.getMessage());
		}
	}

	public static void deleteDirNotInComputer() {
		try {
			ResultSet rsDir = DB.getInstance().getStatement().executeQuery("SELECT * FROM repertoireDeRecherche;");
			// read the result set
			if(!rsDir.isClosed()) {
				String dirPath;
				int dirId;
				File file;
				List<Integer> tabIdDirToDelete = new ArrayList();
				while(rsDir.next()) {
					dirPath = rsDir.getString("path");
					file = new File(dirPath);
					if (!file.exists()) {
						tabIdDirToDelete.add(rsDir.getInt("id_repertoire"));
					}
				}
				for(int idDirToDelete : tabIdDirToDelete) {
					DB.getInstance().getStatement().executeUpdate("DELETE FROM repertoireDeRecherche WHERE id_repertoire = " + idDirToDelete + ";");
				}
			}
		} catch(SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
