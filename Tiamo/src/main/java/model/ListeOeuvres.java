package model;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.*;

public final class ListeOeuvres {

	private List<Oeuvre> oeuvreList;
	private AbstractFactory oeuvreFactory;
	private List<Playlist> playlistList;
	private static ListeOeuvres instance = null;

	private ListeOeuvres(AbstractFactory af) {
		this.oeuvreFactory = af;
		this.oeuvreList = new ArrayList<Oeuvre>();
		this.playlistList = new ArrayList<Playlist>();
	}

	/**
	 * Méthode permettant de renvoyer une instance de la classe Singleton
	 * @return Retourne l'instance du singleton.
	 */
	public final static ListeOeuvres getInstance() {
		//Le "Double-Checked Singleton"/"Singleton doublement vérifié" permet
		//d'éviter un appel coûteux à synchronized,
		//une fois que l'instanciation est faite.
		if (ListeOeuvres.instance == null) {
			// Le mot-clé synchronized sur ce bloc empêche toute instanciation
			// multiple même par différents "threads".
			// Il est TRES important.
			synchronized(ListeOeuvres.class) {
				if (ListeOeuvres.instance == null) {
					ListeOeuvres.instance = new ListeOeuvres(new FactoryMusic());
				}
			}
		}
		return ListeOeuvres.instance;
	}


	/**
	 * Crée une oeuvre avec le fichier passé en paramètre
	 * @param oeuvreFile : Oeuvre à ajouter
	 * @throws Exception
	 */
	public Oeuvre createOeuvre(String fileName, File oeuvreFile, int idSearchDir) throws Exception{
		if(!oeuvreFile.exists()) { //Vérifie si l'oeuvre existe sur le pc
			throw new Exception("Aucune musique au chemin : " + oeuvreFile.getAbsolutePath());
		}
		EnumMusic[] tabEnum = EnumMusic.values(); //Vérifie si le type de fichier est autorisé
		boolean isTypeAllowed = false;
		for(int i = 0 ; i < tabEnum.length ; i++) { //parcourir les enum pour vérifier en fonction de l'extension du fichier
			if(tabEnum[i].getType().equals(Utils.getExtension(oeuvreFile))) {
				isTypeAllowed = true;
			}
		}
		if(!isTypeAllowed) {
			throw new Exception("Le type de fichier \"" + Utils.getExtension(oeuvreFile) + "\" n'est pas autorisé");
		}
		if (searchOeuvreByFileName(fileName) == null) {
			this.oeuvreFactory.setFile(oeuvreFile);
			Oeuvre newOeuvre = oeuvreFactory.createOeuvre(fileName, idSearchDir);
			oeuvreList.add(newOeuvre);
			return newOeuvre;
		} else {
			throw new Exception("L'oeuvre " + oeuvreFile.getName() + " est déja contenue dans la médiathèque");
		}
	}
	
	public void createEmptyPlaylist(String name) throws Exception {
		if("favoris".equals(name.toLowerCase())) {
			throw new Exception("La section favoris existe déjà");
		}
		for(Playlist playlist : this.playlistList) {
			if(playlist.getName().equals(name)) {
				throw new Exception("Une playlist portant le nom " + name + " existe déjà");
			}
		}
		Playlist playlist = new Playlist(name);
		this.playlistList.add(playlist);
		playlist.createPlaylistInDB();
	}

	/**
	 * Supprime l'oeuvre de la liste des fichiers sauvgardés
	 * @param oeuvre
	 * @throws Exception
	 */
	public void deleteOeuvre(Oeuvre oeuvre){ //TODO: Ajouter l'oeuvre à la blacklist
		try {
		    oeuvre.insertOeuvreInIgnoredList();
            deleteOeuvreWithoutIgnoreOeuvre(oeuvre);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Supprime l'oeuvre de la liste des fichiers sauvgardés
     * @param oeuvre
     * @throws Exception
     */
    public void deleteOeuvreWithoutIgnoreOeuvre(Oeuvre oeuvre) throws Exception { //TODO: Ajouter l'oeuvre à la blacklist //TODO: Supprimer de la playlist aussi
        this.playlistList.stream()
            .filter(playlist -> playlist.getCollection().contains(oeuvre))
            .forEach(playlist -> playlist.deleteOeuvre(oeuvre));
        if (!oeuvreList.remove(oeuvre)) {
            throw new Exception("L'oeuvre ne peut pas être supprimée car elle n'est pas connue");
        }
        oeuvre.deleteOeuvreInDB();
    }
	
	/**
	 * Supprime la playlist
	 * @param playlistName
	 */
	public void deletePlaylist(String playlistName) {
		for(Playlist pl : this.playlistList) {
			if(pl.getName().equals(playlistName)) {
				this.playlistList.remove(pl);
				Playlist.deletePlaylistInDB(pl.getName());
				pl = null;
				return;
			}
		}
		try {
			throw new Exception("Impossible de supprimer la playlist \"" + playlistName + "\", elle n'est pas stocké sur l'ordinateur");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Recherche une oeuvre dans le tableau d'oeuvre
	 * @param oeuvre à rechercher
	 * @return si trouvée : oeuvre recherchée ; sinon null
	 */
	public Oeuvre searchOeuvre(Oeuvre oeuvre) {
		if(this.oeuvreList.indexOf(oeuvre) != -1) {
			return this.oeuvreList.get(this.oeuvreList.indexOf(oeuvre));
		}
		return null;
	}

	/**
	 * Recherche une oeuvre dans le tableau d'oeuvre en fonction de son nom de fichier
	 * @param fileName de fichier de l'oeuvre à rechercher
	 * @return si trouvée : oeuvre recherchée ; sinon null
	 */
	public Oeuvre searchOeuvreByFileName(String fileName) {
		for(Oeuvre oeuvre : this.oeuvreList) {
			try {
				if(oeuvre.getMetaData().getMusicName().equals(fileName) || oeuvre.getFile().getName().equals(fileName)) {
					return oeuvre;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Recherche une playlist dans le tableau de playlist en fonction de son nom
	 * @param playlistName
	 * @return si trouvée : playlist recherchée ; sinon null
	 */
	public Playlist searchPlaylistByName(String playlistName) {
		for(Playlist playlist : this.playlistList) {
			if(playlist.getName().equals(playlistName)) {
				return playlist;
			}
		}
		return null;
	}
	
	/**
	 * Retourne un tableau avec toutes les oeuvres dont les extensions correspondent à celles passées en param
	 * @param oeuvreExtensionList : Liste des extensions à rechercher (sans le '.')
	 * @return List<Oeuvre> : Tableau des oeuvres correspondantes
	 */
	public List<Oeuvre> getArraySpecificOeuvre(List<OeuvreType> oeuvreExtensionList) {
		List<Oeuvre> typeOfOeuvreArray = new ArrayList<Oeuvre>();
		for(Oeuvre oeuvre : oeuvreList) {
			for(OeuvreType enumExtension : oeuvreExtensionList) {
				if(oeuvre.getType().getType().equals(enumExtension.getType())) {
					typeOfOeuvreArray.add(oeuvre);
				}
			}
		}
		return typeOfOeuvreArray;
		
	}
	
	/**
	 * Retourne la liste de toutes les oeuvres enregistrées
	 * @return List<Oeuvre> liste de toutes les oeuvres
	 */
	public List<Oeuvre> getOeuvreList() {
		return oeuvreList;
	}

	public AbstractFactory getOeuvreFactory() {
		return oeuvreFactory;
	}

	public List<Playlist> getPlaylistList() {
		return playlistList;
	}

	/**
	 * Remplace la liste d'oeuvres par celle passées en param
	 * @param oeuvreList Liste d'oeuvre
	 */
	public void setOeuvreList(List<Oeuvre> oeuvreList) {
		this.oeuvreList = oeuvreList;
	}
	
	public String toString() {
		String oeuvreList = "";
		for(Oeuvre oeuvre : this.getOeuvreList()) {
			oeuvreList+= "\t" + oeuvre.toString() + "\n"; 
		}
		return oeuvreList;
	}
}
