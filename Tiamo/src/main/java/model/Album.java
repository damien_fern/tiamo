package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Album {
    private String name;
    private Date releaseDate;

    public Album(String name) {
        this.name = name;
        try {
            this.createInDBIfNotExist();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getIdAlbum(String albumName) throws Exception {
        ResultSet rsAlbum = DB.getInstance().getStatement().executeQuery("SELECT * from album where nom_album='" + albumName + "';");
        int idAlbum = 0;
        try {
            idAlbum = rsAlbum.getInt("id_album");
        } catch (SQLException e) {
            //e.printStackTrace();
        }
        if(idAlbum == 0) {
            throw new Exception("L'album \"" + albumName + "\" n'existe pas en BDD");
        }
        return idAlbum;
    }

    public int createInDBIfNotExist() throws Exception {
        try {
            return getIdAlbum(this.name);
        } catch (Exception e) {}
        try {
            String querySQL = "insert into album(";
            String fieldValue = " values(";
            boolean isFirst = true;
            if(this.getName() != null){
                querySQL += "nom_album";
                fieldValue += "'"+this.getName()+"'";
                isFirst = false;
            }
            if(this.getReleaseDate() != null){
                if(!isFirst) {
                    querySQL += ", ";
                    fieldValue += ", ";
                }
                isFirst = false;
                SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
                querySQL += "date_parution_album";
                fieldValue += "'" + formater.format(this.releaseDate) + "'";
            }
            querySQL += ")";
            fieldValue += ")";
            DB.getInstance().getStatement().executeUpdate(querySQL + fieldValue);
            ResultSet rsNewArtist = DB.getInstance().getStatement().executeQuery("SELECT * from album where nom_album='" + this.getName() + "';");
            return rsNewArtist.getInt("id_album");
        } catch (SQLException e) {
            throw new Exception("Impossible de créer l'album \"" + this.name + "\" en BDD" + e.getMessage());
        }
    }

    public static ArrayList<Album> createAllAlbumFromDB() {
        ArrayList<Album> albumsList = new ArrayList<>();
        try {
            ResultSet rsAlbum = DB.getInstance().getStatement().executeQuery("SELECT * FROM album");
            if(!rsAlbum.isClosed()) {
                while(rsAlbum.next()) {
                    Album album = null;
                    if(rsAlbum.getString("nom_album") != null && !rsAlbum.getString("nom_album").equals("")) {
                        album = new Album(rsAlbum.getString("nom_album"));
                        if(rsAlbum.getString("date_parution_album") != null && !rsAlbum.getString("date_parution_album").equals("")) {
                            try {
                                album.setReleaseDate(new SimpleDateFormat("yyyy-MM-dd").parse(rsAlbum.getString("date_parution_album")));
                            } catch (Exception e) {}
                        }
                        albumsList.add(album);
                    }
                    else {
                        continue;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return albumsList;
    }

    public static Album createAlbumFromDB(String albumName) throws Exception {
        Album album = null;
        try {
            ResultSet rsAlbum = DB.getInstance().getStatement().executeQuery("SELECT * FROM album WHERE nom_album='"+albumName+"';");
            if(!rsAlbum.isClosed()) {
                while(rsAlbum.next()) {
                    album = new Album(rsAlbum.getString("nom_album"));
                    if(rsAlbum.getString("date_parution_album") != null && !rsAlbum.getString("date_parution_album").equals("")) {
                        try {
                            album.setReleaseDate(new SimpleDateFormat("yyyy-MM-dd").parse(rsAlbum.getString("date_parution_album")));
                        } catch (Exception e) {}
                    }
                    return album;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new Exception("Album nom trouvé");
    }

    public void deleteInDBIfExist() throws Exception {
        int idAlbum = 0;
        try {
            idAlbum = getIdAlbum(this.name);
        } catch (Exception e) {
            return;
        }
        try {
            DB.getInstance().getStatement().executeUpdate("DELETE FROM album WHERE id_album = " + idAlbum + ";");
        } catch (SQLException e) {
            throw new Exception("Impossible de supprimer l'album \"" + this.name + "\" en BDD : " + e.getMessage());
        }
    }

    public String getName() {
        return name;
    }

    private void modifyNameBDD(String newName) throws Exception {
        int idAlbum = 0;
        try {
            idAlbum = getIdAlbum(this.name);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier le nom de l'album en BDD : " + e.getMessage());
        }
        try {
            DB.getInstance().getStatement().executeUpdate("UPDATE album SET nom_album = '"+newName+"' WHERE id_album = " + idAlbum);
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier le nom de l'album en BDD : " + e.getMessage());
        }
    }

    public void setName(String newName) throws Exception {
        if(!newName.equals(this.getName())) {
            this.modifyNameBDD(newName);
            this.name = newName;
        }
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    private void modifyReleaseDateBDD(Date newReleaseDate) throws Exception {
        int idAlbum = 0;
        try {
            idAlbum = getIdAlbum(this.name);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier la date de parution de l'album en BDD : " + e.getMessage());
        }
        try {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            DB.getInstance().getStatement().executeUpdate("UPDATE album SET date_parution_album = '"+formater.format(newReleaseDate)+"' WHERE id_album = " + idAlbum);
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier la date de parution de l'album en BDD : " + e.getMessage());
        }
    }

    public void setReleaseDate(Date newReleaseDate) throws Exception {
        if(!newReleaseDate.equals(this.getReleaseDate())) {
            this.modifyReleaseDateBDD(newReleaseDate);
            this.releaseDate = newReleaseDate;
        }
    }

    public String toString() {
        String tostring = this.getName();
        if(this.getReleaseDate() != null) {
            tostring+=" : " + new SimpleDateFormat("dd/MM/yyyy").format(this.getReleaseDate());
        }
        return tostring;
    }

}
