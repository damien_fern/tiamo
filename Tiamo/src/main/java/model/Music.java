package model;

import javazoom.jl.decoder.JavaLayerException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.ObjectStreamClass;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Représente une musique
 * @author Flo
 *
 */
public class Music implements Oeuvre{

	private File musicFile;
	private OeuvreType EnumMusic;
	private FileStrategy musicStrategy;
	private MetaDataMusic metaData;
	
	/**
	 * Crée une musique à partir d'un fichier
	 * @param file
	 */
	public Music(File file) {
		this.musicFile = file;
	}

	/**
	 * Retourne le type de l'oeuvre
	 * @return OeuvreType
	 */
	public OeuvreType getType() {
		return this.EnumMusic;
	}

	/**
	 * Retourne le fichier
	 * @return File
	 */
	public File getFile() {
		return this.musicFile;
	}
	
	/**
	 * Définir le type de la musique
	 * @param enumMusic : type de la musique
	 */
	public void setType(OeuvreType enumMusic) {
		this.EnumMusic = enumMusic;
	}
	
	/**
	 * retourne la FileStrategy
	 */
	public FileStrategy getStrategy() {
		return this.musicStrategy;
	}

	/**
	 * Définir la stratégie
	 * @param strategy
	 */
	public void setStrategy(FileStrategy strategy) throws FileNotFoundException, JavaLayerException {
		this.musicStrategy = strategy;
		strategy.setFile(this.musicFile);
	}

	/**
	 * Définir les métadatas
	 * @param md
	 */
	public void setMetaData(MetaDataMusic md) {
		this.metaData = md;
	}

	/**
	 * Obtenir les métadatas
	 * @return
	 */
	public MetaDataMusic getMetaData() throws Exception {
		if(this.metaData == null) {
			throw new Exception("Les métadatas ne sont pas instanciées");
		}
		return this.metaData;
	}

	/**
	 * Vérifie que que l'extension du fichier associé à la musique correspond au type qui est défini dans la classe musique
	 * @return true si correspond, false sinon
	 */
	private boolean isFileRightType() {
        String extension = Utils.getExtension(this.musicFile);
        return extension.equals(this.getType().getType());
	}
	
	public String toString() {
        try {
            return this.getFile().getPath() + " - " + this.EnumMusic.getType() + " - " + this.getMetaData();
        } catch (Exception e) {
            e.printStackTrace();
            return this.getFile().getPath() + " - " + this.EnumMusic.getType();
        }
    }

	/**
	 * Retourne l'id de la musique en BDD pour le nom passer en param
	 * @param musicName
	 * @return musicId or 0
	 * @throws Exception
	 */
	public static int getIdMusicInBDD(String musicName) throws Exception {
		ResultSet rsMusic = DB.getInstance().getStatement().executeQuery("SELECT id_musique from musique where libelle_musique='" + musicName + "';");
		int idMusic = 0;
		try {
			idMusic = rsMusic.getInt("id_musique");
		} catch (SQLException e) {
			//e.printStackTrace();
		}
		if(idMusic == 0) {
			throw new Exception("La musique \"" + musicName + "\" n'existe pas en BDD");
		}
		return idMusic;
	}

	/**
	 * Retourne l'id de la musique en BDD pour le nom passer en param
	 * @param path
	 * @return musicId or 0
	 * @throws Exception
	 */
	public static int getIdMusicFromPathInBDD(String path) throws Exception {
		ResultSet rsMusic = DB.getInstance().getStatement().executeQuery("SELECT id_musique from musique where path_musique='" + path + "';");
		int idMusic = 0;
		try {
			idMusic = rsMusic.getInt("id_musique");
		} catch (SQLException e) {
			//e.printStackTrace();
		}
		if(idMusic == 0) {
			throw new Exception("La musique \"" + path + "\" n'existe pas en BDD");
		}
		return idMusic;
	}

	/**
	 * Crée une musique en BD à partir du fichier passé en paramètre
	 * @throws Exception
	 */
	public void createOeuvreInDB(int idSearchDir) throws Exception { //TODO: Vérifier que le nom de la musique n'est pas déjà pris par une autre en BD
		int idMusic = this.isMusicInDB(this.getFile().getAbsolutePath());
		if (idMusic == 0) {
			String querySQL = "";
			String fieldValue = "";
			MetaDataMusic metadata;
			try {
				metadata = this.getMetaData();
			} catch(Exception e) {
				e.printStackTrace();
				return;
			}
			querySQL = "insert into musique(libelle_musique,path_musique,id_repertoire";
			fieldValue = " values('" + metadata.getMusicName() + "','"+this.musicFile.getAbsolutePath()+"',"+idSearchDir;
			try{
				int musicDuration = metadata.getMusicDurationSeconds();
				querySQL += ",duree_musique";
				fieldValue += ","+musicDuration;
			}catch(NullPointerException e) {}
			try {
				EnumMusic musicExtension = Utils.getEnumFromExtensionMusic(Utils.getExtension(this.getFile()));
				querySQL += ",type_musique";
				fieldValue += ",'"+musicExtension.getType()+"'";
			} catch (Exception e) {}
			try {
				int startAt = metadata.getStartAtInSeconds();
				int endAt = metadata.getEndAtInSeconds();
				querySQL += ",startAt_musique,endAt_musique";
				fieldValue += ","+startAt+","+endAt;
			} catch (Exception e) {}
			try {
				int isLiked = metadata.isLiked() ? 1 : 0;
				querySQL += ",isLiked";
				fieldValue += ","+isLiked;
			} catch (Exception e) {}
			if(!metadata.getArtistList().isEmpty()) {
				for(Artist artist : metadata.getArtistList()) {
				}
				try {
					int idArtist = metadata.getArtistList().get(0).createInDBIfNotExist();
					querySQL += ",id_artiste";
					fieldValue += ","+idArtist;
				} catch (Exception e) {}
			}
			if(!metadata.getAlbumList().isEmpty()) {
				try {
					int idAlbum = metadata.getAlbumList().get(0).createInDBIfNotExist();
					querySQL += ",id_album";
					fieldValue += ","+idAlbum;
				} catch (Exception e) {}
			}
			querySQL += ")";
			fieldValue += ")";
			try {
				DB.getInstance().getStatement().executeUpdate(querySQL + fieldValue);
			} catch (SQLException e) {
				throw new Exception("Impossible de créer la musique en BDD");
			}
			idMusic = this.isMusicInDB(this.musicFile.getAbsolutePath());
			if(idMusic == 0) {
				throw new Exception("Impossible de créer la musique en BDD");
			}
			if(!metadata.getMusicKindList().isEmpty()) {
				for(EnumMusicKind enumMusicKind : metadata.getMusicKindList()) {
					try {
						MetaDataMusic.addMusicKindInBDD(enumMusicKind, idMusic);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		else {
			throw new Exception("L'oeuvre dont le fichier est " + this.musicFile.getName() + " est déjà enregistrée dans la BD");
		}
	}

	/**
	 * Crée un objet musique à partir de l'id en BD d'une musique et l'ajoute à la liste des oeuvres
	 * @param idMusic : id de la musique en BD
	 */
	public static Oeuvre createMusicFromDB(int idMusic) throws Exception {
	    try {
            ResultSet rsMusic = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique WHERE id_musique=" + idMusic + ";");
            if (rsMusic.isClosed() || rsMusic.getString("libelle_musique").equals("")) {
                throw new Exception("L'id \"" + idMusic + "\" ne correspond à aucune playlist en base de données");
            }
            else {
                Oeuvre musique = null;
                while (rsMusic.next()) {
                    musique = ListeOeuvres.getInstance().searchOeuvreByFileName(rsMusic.getString("libelle_musique"));
                    if (musique == null) {
                        File musicFile = new File(rsMusic.getString("path_musique"));
                        /*if(!musicFile.exists()) { //Contrôle sur l'existance des fichiers déjà réalisé en classe BD
                            throw new Exception("La musique à créer depuis la base de données n'existe pas sur l'ordinateur");
                        }*/
                        musique = ListeOeuvres.getInstance().createOeuvre(rsMusic.getString("libelle_musique"), musicFile, rsMusic.getInt("id_repertoire"));
                        try {
                            if (rsMusic.getInt("id_artiste") != 0) {
                                ResultSet rsArtist = DB.getInstance().getStatement().executeQuery("SELECT * FROM artiste WHERE id_artiste = " + rsMusic.getInt("id_artiste") + ";");
                                while (rsArtist.next()) {
                                    Artist artist = new Artist(rsArtist.getString("nom_artiste"));
                                    try {
                                        if (rsArtist.getString("date_naissance_artiste") != null && !rsArtist.getString("date_naissance_artiste").equals("")) {
                                            artist.setDateOfBirth(new SimpleDateFormat("yyyy-MM-dd").parse(rsArtist.getString("date_naissance_artiste")));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        if (rsArtist.getString("pays_artiste") != null && !rsArtist.getString("pays_artiste").equals("")) {
                                            artist.setCountry(rsArtist.getString("pays_artiste"));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        if (rsArtist.getString("groupe_musique_artiste") != null && !rsArtist.getString("groupe_musique_artiste").equals("")) {
                                            artist.setMusicGroup(rsArtist.getString("groupe_musique_artiste"));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        musique.getMetaData().addArtist(artist);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (rsMusic.getInt("id_album") != 0) {
                                ResultSet rsAlbum = DB.getInstance().getStatement().executeQuery("SELECT * FROM album WHERE id_album = " + rsMusic.getInt("id_album") + ";");
                                while (rsAlbum.next()) {
                                    Album album = new Album(rsAlbum.getString("nom_album"));
                                    try {
                                        if (rsAlbum.getString("date_parution_album") != null && !rsAlbum.getString("date_parution_album").equals("")) {
                                            album.setReleaseDate(new SimpleDateFormat("yyyy-MM-dd").parse(rsAlbum.getString("date_parution_album")));
                                        }
                                        musique.getMetaData().addAlbum(album);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        try {

                            ResultSet rsKindId = DB.getInstance().getStatement().executeQuery("SELECT id_genre FROM musique_genre WHERE id_musique = " + rsMusic.getInt("id_musique") + ";");
                            ArrayList<Integer> dbKindIdTab = new ArrayList<Integer>();
                            while (rsKindId.next()) {
                                dbKindIdTab.add(rsKindId.getInt("id_genre"));
                            }
                            ResultSet rsMusicKind = null;
                            for (int dbIdGenre : dbKindIdTab) {
                                rsMusicKind = DB.getInstance().getStatement().executeQuery("SELECT * FROM genre WHERE id_genre = " + dbIdGenre + ";");
                                while (rsMusicKind.next()) {
                                    for (EnumMusicKind emk : EnumMusicKind.values()) {
                                        if (emk.toString().equals(rsMusicKind.getString("libelle_genre"))) {
                                            try {
                                                musique.getMetaData().addMusicKind(emk);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        try {
							musique.getMetaData().setStartAtInSeconds(rsMusic.getInt("startAt_musique"));
						} catch (Exception e) {
							e.printStackTrace();
						}
                        try {
                       		musique.getMetaData().setEndAtInSeconds(rsMusic.getInt("endAt_musique"));
						} catch (Exception e) {
							e.printStackTrace();
						}
                        try {
							musique.getMetaData().setLiked(rsMusic.getInt("isLiked"));
						} catch (Exception e) {
							e.printStackTrace();
						}
                        return musique;
                    }
                }
                throw new Exception("Impossible de créer la musique depuis la BDD");
            }
        } catch(Exception e) {
	        throw e;
        }
	}

    /**
     * Instancie toutes les musiques contenue en BD et les ajoute à la liste des oeuvres
     */
    public static void createOrUpdateAllMusicFromDB() {
        try {
            ResultSet rsMusic = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique;");
            if(rsMusic.isClosed()) {
                throw new Exception("Impossible de créer les musiques depuis la BDD : aucune musique contenue en BDD");
            }
            else {
                int idMusique = 0;
                while(rsMusic.next()) {
                    idMusique = rsMusic.getInt("id_musique");
                    Music.createMusicFromDB(idMusique);
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

	/**
	 * Supprime l'oeuvre de la BD
	 * @throws Exception
	 */
	public void deleteOeuvreInDB() throws Exception {
		try {
			int idMusic = this.isMusicInDB(this.getFile().getAbsolutePath());
			if (idMusic != 0) {
				DB.getInstance().getStatement().executeUpdate("DELETE FROM musique_collection WHERE id_musique = " + idMusic + ";");
				DB.getInstance().getStatement().executeUpdate("DELETE FROM musique_genre WHERE id_musique = " + idMusic + ";");
				DB.getInstance().getStatement().executeUpdate("DELETE FROM musique WHERE id_musique = " + idMusic + ";");
			} else{
				throw new Exception("Impossible de supprimer : l'oeuvre \"" + this.getMetaData().getMusicName() + "\" n'est pas enregistrée dans la BD");
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Recherche une oeuvre dans la BD en fonction de son nom de fichier.
	 * @param fileName : nom du fichier de l'oeuvre à rechercher
	 * @return si trouvée : oeuvre recherchée ; sinon null
	 */
	public static Oeuvre searchMusicByFileNameInDB(String fileName) throws Exception  {
		ResultSet musicAttributs = DB.getInstance().getStatement().executeQuery("SELECT id_musique, path_musique, libelle_musique FROM musique where libelle_musique = '" + fileName + "';");
		if (musicAttributs.getInt("id_musique") != 0) { //si la musique existe en BD
			Oeuvre searchedOeuvre = ListeOeuvres.getInstance().searchOeuvreByFileName(musicAttributs.getString("libelle_musique"));
			if (searchedOeuvre != null) { //Si l'oeuvre est déjà instancié, on la retourne
				return searchedOeuvre;
			} else if (new File(musicAttributs.getString("path_musique")).exists()) { //si pas instanciée et que le fichier existe sur le pc
				return Music.createMusicFromDB(musicAttributs.getInt("id_musique"));
			} else {
				throw new Exception("Erreur : La musique existe en BD mais n'est pas enregistrée sur l'ordinateur"); //TODO rajouter path de la musique
			}
		} else {
			throw new Exception("Impossible de trouver l'oeuvre " + fileName + ". Elle n'est pas enregistrée dans la BD");
		}
	}

	/**
	 * Recherche une oeuvre dans la BD en fonction de son nom de fichier.
	 * @param path : path du fichier de l'oeuvre à rechercher
	 * @return si trouvée : oeuvre recherchée ; sinon null
	 */
	public static Oeuvre searchMusicByPathInDB(String path) throws Exception  {
		ResultSet musicAttributs = DB.getInstance().getStatement().executeQuery("SELECT id_musique, path_musique, libelle_musique FROM musique where path_musique = '" + path + "';");
		if (musicAttributs.getInt("id_musique") != 0) { //si la musique existe en BD
			Oeuvre searchedOeuvre = ListeOeuvres.getInstance().searchOeuvreByFileName(musicAttributs.getString("libelle_musique"));
			if (searchedOeuvre != null) { //Si l'oeuvre est déjà instancié, on la retourne
				return searchedOeuvre;
			} else if (new File(musicAttributs.getString("path_musique")).exists()) { //si pas instanciée et que le fichier existe sur le pc
				return Music.createMusicFromDB(musicAttributs.getInt("id_musique"));
			} else {
				throw new Exception("Erreur : La musique existe en BD mais n'est pas enregistrée sur l'ordinateur"); //TODO rajouter path de la musique
			}
		} else {
			throw new Exception("Impossible de trouver l'oeuvre " + path + ". Elle n'est pas enregistrée dans la BD");
		}
	}

	/**
	 * Recherche une oeuvre dans la BD en fonction de son id.
	 * @param idMusic : id de l'oeuvre à rechercher
	 * @return si trouvée : oeuvre recherchée ; sinon null
	 */
	public static Oeuvre searchMusicByIdInDB(int idMusic) throws Exception {
		ResultSet musicAttributs = DB.getInstance().getStatement().executeQuery("SELECT id_musique, path_musique, libelle_musique FROM musique where id_musique = " + idMusic + ";");
		if(musicAttributs.getInt("id_musique") != 0) {
			Oeuvre searchedOeuvre = ListeOeuvres.getInstance().searchOeuvreByFileName(musicAttributs.getString("libelle_musique"));
			if(searchedOeuvre != null) {
				return searchedOeuvre;
			}
			else if(new File(musicAttributs.getString("path_musique")).exists()) {
				return Music.createMusicFromDB(musicAttributs.getInt("id_musique"));
			}
			else {
				throw new Exception("Erreur : La musique " + musicAttributs.getString("path_musique")  + " existe en BD mais n'est pas enregistrée sur l'ordinateur"); //TODO rajouter path de la musique
			}
		}
		else {
			throw new Exception("Impossible de trouver la musique ayant comme id :" + idMusic + ". Elle n'est pas enregistrée dans la BD");
		}
	}

	/**
	 * Retourne toutes les musiques contenues en BD dans un tableau d'oeuvres
	 * @return List<Oeuvre>
	 */
	public static List<Oeuvre> getTabMusicDB() {
		List<Oeuvre> tabAllMusique = new ArrayList<Oeuvre>();
		try {
			ResultSet rsTabOeuvre = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique"); //Selectionne toutes les musiques contenues en BDD
			Oeuvre musique;
			while(rsTabOeuvre.next()) {
				musique = ListeOeuvres.getInstance().searchOeuvreByFileName(rsTabOeuvre.getString("libelle_musique"));
				if(musique == null) { //si musique existe pas, on en crée une et on l'ajoute au tableau retourné
					File musicFile = new File(rsTabOeuvre.getString("path_musique"));
					if(!musicFile.exists()) {
						throw new Exception("Aucune musique au chemin : " + musicFile.getAbsolutePath());
					}
					else {
						musique = Music.createMusicFromDB(rsTabOeuvre.getInt("id_musique"));
						ListeOeuvres.getInstance().getOeuvreList().remove(musique);
					}
				}
				tabAllMusique.add(musique);
			}
			return tabAllMusique;
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}

	/**
	 * Retourne un tableau avec toutes les oeuvres dans la BDD dont les extensions correspondent à celles passées en param
	 * @param  listMusicType<EnumMusic> liste des extensions des musiques à retourner
	 * @return List<Oeuvre>
	 */
	public static List<Oeuvre> getArraySpecificMusicInDB(List<EnumMusic> listMusicType) {
		List<Oeuvre> tabAllMusique = new ArrayList<Oeuvre>();
		String sqlQuery = "SELECT * FROM musique";
		for(int i = 0 ; i < listMusicType.size() ; i++) { //Création de la requette SQL avec les types de musiques voulues
			if(i == 0) {
				sqlQuery += " WHERE type_musique = '"+listMusicType.get(i).getType()+"'";
			}
			sqlQuery += " OR type_musique = '"+listMusicType.get(i).getType()+"'";
		}
		try {
			ResultSet rsTabOeuvre = DB.getInstance().getStatement().executeQuery(sqlQuery);
			Oeuvre musique;
			while(rsTabOeuvre.next()) {
				musique = ListeOeuvres.getInstance().searchOeuvreByFileName(rsTabOeuvre.getString("libelle_musique"));
				if(musique == null) { //si musique existe pas, on en crée une et on l'ajoute au tableau retourné
					File musicFile = new File(rsTabOeuvre.getString("path_musique"));
					if(!musicFile.exists()) {
						throw new Exception("Aucune musique au chemin : " + musicFile.getAbsolutePath());
					}
					else {
						musique = Music.createMusicFromDB(rsTabOeuvre.getInt("id_musique"));
						ListeOeuvres.getInstance().getOeuvreList().remove(musique);
					}
				}
				tabAllMusique.add(musique);
			}
			return tabAllMusique;
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}

	/**
	 * Vérifie si la musique est dans la base de données
	 * @param musicPath
	 * @return id de la musique si existe sinon 0
	 */
	public static int isMusicInDB(String musicPath) {
		try {
			ResultSet rsMusicId = DB.getInstance().getStatement().executeQuery("SELECT id_musique from musique where path_musique='" + musicPath + "';");
			if(!rsMusicId.isClosed()) {
				return rsMusicId.getInt("id_musique");
			}
		} catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		return 0;
	}

	/**
	 * Vérifie si la musique est dans la playlist en BD
	 * @param idMusic
	 * @param idPlaylist
	 * @return true si musique dans playlist, sinon false
	 */
	public static boolean isMusicInPlaylistInDB(int idMusic, int idPlaylist) {
		try {
			DB db = DB.getInstance();
			ResultSet rsIsMusicInPlaylist = db.getStatement().executeQuery("SELECT * from musique_collection where id_collection=" + idPlaylist + " and id_musique=" + idMusic +";");
			// read the result set
			if(!rsIsMusicInPlaylist.isClosed() && rsIsMusicInPlaylist.getInt("id_collection") != 0) {
				return true;
			}
		} catch(SQLException e)
		{
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		return false;
	}

	public void insertOeuvreInIgnoredList() throws SQLException {
		ResultSet rsIgnoredMusics = DB.getInstance().getStatement().executeQuery("SELECT * FROM musiqueIgnoree WHERE path_musique='"+this.getFile().getAbsolutePath()+"';");
		while(rsIgnoredMusics.next()) {
			return;
		}
		DB.getInstance().getStatement().executeUpdate("INSERT INTO musiqueIgnoree(path_musique) VALUES ('" + this.getFile().getAbsolutePath() + "');");
	}

	public static void removeFromIgnoredList(String path) {
		try {
			DB.getInstance().getStatement().executeUpdate("DELETE FROM musiqueIgnoree WHERE path_musique='" + path + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static boolean isMusicInIngoredList(String path) {
		try {
			ResultSet rsIgnoredMusics = DB.getInstance().getStatement().executeQuery("SELECT * FROM musiqueIgnoree WHERE path_musique='" + path + "';");
			while (rsIgnoredMusics.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static ArrayList<String> getListPathIgnoredMusics() {
		ArrayList<String> listPathIgnoredMusics = new ArrayList<>();
		try {
			ResultSet rsIgnoredMusics = DB.getInstance().getStatement().executeQuery("SELECT * FROM musiqueIgnoree;");
			while (rsIgnoredMusics.next()) {
				listPathIgnoredMusics.add(rsIgnoredMusics.getString("path_musique"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listPathIgnoredMusics;
	}

	private static String assembleConditionsForSQLQuery(String fieldName, ArrayList<Integer> idTab, String operator) {
		String conditionQuery = "";
		for(int i = 0 ; i < idTab.size() ; i++) {
			if(i != 0) {
				conditionQuery += " " + operator + " " + fieldName + "=" + idTab.get(i);

			}
			else {
				conditionQuery += fieldName + "=" + idTab.get(i);
			}

		}
		return conditionQuery;
	}

	public static void updateMusic() throws Exception {
		try {
			ResultSet rsSearchDir = DB.getInstance().getStatement().executeQuery("SELECT * FROM repertoireDeRecherche");
			ArrayList<Integer> idSearchDirTab = new ArrayList<>();
			while (rsSearchDir.next()) { //Mets dans tableau tous les id des répertoires de recherche
				File searchDir = new File(rsSearchDir.getString("path"));
				if (!searchDir.exists()) {
					SearchOeuvres.deleteDirById(rsSearchDir.getInt("id_repertoire"));
				} else {
					idSearchDirTab.add(rsSearchDir.getInt("id_repertoire"));
				}
			}
			if (idSearchDirTab.isEmpty()) {
				throw new Exception("Pas de dossier de recherche enregistré");
			} else {
				ResultSet rsMusicsToDelete = DB.getInstance().getStatement().executeQuery("SELECT id_musique FROM musique WHERE id_musique NOT IN (SELECT id_musique FROM musique WHERE " + assembleConditionsForSQLQuery("id_repertoire", idSearchDirTab, "OR") + ");");
				while (rsMusicsToDelete.next()) {
					ListeOeuvres.getInstance().deleteOeuvreWithoutIgnoreOeuvre(Music.searchMusicByIdInDB(rsMusicsToDelete.getInt("id_musique")));
				}
				ArrayList<Integer> idMusicToKeepTab = new ArrayList<>();
				for (int idSearchDir : idSearchDirTab) {
					rsSearchDir = DB.getInstance().getStatement().executeQuery("SELECT path FROM repertoireDeRecherche WHERE id_repertoire = " + idSearchDir + ";");
					while (rsSearchDir.next()) {
						for (File file : SearchOeuvres.getInstance().lookForOeuvre(new File(rsSearchDir.getString("path")))) {
							if(!Music.isMusicInIngoredList(file.getAbsolutePath())) { //Si la musique est blacklisté, ne pas l'ajouter
								try {
									Music.searchMusicByPathInDB(file.getAbsolutePath()); //regarde si le fichier existe en BD : si oui le crée sinon -> catch
								} catch (Exception e) {
									try {
										ListeOeuvres.getInstance().createOeuvre(Utils.getFileName(file), file, idSearchDir);
									} catch (Exception ex) {}
								}
								try {
									idMusicToKeepTab.add(Music.getIdMusicFromPathInBDD(file.getAbsolutePath()));
								} catch (Exception e) {}
							}
						}
					}
				}
				rsMusicsToDelete = DB.getInstance().getStatement().executeQuery("SELECT id_musique FROM musique WHERE id_musique NOT IN (SELECT id_musique FROM musique WHERE " + assembleConditionsForSQLQuery("id_repertoire", idSearchDirTab, "OR") + ");");
				while (rsMusicsToDelete.next()) { //Supprime toutes les musiques qui sont en BD et pas dans les répertoires de recherche
					ListeOeuvres.getInstance().deleteOeuvreWithoutIgnoreOeuvre(Music.searchMusicByIdInDB(rsMusicsToDelete.getInt("id_musique")));
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
}
