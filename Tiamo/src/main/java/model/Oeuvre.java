package model;

import javazoom.jl.decoder.JavaLayerException;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;

/**
 * Représente une oeuvre
 * @author Flo
 *
 */
public interface Oeuvre{
	/**
	 * Retourne le type de l'oeuvre
	 * @return OeuvreType
	 */
	public OeuvreType getType();
	
	/**
	 * Défini le type de l'oeuvre
	 * @param enumMusic
	 */
	public void setType(OeuvreType enumMusic);
	
	/**
	 * Retourne le fichier
	 * @return File
	 */
	public File getFile();

	/**
	 * retourne la FileStrategy
	 */
	public FileStrategy getStrategy();

	/**
	 * Défini la stratégie
	 * @param fs
	 */
	public void setStrategy(FileStrategy fs) throws FileNotFoundException, JavaLayerException;

	/**
	 * Défini les metadata de la musique
	 * @param md
	 */
	public void setMetaData(MetaDataMusic md);

	/**
	 * Retourne les metadata de la musique
	 * @return MetaDataMusic
	 */
	public MetaDataMusic getMetaData() throws Exception;

	public void deleteOeuvreInDB() throws Exception;
	
	public String toString();

	void createOeuvreInDB(int idSearchDir) throws Exception;

	void insertOeuvreInIgnoredList() throws SQLException;
}
