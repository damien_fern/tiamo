package model;

/**
 * Type de l'oeuvre
 * @author flori
 *
 */
public interface OeuvreType {
	
	/**
	 * retourne le type de l'oeuvre
	 * @return String
	 */
	public String getType();

	/**
	 * Retourne la stratégie de l'oeuvre
	 * @return
	 */
	public FileStrategy getStrategy();
}
