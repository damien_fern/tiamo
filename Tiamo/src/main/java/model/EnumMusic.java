package model;
/**
 * 
 * @author Flo
 * Enumeration de tous les types de musique acceptés
 */
public enum EnumMusic implements OeuvreType{
	MP3 {
		/**
		 * @return String : extension de la musique sans le '.'
		 */
		public String getType() {
			return "mp3";
		}
		/**
		 * @return FileStrategy : strategie du type de fichier
		 */
		public FileStrategy getStrategy() {
			return new Mp3AndWav();
		}
	},
	WAV{
		/**
		 * @return String : extension de la musique sans le '.'
		 */
		public String getType() {
			return "wav";
		}
		/**
		 * @return FileStrategy : strategie du type de fichier
		 */
		public FileStrategy getStrategy() {
			return new Mp3AndWav();
		}
	};
}
