package model;

import model.mp3Player.StreamPlayer;
import model.mp3Player.StreamPlayerException;
import org.tritonus.share.sampled.file.TAudioFileFormat;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class Mp3AndWav implements FileStrategy {


	private StreamPlayer streamPlayer = null;

	private File mp3OrWavFile;

	/**
	 * Joue la musique mp3
	 */
	public void play() throws Exception {
		this.streamPlayer = new StreamPlayer();
		this.streamPlayer.open(this.mp3OrWavFile);
		this.streamPlayer.play();
	}

	/**
	 * Joue la musique mp3 au temps spécifié
	 */
	public void play(int startInSeconds, int musicDurationSeconds) throws Exception {
		this.streamPlayer = new StreamPlayer();
		this.streamPlayer.open(this.mp3OrWavFile);
		this.streamPlayer.play();
		if(musicDurationSeconds != 0) {
			this.goTo(startInSeconds, musicDurationSeconds);
		}

	}

	/**
	 * Reprend la lecture de la musique mp3
	 */
	public void resume() {
		this.streamPlayer.resume();
	}

	/**
	 * Met pause à la musique mp3
	 */
	public void pause() {
		this.streamPlayer.pause();
	}

	/**
	 * Set the video to the number of seconds specified in param
	 * @param timeToGoToInSeconds
	 * @param musicDurationSeconds
	 * @throws StreamPlayerException
	 */
	public void goTo(int timeToGoToInSeconds, int musicDurationSeconds) throws Exception {
		if(this.streamPlayer != null) {
			long timeInBytes = this.streamPlayer.getTotalBytes() * (long) timeToGoToInSeconds / (long) musicDurationSeconds;
			this.streamPlayer.seek(timeInBytes);
		}
	}

	/**
	 * Stop la musique mp3
	 */
	public void stop() {
		if(this.streamPlayer != null) {
			try {
				this.streamPlayer.stop();
				/*TimeUnit.MILLISECONDS.sleep(400);
				this.streamPlayer.call();*/
				this.streamPlayer.getEventsExecutorService().shutdownNow();
				if (!this.streamPlayer.getEventsExecutorService().isShutdown()) {
					this.streamPlayer.getEventsExecutorService().shutdown();
				}
				this.streamPlayer.getStreamPlayerExecutorService().shutdownNow();
				if (!this.streamPlayer.getStreamPlayerExecutorService().isShutdown()) {
					this.streamPlayer.getStreamPlayerExecutorService().shutdown();
				}
				this.streamPlayer = null;
			} catch (Exception e) {
			}
		}
	}

	@Override
	public boolean isFinished() {
		if(this.streamPlayer != null){
			return this.streamPlayer.isStopped();
		}
		return true;
	}

	public boolean isPlayling() {
		if(this.streamPlayer!=null) {
			return this.streamPlayer.isPlaying();
		}
		return false;
	}

	public boolean isPaused() {
		if(this.streamPlayer!=null) {
			return this.streamPlayer.isPaused();
		}
		return false;
	}

	@Override
	public void setFile(File file) {
		this.mp3OrWavFile = file;
	}

	public String toString() {
		return "MP3 stratégie";
	}

	public void setVolume(double vgain) {
		if(this.streamPlayer != null) {
			this.streamPlayer.setGain(vgain);
		}
	}

	public float getMaxVolume() throws Exception {
		if(this.streamPlayer != null) {
			return this.streamPlayer.getMaximumGain();
		}
		throw new Exception("Impossible d'obtenir le volume maximum");
	}

	public float getMinimumVolume() throws Exception {
		if(this.streamPlayer != null) {
			return this.streamPlayer.getMaximumGain();
		}
		throw new Exception("Impossible d'obtenir le volume minimum");
	}

	public int getMusicDurationInSeconds() throws UnsupportedAudioFileException, IOException {
		if(Utils.getExtension(this.mp3OrWavFile).equals(EnumMusic.MP3.getType())) {
			AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(this.mp3OrWavFile);
			if (fileFormat instanceof TAudioFileFormat) {
				Map<?, ?> properties = ((TAudioFileFormat) fileFormat).properties();
				String key = "duration";
				Long microseconds = (Long) properties.get(key);
				int sec = (int) (microseconds / 1000000);
				return sec;
			} else {
				throw new UnsupportedAudioFileException();
			}
		}
		else if(Utils.getExtension(this.mp3OrWavFile).equals(EnumMusic.WAV.getType())) {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(this.mp3OrWavFile);
			AudioFormat format = audioInputStream.getFormat();
			long audioFileLength = this.mp3OrWavFile.length();
			int frameSize = format.getFrameSize();
			float frameRate = format.getFrameRate();
			float durationInSeconds = (audioFileLength / (frameSize * frameRate));
			return (int) durationInSeconds;
		}
		else {
			throw new UnsupportedAudioFileException();
		}
	}

	public int getCurrentTimeSeconds(int musicDurationSeconds) {
		if(this.streamPlayer != null) {
			return (int) (musicDurationSeconds * this.streamPlayer.getEncodedStreamPosition() / this.streamPlayer.getTotalBytes());
		}
		return 0;
	}
}