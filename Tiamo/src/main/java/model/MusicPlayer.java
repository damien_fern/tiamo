package model;

import javazoom.jl.player.Player;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class MusicPlayer {
	
	private FixedOeuvreStack tabLecture;
	private FileStrategy globalPlayer;
	private PlayerState fileState = PlayerState.STOP;
	private Thread musicPlayed = null;
	private Lock lock;
	private List<Oeuvre> readingList;

	private static MusicPlayer instance = null;
	PropertyChangeSupport changeSupport;

	private boolean automaticReading = false;
	private boolean randomReading = true;

	/**
	 * Permet de gérer le player de musique
	 */
	private MusicPlayer() {
		this.readingList = ListeOeuvres.getInstance().getOeuvreList();
		try {
			this.tabLecture = new FixedOeuvreStack(10);
		} catch (Exception e) {
			e.printStackTrace();
		}
		globalPlayer = null;
		lock = new LockProxy<ReentrantLock>();
		changeSupport = new PropertyChangeSupport(this);
	}

	/**
	 * Méthode permettant de renvoyer une instance de la classe Singleton
	 * @return Retourne l'instance du singleton.
	 */
	public final static MusicPlayer getInstance() {
		//Le "Double-Checked Singleton"/"Singleton doublement vérifié" permet
		//d'éviter un appel coûteux à synchronized,
		//une fois que l'instanciation est faite.
		if (MusicPlayer.instance == null) {
			// Le mot-clé synchronized sur ce bloc empêche toute instanciation
			// multiple même par différents "threads".
			// Il est TRES important.
			synchronized(MusicPlayer.class) {
				if (MusicPlayer.instance == null) {
					MusicPlayer.instance = new MusicPlayer();
				}
			}
		}
		return MusicPlayer.instance;
	}

	public boolean isAutomaticReading() {
		return automaticReading;
	}

	public boolean isRandomReading() {
		return randomReading;
	}

	public PlayerState getFileState() {
		return fileState;
	}

	/**
	 * Lance la lecture de l'oeuvre en param en vérifiant qu'il y a bien des oeuvres enregistrées et que son status n'est pas sur play
	 * @param oeuvre
	 */
	private void startPlayOeuvre(Oeuvre oeuvre) {
		try {
			Oeuvre currentOeuvre;
			try{
				currentOeuvre = getCurrentOeuvre();
			}
			catch (Exception exception)
			{
				currentOeuvre = null;
			}
			if(this.tabLecture.nbElements() > 0) { //si au moins une oeuvre a déjà été jouée et l'oeuvre n'est pas en cours de lecture
				try {
					if (this.tabLecture.getLastOeuvre() != oeuvre) { //si la dernière oeuvre jouée n'est pas l'oeuvre actuelle
						this.globalPlayer = oeuvre.getStrategy();
						this.tabLecture.push(oeuvre);
						//this.tabLecture.getLastOeuvre().play();
						this.globalPlayer.play(oeuvre.getMetaData().getStartAtInSeconds(), oeuvre.getMetaData().getMusicDurationSeconds());
						this.fileState = PlayerState.PLAY;
						changeSupport.firePropertyChange("oeuvre", fileState, oeuvre);
					} else { //si l'oeuvre est la même que celle en lecture
						if (this.fileState == PlayerState.PAUSE) {
							//getCurrentOeuvre().resume();
							this.globalPlayer.resume();
							this.fileState = PlayerState.RESUME;
							changeSupport.firePropertyChange("oeuvre", fileState, oeuvre);
						} else if (this.fileState == PlayerState.STOP && globalPlayer != null) {
							//getCurrentOeuvre().play();
							this.globalPlayer.play(oeuvre.getMetaData().getStartAtInSeconds(), oeuvre.getMetaData().getMusicDurationSeconds());
							this.fileState = PlayerState.PLAY;
							changeSupport.firePropertyChange("oeuvre", fileState, oeuvre);
						} else if (this.globalPlayer == null) {
							this.globalPlayer = oeuvre.getStrategy();
							this.globalPlayer.play(oeuvre.getMetaData().getStartAtInSeconds(), oeuvre.getMetaData().getMusicDurationSeconds());
							this.fileState = PlayerState.PLAY;
							changeSupport.firePropertyChange("oeuvre", fileState, oeuvre);
						}
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if(this.tabLecture.nbElements() == 0) {
				this.tabLecture.push(oeuvre);
				this.globalPlayer = oeuvre.getStrategy();
				this.globalPlayer.play(oeuvre.getMetaData().getStartAtInSeconds(), oeuvre.getMetaData().getMusicDurationSeconds());
				this.fileState = PlayerState.PLAY;
				changeSupport.firePropertyChange("oeuvre", fileState, oeuvre);
			}
		}catch(Exception e) {
			System.err.print("Impossible de lancer la lecture de la musique \""+oeuvre.getFile().getName()+"\" : ");
			e.printStackTrace();
		}finally {
			this.lock.unlock();
		}
	}

	/**
	 * Attend que l'oeuvre en cours se termine, puis tue le player
	 */
	private void waitEndOfPlaying() throws Exception {
		try {
			while (this.globalPlayer!=null && !this.globalPlayer.isFinished()) {
				Thread.sleep(50);
			}
			this.globalPlayer.stop(); //stop pour tuer le thread du player mp3
			this.fileState = PlayerState.STOP;
			changeSupport.firePropertyChange("oeuvre", fileState, this.getCurrentOeuvre());
		} catch (InterruptedException e) {
			throw new InterruptedException();
		} catch (Exception e) {
			throw new Exception("Impossible de stopper la lecture de la musique : " + e.getMessage());
		}
	}

	/**
	 * Attend que l'oeuvre en cours se termine, puis tue le player
	 */
	public void stopMusicPlayer() throws Exception {
		try {
			this.globalPlayer.stop(); //stop pour tuer le thread du player mp3
			this.fileState = PlayerState.STOP;
			changeSupport.firePropertyChange("oeuvre", fileState, this.getCurrentOeuvre());
		} catch (Exception e) {
			throw new Exception("Impossible de stopper la lecture de la musique : " + e.getMessage());
		}
	}

	/**
	 * Lance la lecture de l'oeuvre en param en vérifiant qu'il y a bien des oeuvres enregistrées et que son status n'est pas sur play
	 * @param oeuvre
	 */
	private void playMusic(Oeuvre oeuvre) {
		List<Oeuvre> randomListOeuvre = new ArrayList<>(this.readingList);
		Oeuvre oeuvreToPlay;
		int oeuvreNumber;
		if(oeuvre == null) { //si l'oeuvre à lire est null, on prend une oeuvre dans tableau d'oeuvres (si on veut jouer une musique random)
			if(this.readingList.isEmpty()) {
				this.lock.unlock();
				return;
			}
			oeuvreNumber = (int)(Math.random() * (randomListOeuvre.size()));
			oeuvreToPlay = randomListOeuvre.get(oeuvreNumber);
		}
		else {
			if(randomListOeuvre.contains(oeuvre)) {
				oeuvreNumber = randomListOeuvre.indexOf(oeuvre);
			}
			else {
				oeuvreNumber = (int)(Math.random() * (randomListOeuvre.size()));
			}
			oeuvreToPlay = oeuvre;
		}
		try {
			while (true) {
				startPlayOeuvre(oeuvreToPlay);
				randomListOeuvre.remove(this.tabLecture.getLastOeuvre()); //retire l'oeuvre pour pas la lire de nouveau
				waitEndOfPlaying();
				if(!automaticReading) { //si plus en lecture automatique, on sort de la boucle
					break;
				}
				if (randomListOeuvre.isEmpty()) { //si tableau d'oeuvre vide, on le remplit de nouveau
					if(this.readingList.isEmpty()) {
						return;
					}
					randomListOeuvre.addAll(this.readingList);
				}
				if(this.randomReading) { //Définir l'oeuvre suivante à lire
					oeuvreNumber = (int) (Math.random() * (randomListOeuvre.size()));

				}else{
					if(oeuvreNumber == 0) {}
					else if(oeuvreNumber == randomListOeuvre.size()-1) { //si on a joué l'avant dernière oeuvre
						oeuvreNumber = randomListOeuvre.size()-1;
					}
					else if(oeuvreNumber > randomListOeuvre.size()-1) { //si on a jouer la dernière oeuvre
						oeuvreNumber = 0;
					}
					else {
						oeuvreNumber++;
					}
				}
				oeuvreToPlay = randomListOeuvre.get(oeuvreNumber);
			}
		}catch (InterruptedException e) {
			//appel de la fonction stop : finally --> on tue le thread
		}
		catch (Exception e) {
			System.err.print("Impossible de lancer la lecture de la musique : ");
			e.printStackTrace(); //que si pas d'oeuvre courrante
		}
		finally {
			Thread.currentThread().interrupt();//fin de musique : on tue le thread
		}
	}

	/**
	 * Arrete la musique en cours dans le but de jouer la musique en paramètre
	 * @param oeuvreToPlay
	 */
	private void stopPlayingMusicToPlayAnother(Oeuvre oeuvreToPlay) {
		Oeuvre currentOeuvre = null;
		try {
			currentOeuvre = getCurrentOeuvre();
		} catch (Exception e) {
			currentOeuvre = null;
		}
		if(oeuvreToPlay != currentOeuvre) { //L'oeuvre à jouer est l'oeuvre n'est pas celle en cours de lecture
			try {
				if (globalPlayer != null) {
					if(this.fileState != PlayerState.STOP) {
						this.globalPlayer.stop();
						this.fileState = PlayerState.STOP;
						changeSupport.firePropertyChange("oeuvre", fileState, this.getCurrentOeuvre());
					}
				}
				this.globalPlayer = null;
			} catch(Exception e) {
				//Erreur que s'il n'y a pas d'oeuvre en cours de lecture
			}
		}
		if(this.musicPlayed != null && this.musicPlayed.isAlive()) {
			this.musicPlayed.interrupt();
		}
	}

	/**
	 * Retourne la dernière oeuvre ajoutée à la pile de lecture
	 * @return
	 */
	public Oeuvre getCurrentOeuvre() throws Exception {
		return this.tabLecture.getLastOeuvre();
	}

	/**
	 * Définir si lecture automatique est activée
	 * @param automaticReading
	 */
	public void setAutomaticReading(boolean automaticReading) {
		this.automaticReading = automaticReading;
	}

	/**
	 * Définir si la lecture de la musique suivante est random ou non
	 * @param randomReading
	 */
	public void setRandomReading(boolean randomReading) {
		this.randomReading = randomReading;
	}


	public void togglePlayPauseCurrent(){
		PlayerState playerState = getFileState();
		switch (playerState) {
			case PLAY:
				pauseCurrentOeuvre();
				break;
			case RESUME:
				pauseCurrentOeuvre();
				break;
			case PAUSE:
				try {
					play(getCurrentOeuvre());
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case STOP:
				try {
					play(getCurrentOeuvre());
				} catch (Exception e) {}
				break;
		}
	}
	/**
	 * Mets l'oeuvre en cours de lecture sur pause
	 */
	public void pauseCurrentOeuvre() {
		if(this.lock.tryLock()) {
			try {
				if(this.fileState == PlayerState.PLAY || this.fileState == PlayerState.RESUME ) {
					this.globalPlayer.pause();
					this.musicPlayed.interrupt(); //on coupe le thread de cette classe pour pas qu'il tourne en boucle pendant la pause.
												  //!!Le thread du player n'est pas tué!!
					this.fileState = PlayerState.PAUSE;
					changeSupport.firePropertyChange("oeuvre", fileState, this.getCurrentOeuvre());
				}
			} catch(Exception e) {
				System.err.print("Impossible de mettre pause : ");
				e.printStackTrace();
			} finally {
				this.lock.unlock();
			}
		}
		else {
			try {
				throw new Exception("Impossible de mettre pause à la lecture : une fonction est déjà en cours d'exécution"); //TODO: enlever le try catch et mettre throw dans décla fonction
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Lance la lecture de l'oeuvre suivante par rapport à celle en cours de lecture
	 */
	public void playNextOeuvre() {
		if(this.lock.tryLock()) {
			stopPlayingCurrentMusic();
			Oeuvre currentOeuvre;
			Oeuvre nextOeuvre = null;
			try {
				currentOeuvre = this.getCurrentOeuvre();
				if(this.readingList.isEmpty()) {
					nextOeuvre = currentOeuvre;
				}
				else {
					int indexCurrentOeuvre = this.readingList.indexOf(currentOeuvre);
					if (indexCurrentOeuvre != -1) {
						if (this.readingList.size() < 2) {
							nextOeuvre = currentOeuvre;
						} else if (indexCurrentOeuvre == this.readingList.size() - 1) {
							nextOeuvre = this.readingList.get(0);
						} else {
							nextOeuvre = this.readingList.get(indexCurrentOeuvre + 1);
						}

					} else {
						if(this.randomReading) {
							nextOeuvre = this.readingList.get((int) (Math.random() * (this.readingList.size())));
						}
						else {
							nextOeuvre = this.readingList.get(0);
						}
					}
				}
			} catch (Exception e) {
				if(!this.readingList.isEmpty()) {
					nextOeuvre = this.readingList.get(0);
				}
				else {
					try {
						throw new Exception("Impossible de lancer l'oeuvre suivante : aucune oeuvre dans la liste");
					} catch (Exception e1) {
						e1.printStackTrace();
						this.lock.unlock();
						return;
					}
				}
			}
			startPlayThread(nextOeuvre);
		}
		else {
			try {
				throw new Exception("Impossible de lancer la musique suivante : une fonction est déjà en cours d'exécution"); //TODO: enlever le try catch et mettre throw dans décla fonction
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Lance la lecture de l'oeuvre précédente à celle en cours
	 */
	public void playPreviusOeuvre() {
		if(this.lock.tryLock()) {
			if(this.tabLecture.nbElements() < 2) {
				try {
					throw new Exception("Impossible le lire l'oeuvre précendante, il n'y en a aucune en mémoire");
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}
			}
			stopPlayingCurrentMusic();
			this.tabLecture.pop();
			Oeuvre previusOeuvre = null;
			try {
				previusOeuvre = getCurrentOeuvre();
			} catch (Exception e) {
				System.err.print("Impossible de lire l'oeuvre précédente : ");
				e.printStackTrace();
				return;
			}
			startPlayThread(previusOeuvre);
		}
		else {
			try {
				throw new Exception("Impossible de lancer la musique précédente : une fonction est déjà en cours d'exécution"); //TODO: enlever le try catch et mettre throw dans décla fonction
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Arrete la musique en cours dans le but de jouer la musique en paramètre
	 */
	private void stopPlayingCurrentMusic() {
		if (this.globalPlayer != null) {
			if(this.fileState != PlayerState.STOP) {
				try {
					this.globalPlayer.stop();
					this.fileState = PlayerState.STOP;
					changeSupport.firePropertyChange("oeuvre", fileState, this.getCurrentOeuvre());
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		if(this.musicPlayed != null && this.musicPlayed.isAlive()) {
			this.musicPlayed.interrupt();
		}
		this.globalPlayer = null;
	}

	private void startPlayThread(Oeuvre oeuvre) { //TODO: faire en sorte de mettre que les musiques et pas d'autres oeuvres / Voir si jamais on ajoute oeuvre dans playlist pendant lecture
		final Runnable r = () -> {
			playMusic(oeuvre);
		};
		this.musicPlayed = new Thread(r);
		musicPlayed.setDaemon(false);
		musicPlayed.setPriority(Thread.MAX_PRIORITY);
		musicPlayed.start();
	}

	/**
	 * Jouer une oeuvre qui est dans une autre collection d'oeuvre que l'oeuvre précédente
	 * @param oeuvreToPlay oeuvre à jouer immédiatement
	 * @param newListOeuvreToPlay nouvelle liste de musique à jouer pour les musiques suivantes
	 */
	public void play(Oeuvre oeuvreToPlay, List<Oeuvre> newListOeuvreToPlay) {
		try {
			if(oeuvreToPlay == getCurrentOeuvre() && (this.fileState == PlayerState.PLAY || this.fileState == PlayerState.RESUME)) {
				return;
			}
		} catch (Exception ignored) { //que si pas d'oeuvre courrante
		}
		if(this.lock.tryLock()) {
			stopPlayingMusicToPlayAnother(oeuvreToPlay);
			if(newListOeuvreToPlay != null) {
				this.readingList = newListOeuvreToPlay;
			}
			startPlayThread(oeuvreToPlay);
		}
		else {
			try {
				throw new Exception("Impossible de lancer la lecture : une fonction est déjà en cours d'exécution"); //TODO: enlever le try catch et mettre throw dans décla fonction
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Jouer une oeuvre (la liste de lecture pour les musiques suivantes reste la même)
	 * @param oeuvreToPlay
	 */
	public void play(Oeuvre oeuvreToPlay) {
		play(oeuvreToPlay, null);
	}

	public void stop() {
		if(this.lock.tryLock()) {
			stopPlayingCurrentMusic();
			this.lock.unlock();
		}
		else {
			try {
				throw new Exception("Impossible d'arreter la lecture : une fonction est déjà en cours d'exécution"); //TODO: enlever le try catch et mettre throw dans décla fonction
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void goTo(int timeToGoToInSeconds) {
		if(this.globalPlayer != null && this.getFileState() != PlayerState.STOP) {
			try {
				if(this.lock.tryLock()) {
					this.globalPlayer.goTo(this.getCurrentOeuvre().getMetaData().getStartAtInSeconds() + timeToGoToInSeconds, this.getCurrentOeuvre().getMetaData().getMusicDurationSeconds());
					this.lock.unlock();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else {
			System.err.println("Impossible de changer le temps de la musique tant qu'elle n'est pas lancée");
		}
	}

	public PlayerState getPlayerState() {
		if(this.globalPlayer.isFinished()) {
			return PlayerState.STOP;
		}
		else if(this.globalPlayer.isPaused()) {
			return PlayerState.PAUSE;
		}
		else if(this.globalPlayer.isPlayling()) {
			return PlayerState.PLAY;
		}
		return this.getFileState();
	}

	public Float getMaxVolume() {
		if(this.globalPlayer != null && this.getFileState() != PlayerState.STOP) {
			try {
				return this.globalPlayer.getMaxVolume();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0.0F;
	}

	public Float getMinimumVolume() {
		if(this.globalPlayer != null && this.getFileState() != PlayerState.STOP) {
			try {
				return this.globalPlayer.getMinimumVolume();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0.0F;
	}

	public int getCurrentTimePosition() {
		try {
			return this.globalPlayer.getCurrentTimeSeconds(this.getCurrentOeuvre().getMetaData().getMusicDurationSeconds());
		} catch (Exception e) {
			return 0;
		}
	}

	public int getDuration() {
		try {
			return this.getCurrentOeuvre().getMetaData().getMusicDurationSeconds();
		} catch (Exception e) {
			return 0;
		}
	}

	public List<Oeuvre> getReadingList() {
		return readingList;
	}

	public void setReadingList(List<Oeuvre> readingList) {
		this.readingList = readingList;
	}

	public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
		changeSupport.addPropertyChangeListener(listener);
	}
}
