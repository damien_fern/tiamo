package model;

public enum EnumMusicKind {
    AFRO_BEAT("Afro Beat"),AMBIENT("Ambient"),BLACK_METAL("Black Métal"),
    BLUES("Blues"),BOSSA_NOVA("Bossa Nova"),CHANSON_FRANCAISE("chanson française"),
    CHANTS_POPULAIRES("Chants populaires"),CHORALE("Chorale"),COLD_WAVE("Cold Wave"),
    COMEDIE_MUSICALE("Comédie Musicale"),COUNTRY("Country"),DANCE("Dance"),
    DEATH_METAL("Death Métal"),DISCO("Disco"),DRUMNBASS("Drum'n'Bass"),
    DUB("Dub"),ELECTRO("Electro"),ELECTRO_FOLK("Electro folk"),
    ELECTRO_POP("Electro Pop"),ELECTRO_CLUBBING("Electro Clubbing"),ELECTRONICA("Electronica"),
    EMO_ROCK("Emo rock"),FOLK("Folk"),FOLK_ROCK("Folk Rock"),
    FUNK("Funk"),FUSION("Fusion"),GLAM_ROCK("Glam Rock"),
    GOSPEL("Gospel"),GOTHIC_METAL("Gothic Metal"),GRIND("Grind"),
    GROOVE_RNB("Groove RNB"),GRUNGE("Grunge"),HARD_ROCK("Hard rock"),
    HARDCORE("Hardcore"),HEAVY_METAL("Heavy Metal"),HIPHOP_FRANÇAIS("Hip-Hop Français"),
    HOUSE("House"),HUMOUR("Humour"),INDIE_ROCK("Indie Rock"),
    INDUS("Indus"),JAZZ("Jazz"),JAZZ_MANOUCHE("Jazz Manouche"),
    LAID_BACK("Laid Back"),LATINO("Latino"),LOUNGE("Lounge"),
    METAL("Métal"),METAL_PROG("Metal Prog"),MUSIQUE_CLASSIQUE("Musique Classique"),
    MUSIQUE_CONTEMPORAINE("Musique Contemporaine"),MUSIQUE_GITANE("Musique gitane"),MUSIQUE_ORIENTALE("Musique Orientale"),
    MUSIQUE_TRADITIONNELLE("Musique traditionnelle"),MUSIQUES_DE_FILMS("Musiques de films"),NEO_METAL("Néo Metal"),
    NEW_AGE("New Age"),NEW_WAVE("New Wave"),OPERA("Opera"),
    POP("Pop"),POST_PUNK("Post Punk"),POST_ROCK("Post Rock"),
    POWER_METAL("Power Metal"),PUNK("Punk"),RAGGA_DANCEHALL("Ragga Dancehall"),
    RAI("Raï"),RAP_HIPHOP("Rap Hip-Hop"),REGGAE("Reggae"),
    REGGAETON("Reggaeton"),ROCK("Rock"),ROCK_FUSION("Rock - Fusion"),
    ROCK_ALTERNATIF("Rock Alternatif"),ROCK_FRANCAIS("Rock Français"),ROCK_PROGRESSIF("rock progressif"),
    ROCK_PSYCHEDELIQUE("Rock psychédélique"),ROCKNROLL("Rock'n'roll"),ROCKSKA("Rock-ska"),
    RYTHMNBLUES("Rythm'n'blues"),SALSA("Salsa"),SCENE_FRANCAISE("Scène française"),
    SHOEGAZE("Shoegaze"),SITAR("Sitar"),SKA("Ska"),
    SLAM("Slam"),SLOW("Slow"),SOUL("Soul"),
    STONER("Stoner"),SWING("Swing"),TECHNO("Techno"),
    TRASH_METAL("Trash Metal"),TRIPHOP("Trip-Hop"),VARIETES("Variétés"),
    WORLD("World"),ZOUK("Zouk");

    private final String toString;

    private EnumMusicKind(String toString) {
        this.toString = toString;
    }

    public String toString(){
        return toString;
    }
}
