package model;

import java.io.File;

/**
 * 
 * @author Flo
 * Classe abstraite de la factory pour créer des oeuvres
 */
public interface AbstractFactory {
	
	/**
	 * Creer une oeuvre
	 * @return Oeuvre : oeuvreCréé
	 * @throws Exception
	 */
	public Oeuvre createOeuvre(String fileName, int idSearchDir) throws Exception;
	
	/**
	 * Défini le fichier qui va être lié à l'oeuvre créer
	 * @param file
	 */
	public void setFile(File file);
}
