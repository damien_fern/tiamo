package model;

import java.io.File;

/**
 * 
 * @author Flo
 * Factorie de musique
 */
public class FactoryMusic implements AbstractFactory{

	/**
	 * Fichier présent sur l'ordinateur
	 */
	private File file;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	/**
	 * Crée la musique avec le type de fichier en fonction de l'extension du fichier
	 * @return Music
	 */
	public Oeuvre createOeuvre(String fileName, int idSearchDir) throws Exception {
		if(file == null) {
			throw new Exception("Musique non définie");
		}
		Oeuvre oeuvre = new Music(this.file);
		EnumMusic musicExtension = Utils.getEnumFromExtensionMusic(Utils.getExtension(this.file));
		oeuvre.setType(musicExtension);
		oeuvre.setStrategy(musicExtension.getStrategy());
		//MetaDataMusic metaData = new MetaDataMusic(Utils.getFileName(oeuvre.getFile()));
		MetaDataMusic metaData = new MetaDataMusic(fileName);
		metaData.setMusicDurationSeconds(oeuvre.getStrategy().getMusicDurationInSeconds());
		oeuvre.setMetaData(metaData);
		try {
			oeuvre.createOeuvreInDB(idSearchDir);
		} catch(Exception e) {}
		return oeuvre;
	}

}
