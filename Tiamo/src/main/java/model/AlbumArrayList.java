package model;

import java.util.ArrayList;
import java.util.Iterator;

public class AlbumArrayList extends ArrayList<Album> {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        String stringList = "";
        Iterator<Album> iterator = this.iterator();
        while (iterator.hasNext()) {
            Album album = iterator.next();
            stringList += album.getName();
            //Do stuff
            if (iterator.hasNext()) {
                stringList += ", ";
            }
        }
        return stringList;
    }

}
