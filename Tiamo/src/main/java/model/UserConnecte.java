package model;

import java.net.ConnectException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UserConnecte extends User{
    private static UserConnecte ourInstance = new UserConnecte();

    public static UserConnecte getInstance() {
        return ourInstance;
    }

    private UserConnecte() {
    }

    public String getToken() throws ConnectException {
        try {
            int idUserTable = getIdofUserTableDB();
            ResultSet rsUser = DB.getInstance().getStatement().executeQuery("SELECT * FROM user WHERE id_user = " + idUserTable + ";");
            while (!rsUser.isClosed() && rsUser.next()) {
                String dateCreationTokenDB = rsUser.getString("dateCreationToken_user");
                if (dateCreationTokenDB != null && !"".equals(dateCreationTokenDB)) {
                    if (this.dateCreationToken != null) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(this.dateCreationToken);
                        cal.add(Calendar.DATE, 1);
                        if (cal.getTime().compareTo(new Date()) > 0) {
                            return this.token;
                        } else {
                            DB.getInstance().getStatement().executeUpdate("DELETE user;");
                            this.token = null;
                            this.dateCreationToken = null;
                            throw new ConnectException("La date d'expiration du token est passée");
                        }
                    } else {
                        String tokenInDB = rsUser.getString("token_user");
                        Date dateInDB = null;
                        try {
                            dateInDB = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateCreationTokenDB);
                        } catch (ParseException e) {}
                        if (dateInDB != null && new Date().compareTo(dateInDB) > 0) {
                            this.dateCreationToken = dateInDB;
                            this.token = tokenInDB;
                            return this.token;
                        } else {
                            DB.getInstance().getStatement().executeUpdate("DELETE user;");
                            throw new ConnectException("La date d'expiration du token est passée");
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new ConnectException("Aucun token en BDD");
    }

    private void setTokenAndDate(String token, Date date) {
        this.token = token;
        this.dateCreationToken = date;
    }

    private int getIdofUserTableDB() throws Exception {
        try {
            ResultSet rsUser = DB.getInstance().getStatement().executeQuery("SELECT * FROM user;");
            while (!rsUser.isClosed() && rsUser.next()) {
                return rsUser.getInt("id_user");
            }
            DB.getInstance().getStatement().executeUpdate("INSERT INTO user (token_user) VALUES ('0')");
            rsUser = DB.getInstance().getStatement().executeQuery("SELECT * FROM user;");
            while (!rsUser.isClosed() && rsUser.next()) {
                return rsUser.getInt("id_user");
            }
            rsUser.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new Exception("Impossible de trouver l'instance de la table user en BDD");
    }

    public void updateToken(String token) {
        try {
            int idUserTable = getIdofUserTableDB();
            DB.getInstance().getStatement().executeUpdate("UPDATE user SET token_user = '"+token+"', dateCreationToken_user = DateTime('now') WHERE id_user = "+idUserTable+";");
        } catch (Exception e) {
            e.printStackTrace();
        }
        setTokenAndDate(token, new Date());
    }

    public void removeTokenAndDate() {
        try {
            int idUserTable = getIdofUserTableDB();
            DB.getInstance().getStatement().executeUpdate("UPDATE user SET token_user = '', dateCreationToken_user = null WHERE id_user = "+idUserTable+";");
            this.token = null;
            this.dateCreationToken = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMainInfo(String username, String token, Date dateCreation)
    {
        setDateCreation(dateCreation);
        this.updateToken(token);
        setUsername(username);
    }

}
