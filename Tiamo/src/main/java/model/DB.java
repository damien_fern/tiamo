package model;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class DB {
    private Connection connection = null;
    private Statement statement = null;
    private static DB instance = new DB();

    /**
     * Méthode permettant de renvoyer une instance de la classe Singleton
     * @return Retourne l'instance du singleton.
     */
    public final static DB getInstance() {
        if (DB.instance == null) {
            synchronized(DB.class) {
                if (DB.instance == null) {
                    DB.instance = new DB();
                }
            }
        }
        return DB.instance;
    }

    private DB() {
        try {
            // create a database connection
            this.connection = DriverManager.getConnection("jdbc:sqlite:TiamoDB.db");
            this.statement = this.connection.createStatement();
            this.statement.setQueryTimeout(30);  // set timeout to 30 sec.
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS artiste ( id_artiste integer PRIMARY KEY AUTOINCREMENT, nom_artiste text NOT NULL, date_naissance_artiste date, pays_artiste text, groupe_musique_artiste text);");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS album ( id_album integer PRIMARY KEY AUTOINCREMENT, nom_album text NOT NULL, date_parution_album date);");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS metadata_custom ( id_metadata integer PRIMARY KEY AUTOINCREMENT, type_metadata text NOT NULL, libelle_metadata text NOT NULL );");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS repertoireDeRecherche ( id_repertoire integer PRIMARY KEY AUTOINCREMENT, path text NOT NULL );");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS typeCollection ( id_type_collection integer PRIMARY KEY AUTOINCREMENT, libelle_type_collection text NOT NULL );");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS collection ( id_collection integer PRIMARY KEY AUTOINCREMENT, libelle_collection text NOT NULL, id_type_collection integer, FOREIGN KEY (id_type_collection) REFERENCES typeCollection (id_type_collection) ON DELETE CASCADE ON UPDATE CASCADE );");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS genre ( id_genre integer PRIMARY KEY AUTOINCREMENT, libelle_genre text NOT NULL );");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS musique ( id_musique integer PRIMARY KEY AUTOINCREMENT, libelle_musique text NOT NULL, path_musique text NOT NULL, duree_musique time NOT NULL, miniature_path text, type_musique text NOT NULL, id_repertoire integer, id_artiste integer, id_album integer, id_metadata_custom integer, id_musique_server integer, startAt_musique integer, endAt_musique integer, isLiked integer DEFAULT 0, FOREIGN KEY (id_artiste) REFERENCES artiste (id_artiste) ON DELETE CASCADE ON UPDATE CASCADE FOREIGN KEY (id_album) REFERENCES album (id_album) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY (id_metadata_custom) REFERENCES metadata_custom (id_metadata) ON DELETE CASCADE ON UPDATE CASCADE );");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS musiqueIgnoree ( id_musique integer PRIMARY KEY AUTOINCREMENT, path_musique text NOT NULL);");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS musique_collection ( id_musique integer NOT NULL, id_collection integer NOT NULL, FOREIGN KEY (id_musique) REFERENCES musique (id_musique) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY (id_collection) REFERENCES collection (id_collection) ON DELETE CASCADE ON UPDATE CASCADE ); ");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS musique_genre ( id_musique integer NOT NULL, id_genre integer NOT NULL, FOREIGN KEY (id_musique) REFERENCES musique (id_musique) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY (id_genre) REFERENCES genre (id_genre) ON DELETE CASCADE ON UPDATE CASCADE );");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS user(id_user integer PRIMARY KEY AUTOINCREMENT, token_user text NOT NULL, dateCreationToken_user DATE);");
            this.deleteMusicsNotInComputer();
            this.deleteDirNotInComputer();
        }
        catch(SQLException e)
        {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
    }

    public Statement getStatement() {
        try {
            return this.connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void closeDB() {
        try
        {
            if(this.connection != null)
                this.connection.close();
        }
        catch(SQLException e)
        {
            // connection close failed.
            System.err.println(e);
        }
    }

    public void deleteMusicsNotInComputer() {
        try {
            ResultSet rsMusics = this.getStatement().executeQuery("SELECT * FROM musique;");
            // read the result set
            if(!rsMusics.isClosed()) {
                String musicPath;
                int musicId;
                File file;
                List<Integer> tabIdMusicToDelete = new ArrayList();
                while(rsMusics.next()) {
                    musicPath = rsMusics.getString("path_musique");
                    file = new File(musicPath);
                    if (!file.exists()) {
                        tabIdMusicToDelete.add(rsMusics.getInt("id_musique"));
                    }
                }
                for(int idMusicToDelete : tabIdMusicToDelete) {
                    ResultSet rsCollection = this.getStatement().executeQuery("SELECT * FROM musique_collection WHERE id_musique = " + idMusicToDelete + ";");
                    while (rsCollection.next()) {
                        DB.getInstance().getStatement().executeUpdate("DELETE FROM musique_collection WHERE id_collection = " + rsCollection.getInt("id_collection") + ";");
                    }
                    ResultSet rsGenre = this.getStatement().executeQuery("SELECT * FROM musique_genre WHERE id_musique = " + idMusicToDelete + ";");
                    while (rsGenre.next()) {
                        DB.getInstance().getStatement().executeUpdate("DELETE FROM musique_genre WHERE id_genre = " + rsGenre.getInt("id_genre") + ";");
                    }
                    this.getStatement().executeUpdate("DELETE FROM musique WHERE id_musique = " + idMusicToDelete + ";");
                }
            }
        } catch(SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
    }

    public void deleteDirNotInComputer() {
        try {
            ResultSet rsDir = this.getStatement().executeQuery("SELECT * FROM repertoireDeRecherche;");
            // read the result set
            if(!rsDir.isClosed()) {
                String dirPath;
                int dirId;
                File file;
                List<Integer> tabIdDirToDelete = new ArrayList();
                while(rsDir.next()) {
                    dirPath = rsDir.getString("path");
                    file = new File(dirPath);
                    if (!file.exists()) {
                        tabIdDirToDelete.add(rsDir.getInt("id_repertoire"));
                    }
                }
                for(int idDirToDelete : tabIdDirToDelete) {
                    this.getStatement().executeUpdate("DELETE FROM repertoireDeRecherche WHERE id_repertoire = " + idDirToDelete + ";");
                }
            }
        } catch(SQLException e) {
            System.err.println(e.getMessage());
        }
    }
}