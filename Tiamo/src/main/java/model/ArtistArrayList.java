package model;

import java.util.ArrayList;
import java.util.Iterator;

public class ArtistArrayList extends ArrayList<Artist> {
    private static final long serialVersionUID = 1L;

    @Override public String toString()
    {
        String stringList = "";
        Iterator<Artist> iterator = this.iterator();
        while (iterator.hasNext()) {
            Artist artist = iterator.next();
            stringList += artist.getName();
            //Do stuff
            if (iterator.hasNext()) {
                stringList += ", ";
            }
        }
        return stringList;
    }
}
