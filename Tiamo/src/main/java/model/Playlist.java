package model;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Flo
 * Représente une playlist d'oeuvres
 */
public class Playlist {
	
	private String name;
	private List<Oeuvre> oeuvrePlaylist;

	/**
	 * Crée une nouvelle playlist (jamais créée : pas de fichier json stockant des données créé par le passé)
	 * @param name de la playlist
	 * @throws Exception 
	 */
	public Playlist(String name) throws Exception {
		if(name.isEmpty()) {
			throw new Exception("Le nom de la playlist ne doit pas être vide");
		}
		this.name = name;
		this.oeuvrePlaylist = new ArrayList<Oeuvre>();
	}
	
	/**
	 * Crée une playlist à partir du nom en et de toutes les oeuvres contenues spécifiés en paramètre
	 * @param name de la playlist
	 * @param oeuvreList Tableau contenant les oeuvres de la playlist
	 * @throws Exception 
	 */
	public Playlist(String name, List<Oeuvre> oeuvreList) throws Exception {
		if(name.isEmpty()) {
			throw new Exception("Le nom de la playlist ne doit pas être vide");
		}
		this.name = name;
		this.oeuvrePlaylist = new ArrayList(oeuvreList);
	}

	/**
	 * Retour les oeuvres contenues dans la playlist
	 * @return List<Oeuvre>
	 */
	public List<Oeuvre> getCollection() {
		return this.oeuvrePlaylist;
	}
	
	/**
	 * Ajoute une oeuvre à la playlist
	 * @param oeuvre à ajouter
	 */
	public void addPlaylist(Oeuvre oeuvre) throws Exception { //TODO: faire en sorte que ça puisse  ajouter seulement si même type (ex : playlist que de musique ou que de film)
		if(this.search(oeuvre) == null) {
			this.oeuvrePlaylist.add(oeuvre);
			this.addMusicToPlaylistInDB(oeuvre, this.getName());
		}
		else {
			throw new Exception("L'oeuvre est déjà présente dans la collection");
		}
		
	}
	
	/**
	 * Ajoute une liste d'oeuvre à la playlist
	 * @param oeuvreList : liste des oeuvres à ajouter
	 */
	public void addPlaylist(List<Oeuvre> oeuvreList) throws Exception {
		for(Oeuvre oeuvre : oeuvreList) {
			this.addPlaylist(oeuvre);
		}
	}
	
	/**
	 * Recherche une oeuvre dans la playlist
	 * @param oeuvre à rechercher
	 * @return si trouvée : oeuvre recherchée ; sinon null
	 */
	public Oeuvre search(Oeuvre oeuvre) {
		if(this.oeuvrePlaylist.indexOf(oeuvre) != -1) {
			return this.oeuvrePlaylist.get(this.oeuvrePlaylist.indexOf(oeuvre));
		}
		return null;
	}

	/**
	 * Supprime une oeuvre de la playlist
	 * @param oeuvre à supprimer
	 */
	public void deleteOeuvre(Oeuvre oeuvre) {
		if(this.search(oeuvre) != null) {
			this.oeuvrePlaylist.remove(oeuvre);
			this.deleteMusicOfPlaylistInDB(this.getName(), oeuvre);
		}
		else {
			try {
				throw new Exception("Impossible de supprimer l'oeuvre : elle n'est pas dans la playlist");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Retourne le nom de la playlist
	 * @return nom de la playlist
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Change le nom de la playlist
	 * @param playlistName
	 * @throws Exception 
	 */
	public void setName(String playlistName) throws Exception {
		if(playlistName.isEmpty()) {
			throw new Exception("Le nom de la playlist ne doit pas être vide");
		}
		this.setPlaylistNameInDB(this.name, playlistName);
		this.name = playlistName;
	}
	
	public String toString() {
		String stringPlaylist = "";
		for(int i = 0 ; i < this.oeuvrePlaylist.size() ; i++) {
			stringPlaylist += this.oeuvrePlaylist.get(i).getFile().getName();
			if(i != this.oeuvrePlaylist.size()-1) {
				stringPlaylist += " - ";
			}
		}
		return stringPlaylist;
	}

	/**
	 * Crée l'objet playlist et ses musiques contenus à partir de l'id en BD de la playlist puis le met dans la liste des playlist
	 * @param idPlaylist
	 */
	public static Playlist createPlaylistFromDB(int idPlaylist) throws Exception {
		try {
			ResultSet rsPlaylist = DB.getInstance().getStatement().executeQuery("SELECT * FROM collection WHERE id_collection="+idPlaylist+";");
			if(rsPlaylist.isClosed() || rsPlaylist.getString("libelle_collection").equals("")) { //si aucune playlist en BDD avec cette id
				throw new Exception("L'id \"" + idPlaylist + "\" ne correspond à aucune playlist en base de données");
			}

			ResultSet rsMusicsInCollection = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique_collection WHERE id_collection="+idPlaylist+";");
			List<Oeuvre> musicsPlaylist = new ArrayList<>();
			Oeuvre musique;
			Playlist playlist = ListeOeuvres.getInstance().searchPlaylistByName(rsPlaylist.getString("libelle_collection"));
			while(rsMusicsInCollection.next()) { //récupère tous les id des musiques de la playlist
				musique = Music.searchMusicByIdInDB(rsMusicsInCollection.getInt("id_musique"));
				musicsPlaylist.add(musique);
			}
			if(playlist != null) { //si la playlist existe déjà sur l'ordi
				playlist.addPlaylist(musicsPlaylist);
				return playlist;
			}
			else {
				playlist = new Playlist(rsPlaylist.getString("libelle_collection"), musicsPlaylist);
				ListeOeuvres.getInstance().getPlaylistList().add(playlist);
				return playlist;
			}
		}catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Crée l'objet favoris et ses musiques contenus si existe déjà en BD
	 */
	public static Playlist createOrGetFav() throws Exception{
		ResultSet rsPlaylist = DB.getInstance().getStatement().executeQuery("SELECT * FROM collection WHERE libelle_collection='favoris';");
		if(!rsPlaylist.isClosed() && rsPlaylist.getString("libelle_collection").equals("favoris")) {
			List<Oeuvre> musicsPlaylist = new ArrayList<>();
			Oeuvre musique;
			ResultSet rsMusicsInCollection = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique_collection WHERE id_collection="+rsPlaylist.getInt("id_collection")+";");
			while(rsMusicsInCollection.next()) { //récupère tous les id des musiques des fav
				musique = Music.searchMusicByIdInDB(rsMusicsInCollection.getInt("id_musique"));
				musicsPlaylist.add(musique);
			}
				return new Playlist("favoris", musicsPlaylist);
		}
		else {
			DB.getInstance().getStatement().executeUpdate("insert into collection(libelle_collection) values('favoris');");
			return new Playlist("favoris");
		}
	}

	/**
	 * instancie toutes les playlists contenues en BDD puis les met dans la liste des playlists
	 */
	public static void createAllPlaylistFromDB() {
		try {
			ResultSet rsPlaylist = DB.getInstance().getStatement().executeQuery("SELECT * FROM collection;");
			if(rsPlaylist.isClosed()) {
				throw new Exception("Impossible de créer les playlists depuis la BDD");
			}
			else {
				int idPlaylist = 0;
				while(rsPlaylist.next()) {
					if(!"favoris".equals(rsPlaylist.getString("libelle_collection").toLowerCase())) {
						idPlaylist = rsPlaylist.getInt("id_collection");
						Playlist.createPlaylistFromDB(idPlaylist);
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Crée une playliste en BD vide avec le nom spécifié en paramètre
	 * @param playlistName
	 */
	public static void createEmptyPlaylistInDB(String playlistName) {
		try {
			DB db = DB.getInstance();
			if(Playlist.isPlaylistInDB(playlistName) != 0) {
				throw new Exception("La playlist existe déjà en base de données");
			}
			else {
				db.getStatement().executeUpdate("insert into collection(libelle_collection) values('"+playlistName+"');"); //TODO: rajouter la clé étrangère du type de collection
			}
		} catch(SQLException e) {
			System.err.println(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Crée une playlist en BD à partir du fichier passé en paramètre
	 * @throws Exception
	 */
	public void createPlaylistInDB() {
		try {
			int idPlaylist = Playlist.isPlaylistInDB(this.getName());
			if( idPlaylist!= 0) {
				throw new Exception("La playlist existe déjà en base de données");
			}
			else { //La playlist n'existe pas en BD
				DB.getInstance().getStatement().executeUpdate("insert into collection(libelle_collection) values('"+this.getName()+"');");
				idPlaylist = Playlist.isPlaylistInDB(this.getName());
				int idMusic = 0;
				for(Oeuvre oeuvreOfPlaylist : this.oeuvrePlaylist) {
					idMusic = Music.isMusicInDB(oeuvreOfPlaylist.getFile().getAbsolutePath());
					if(idMusic !=0) {
						DB.getInstance().getStatement().executeUpdate("insert into musique_collection(id_musique, id_collection) values("+idMusic+","+idPlaylist+");");
					}
					else {
						System.err.println("La musique \""+oeuvreOfPlaylist.getMetaData().getMusicName() + "\" n'a pas pu être ajoutée à la playlist car elle n'a pas été trouvée en BDD");
					}
				}
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Ajoute une oeuvre à la playlist
	 * @param oeuvre : oeuvre à ajouter à la playlist
	 * @param playlistName : nom de la playlist dans laquelle il faut ajouter l'oeuvre
	 */
	private void addMusicToPlaylistInDB(Oeuvre oeuvre, String playlistName) throws Exception { //TODO: faire en sorte que ça puisse  ajouter seulement si même type (ex : playlist que de musique ou que de film)
		int idMusique = Music.isMusicInDB(oeuvre.getFile().getAbsolutePath());
		if (idMusique != 0) {
			int idPlaylist = Playlist.isPlaylistInDB(playlistName);
			if (idPlaylist != 0) {
				if (Music.isMusicInPlaylistInDB(idMusique, idPlaylist)) {
					throw new Exception("La musique \"" + oeuvre.getFile().getName() + "\" est déjà dans la playslit \"" + playlistName);
				} else {
					DB db = DB.getInstance();
					db.getStatement().executeUpdate("insert into musique_collection(id_musique, id_collection)  values("+idMusique+","+idPlaylist+")");
				}
			}
			else {
				throw new Exception("La playlist \"" + playlistName + "\" n'est pas enregistrée en base de donnée");
			}
		}
		else {
			throw new Exception("La musique \"" + oeuvre.getFile().getName() + "\" n'est pas enregistrée en base de donnée");
		}
	}

	/**
	 * Supprime la playlist en BD
	 * @param playlistName
	 */
	public static void deletePlaylistInDB(String playlistName) {
		try {
			int idPlaylist = Playlist.isPlaylistInDB(playlistName);
			if (idPlaylist != 0) {
				DB.getInstance().getStatement().executeUpdate("DELETE FROM musique_collection WHERE id_collection = " + idPlaylist + ";");
				DB.getInstance().getStatement().executeUpdate("DELETE FROM collection WHERE id_collection = " + idPlaylist + ";");
			}
			else {
				throw new Exception("La playlist \"" + playlistName + "\" n'est pas enregistrée en base de donnée");
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Supprime une oeuvre de la playlist en BD
	 * @param playlistName
	 * @param oeuvre
	 */
	private void deleteMusicOfPlaylistInDB(String playlistName, Oeuvre oeuvre) {
		try {
			int idMusic = Music.isMusicInDB(oeuvre.getFile().getAbsolutePath());
			if (idMusic != 0) {
				int idPlaylist = Playlist.isPlaylistInDB(playlistName);
				if (idPlaylist != 0) {
					if (!Music.isMusicInPlaylistInDB(idMusic, idPlaylist)) {
						throw new Exception("La musique \"" + oeuvre.getMetaData().getMusicName() + "\" n'est pas dans la playslit \"" + playlistName);
					} else {
						DB.getInstance().getStatement().executeUpdate("DELETE FROM musique_collection WHERE id_musique = " + idMusic + " AND id_collection = "+idPlaylist+";");
					}
				}
				else {
					throw new Exception("La playlist \"" + playlistName + "\" n'est pas enregistrée en base de donnée");
				}
			} else{
				throw new Exception("Impossible de supprimer : l'oeuvre " + oeuvre.getMetaData().getMusicName() + " n'est pas enregistrée dans la BD");
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Retourne une liste d'oeuvre contenant toutes les musiques qui sont dans la playlist dont le nom est spécifié en paramètre
	 * @param playlistName
	 * @return List<Oeuvre>
	 */
	public List<Oeuvre> getPlaylistMusicDB(String playlistName) {
		List<Oeuvre> tabMusicInPlaylist = new ArrayList<Oeuvre>();
		try {
			ResultSet rsIdCollection = DB.getInstance().getStatement().executeQuery("SELECT id_collection FROM collection where libelle_collection ='"+playlistName+"';");
			int idCollection = rsIdCollection.getInt("id_collection");
			if(idCollection == 0) {
				throw new Exception("La collection \""+playlistName+"\" n'existe pas en base de données");
			}
			ResultSet rsTabMusicInPlaylist = DB.getInstance().getStatement().executeQuery("SELECT id_musique FROM musique_collection WHERE id_collection ="+idCollection+";");
			ArrayList<Integer> test = new ArrayList<Integer>();
			while(rsTabMusicInPlaylist.next()) {
				test.add(rsTabMusicInPlaylist.getInt("id_musique"));
			}
			for(int idMusic : test) {
				tabMusicInPlaylist.add(Music.searchMusicByIdInDB(idMusic));
			}
			return tabMusicInPlaylist;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}

	/**
	 * Change le nom de la playlist en BD
	 * @param oldPlaylistName
	 * @param newPlaylistName
	 */
	private void setPlaylistNameInDB(String oldPlaylistName, String newPlaylistName) {
		try {
			int idPlaylist = Playlist.isPlaylistInDB(oldPlaylistName);
			if (idPlaylist != 0) {
				DB.getInstance().getStatement().executeUpdate("UPDATE collection SET libelle_collection = '"+ newPlaylistName +"' WHERE id_collection = " + idPlaylist + ";");
			}
			else {
				throw new Exception("Impossible de changer le nom : La playlist \"" + oldPlaylistName + "\" n'est pas enregistrée en base de donnée");
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Vérifie si la playlist est dans la base de données
	 * @param playlistName
	 * @return id de la playlist si existe sinon 0
	 */
	public static int isPlaylistInDB(String playlistName) {
		try {
			DB db = DB.getInstance();
			ResultSet rsPlaylistId = db.getStatement().executeQuery("SELECT id_collection from collection where libelle_collection='" + playlistName + "';"); //TODO: à changer par le nom de l'oeuvre dans les metadata
			// read the result set
			while(rsPlaylistId.next()) {
				return rsPlaylistId.getInt("id_collection");
			}
		} catch(SQLException e) {
			// if the error message is "out of memory",
			// it probably means no database file is found
			System.err.println(e.getMessage());
		}
		return 0;
	}

	/**
	 * Recherche une playlist dans la BDD en fonction de son id.
	 * @param idPlaylist : id de l'oeuvre à rechercher
	 * @return si trouvée : oeuvre recherchée ; sinon null
	 */
	public static Playlist searchPlaylistByIdInDB(int idPlaylist) throws Exception {
		ResultSet rsPlaylist = DB.getInstance().getStatement().executeQuery("SELECT * FROM collection where id_collection = " + idPlaylist + ";");
		if(rsPlaylist.getInt("id_collection") != 0) {
			Playlist playlistSearched = ListeOeuvres.getInstance().searchPlaylistByName(rsPlaylist.getString("libelle_collection"));
			if(playlistSearched != null) {
				return playlistSearched;
			}
			else  {
				return Playlist.createPlaylistFromDB(rsPlaylist.getInt("id_playlist"));
			}
		}
		else {
			throw new Exception("Impossible de trouver la musique ayant comme id :" + idPlaylist + ". Elle n'est pas enregistrée dans la BD");
		}
	}
}