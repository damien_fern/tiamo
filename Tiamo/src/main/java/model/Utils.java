package model;

import java.io.File;
import java.time.LocalTime;

public final class Utils {

    public static String getExtension(File file) {
        String fileName = file.getName();
        return fileName.substring(fileName.lastIndexOf('.') + 1);
    }

    public static String getFileName(File file) {
        return file.getName().substring(0, file.getName().lastIndexOf('.'));
    }

    public static EnumMusic getEnumFromExtensionMusic(String extension) throws Exception {
        EnumMusic[] tabEnum = EnumMusic.values();
        boolean isTypeFound = false;

        for(int i = 0 ; i < tabEnum.length ; i++) { //parcourir les enum pour attriuer en fonction de l'extension du fichier
            if(tabEnum[i].getType().equals(extension)) {
               return tabEnum[i];
            }
        }
        throw new Exception("Type de Musique \""+extension+"\" non trouvé par rapport à l'extension du fichier");
    }

    public static String timeInSecondsToHHmmss(int timeSeconds) {
        try {
            LocalTime timeOfDay = LocalTime.ofSecondOfDay(timeSeconds);
            return timeOfDay.toString();
        } catch(Exception e) {
            e.printStackTrace();
            return "00:00:00";
        }
    }

}
