package model;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MetaDataMusic {
    private List<Album> albumList; //Fait
    private List<EnumMusicKind> musicKindList; //Fait
    private List<Artist> artistList; //Fait
    private int musicDurationSeconds;
    private int modifiedMusicDurationSeconds;
    private String musicName;
    private int startAtInSeconds;
    private int endAtInSeconds;
    private boolean isLiked;

    public MetaDataMusic(String musicName) {
        this.musicName = musicName;
        this.albumList = new AlbumArrayList();
        this.startAtInSeconds = 0;
        this.endAtInSeconds = musicDurationSeconds;
        modifiedMusicDurationSeconds = musicDurationSeconds;
        this.albumList = new ArrayList<Album>(){
            private static final long serialVersionUID = 1L;

            @Override public String toString()
            {
                String stringList = "";
                Iterator<Album> iterator = this.iterator();
                while (iterator.hasNext()) {
                    Album album = iterator.next();
                    stringList += album.getName();
                    //Do stuff
                    if (iterator.hasNext()) {
                        stringList += ", ";
                    }
                }
                return stringList;
            }
        };
        this.musicKindList = new ArrayList<EnumMusicKind>();
        this.artistList = new ArtistArrayList();
        isLiked = false;
    }

    public List<Album> getAlbumList() {
        return albumList;
    }

    private void addAlbumInBDD(Album album) {
        try {
            ResultSet rsMusic = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique WHERE libelle_musique='"+this.musicName+"';");
            ArrayList<Integer> idAlbumFromMusicTab = new ArrayList();
            while(rsMusic.next()) { //Vérif si la musique en question n'a pas déjà l'album
                if(rsMusic.getInt("id_album") != 0) { //A mettre chez les autre
                    idAlbumFromMusicTab.add(rsMusic.getInt("id_album"));
                }
            }
            ResultSet rsAlbum = null;
            for(int idAlbumFromMusic : idAlbumFromMusicTab) { // Si la musique en question a déjà un artiste d'associé
                rsAlbum = DB.getInstance().getStatement().executeQuery("SELECT * FROM album WHERE id_album="+idAlbumFromMusic+";");
                if(!rsAlbum.isClosed() && album.getName().equals(rsAlbum.getString("nom_album"))) {
                    return;
                }
            }
            int idAlbum = album.createInDBIfNotExist();
            DB.getInstance().getStatement().executeUpdate("UPDATE musique SET id_album = " + idAlbum + " WHERE libelle_musique='"+this.musicName+"';");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addAlbum(Album album) {
        if(!this.albumList.contains(album)) {
            this.albumList.add(album);
            try {
                Music.getIdMusicInBDD(this.getMusicName());
                try {
                    this.addAlbumInBDD(album);
                }catch(Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {}
        }
    }

    private void removeAlbumInBDD(Album album) {
        try {
            ResultSet rsMusic = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique WHERE libelle_musique='"+this.musicName+"';");
            ArrayList<Integer> idAlbumFromMusicTab = new ArrayList();
            while(rsMusic.next()) {
                if(rsMusic.getInt("id_album") != 0) {
                    idAlbumFromMusicTab.add(rsMusic.getInt("id_album"));
                }
            }
            ResultSet rsAlbum = null;
            for(int idAlbumFromMusic : idAlbumFromMusicTab) {
                rsAlbum = DB.getInstance().getStatement().executeQuery("SELECT * FROM album WHERE id_album="+idAlbumFromMusic+";");
                if(!rsAlbum.isClosed() && album.getName().equals(rsAlbum.getString("nom_album"))) {
                    DB.getInstance().getStatement().executeUpdate("UPDATE musique SET id_album = NULL WHERE libelle_musique='"+this.musicName+"';");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeAlbum(Album album) {
        this.albumList.remove(album);
        try {
            Music.getIdMusicInBDD(this.getMusicName());
            try{
                this.removeAlbumInBDD(album);
            }catch(Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {}
    }

    public void removeAlbum() {
        if(!this.getAlbumList().isEmpty()) {
            try {
                Music.getIdMusicInBDD(this.getMusicName());
                try {
                    this.removeAlbumInBDD(this.getAlbumList().get(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
            }
            this.albumList.clear();
        }
    }

    public List<EnumMusicKind> getMusicKindList() {
        return musicKindList;
    }

    private void addMusicKindInBDD(EnumMusicKind musicKind) throws Exception {
        ArrayList<Integer> idGenreTab = new ArrayList<>();
        try { //Recherche le genre en BDD
            ResultSet rsGenre = DB.getInstance().getStatement().executeQuery("SELECT * FROM genre WHERE libelle_genre='" + musicKind.toString() + "';");
            while (rsGenre.next()) {
                idGenreTab.add(rsGenre.getInt("id_genre"));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        if(idGenreTab.isEmpty()) { //si jamais le genre n'existe pas en BD --> le créer
            try {

                DB.getInstance().getStatement().executeUpdate("INSERT INTO genre (libelle_genre) VALUES ('" + musicKind.toString() + "')");
                ResultSet rsGenre = DB.getInstance().getStatement().executeQuery("SELECT * FROM genre WHERE libelle_genre='" + musicKind.toString() + "';");
                while (rsGenre.next()) {
                    idGenreTab.add(rsGenre.getInt("id_genre"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new Exception("Impossible d'ajouter en BDD le genre \""+musicKind.toString()+"\" pour la musique \""+this.getMusicName()+"\" : impossible de créer le nouveau genre en BDD");
            }
        }
        ArrayList<Integer> idMusicTab = new ArrayList<>();
        try {//Recherche la musique pour laquelle il faut ajouter le genre
            ResultSet rsIdMusic = DB.getInstance().getStatement().executeQuery("SELECT id_musique FROM musique WHERE libelle_musique='" + this.musicName + "';");
            while (rsIdMusic.next()) {
                idMusicTab.add(rsIdMusic.getInt("id_musique"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(idMusicTab.isEmpty()) {
            throw new Exception("Impossible d'ajouter en BDD le genre \""+musicKind.toString()+"\" pour la musique \""+this.getMusicName()+"\" car la musique n'est pas trouvée");
        }
        ResultSet rsMusicKind = null;
        //Recherche si une musique est déjà liée à ce genre
        for(int idMusic : idMusicTab) {
            for(int idGenre : idGenreTab) {
                try {
                    rsMusicKind = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique_genre WHERE id_musique=" + idMusic + " AND id_genre=" + idGenre + ";");
                    while (rsMusicKind.next()) { //si jamais le genre est déjà associé à la table on return
                        return;
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    throw new Exception("Impossible d'ajouter en BDD le genre \""+musicKind.toString()+"\" pour la musique \""+this.getMusicName()+"\" : " + e.getMessage());
                }
            }
        }
        try {
            DB.getInstance().getStatement().executeUpdate("INSERT INTO musique_genre(id_musique, id_genre) VALUES (" + idMusicTab.get(0) + "," + idGenreTab.get(0) + ");");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addMusicKindInBDD(EnumMusicKind musicKind, int idMusic) throws Exception {
        ResultSet rsMusic = DB.getInstance().getStatement().executeQuery("SELECT id_musique FROM musique WHERE id_musique ="+idMusic);
        if(rsMusic.isClosed() || rsMusic.getInt("id_musique") == 0) {
            throw new Exception("La musique à laquelle il faut ajouter le genre n'existe pas en BDD");
        }
        ArrayList<Integer> idGenreTab = new ArrayList<>();
        try { //Recherche le genre en BDD
            ResultSet rsGenre = DB.getInstance().getStatement().executeQuery("SELECT * FROM genre WHERE libelle_genre='" + musicKind.toString() + "';");
            while (rsGenre.next()) {
                idGenreTab.add(rsGenre.getInt("id_genre"));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        if(idGenreTab.isEmpty()) { //si jamais le genre n'existe pas en BD --> le créer
            try {
                DB.getInstance().getStatement().executeUpdate("INSERT INTO genre (libelle_genre) VALUES ('" + musicKind.toString() + "')");
                ResultSet rsGenre = DB.getInstance().getStatement().executeQuery("SELECT * FROM genre WHERE libelle_genre='" + musicKind.toString() + "';");
                while (rsGenre.next()) {
                    idGenreTab.add(rsGenre.getInt("id_genre"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new Exception("Impossible d'ajouter en BDD le genre \""+musicKind.toString()+"\" pour la musique d'id \""+idMusic+"\" : impossible de créer le nouveau genre en BDD");
            }
        }
        ResultSet rsMusicKind = null;
        //Recherche si une musique est déjà liée à ce genre
        for(int idGenre : idGenreTab) {
            try {
                rsMusicKind = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique_genre WHERE id_musique=" + idMusic + " AND id_genre=" + idGenre + ";");
                while (rsMusicKind.next()) { //si jamais le genre est déjà associé à la table on return
                    return;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new Exception("Impossible d'ajouter en BDD le genre \""+musicKind.toString()+"\" pour la musique d'id \""+idMusic+"\" : " + e.getMessage());
            }
        }
        try {
            DB.getInstance().getStatement().executeUpdate("INSERT INTO musique_genre(id_musique, id_genre) VALUES (" + idMusic + "," + idGenreTab.get(0) + ");");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addMusicKind(EnumMusicKind musicKind) {
        if(!this.musicKindList.contains(musicKind)) {
            this.musicKindList.add(musicKind);
            try {
                Music.getIdMusicInBDD(this.getMusicName());
                try {
                    this.addMusicKindInBDD(musicKind);
                }catch(Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {}
        }
    }

    private void removeMusicKindInBDD(EnumMusicKind musicKind) throws Exception {
        ArrayList<Integer> idGenreTab = new ArrayList<>();
        try { //Recherche le genre en BDD
            ResultSet rsGenre = DB.getInstance().getStatement().executeQuery("SELECT * FROM genre WHERE libelle_genre='" + musicKind.toString() + "';");
            while (rsGenre.next()) {
                idGenreTab.add(rsGenre.getInt("id_genre"));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        if(idGenreTab.isEmpty()) { //si jamais le genre n'existe pas en BD --> on throw une exception
            throw new Exception("Impossible de supprimer en BDD le genre \""+musicKind.toString()+"\" pour la musique \""+this.getMusicName()+"\" : ce genre n'est pas créé en BDD");
        }
        ArrayList<Integer> idMusicTab = new ArrayList<>();
        try {//Recherche la musique pour laquelle il faut ajouter le genre
            ResultSet rsIdMusic = DB.getInstance().getStatement().executeQuery("SELECT id_musique FROM musique WHERE libelle_musique='" + this.musicName + "';");
            while (rsIdMusic.next()) {
                idMusicTab.add(rsIdMusic.getInt("id_musique"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(idMusicTab.isEmpty()) {
            throw new Exception("Impossible de supprimer en BDD le genre \""+musicKind.toString()+"\" pour la musique \""+this.getMusicName()+"\" car la musique n'est pas trouvée");
        }
        ResultSet rsMusicKind = null;
        //Recherche si une musique est déjà liée à ce genre --> suppression si oui
        for(int idMusic : idMusicTab) {
            for(int idGenre : idGenreTab) {
                try {
                    DB.getInstance().getStatement().executeUpdate("DELETE FROM musique_genre WHERE id_musique=" + idMusic + " AND id_genre=" + idGenre + ";");
                } catch (SQLException e) {
                    e.printStackTrace();
                    throw new Exception("Impossible de supprimer en BDD le genre \""+musicKind.toString()+"\" pour la musique \""+this.getMusicName()+"\" : " + e.getMessage());
                }
            }
        }
    }

    public void removeAllMusicKindInBDD() throws Exception {
        ArrayList<Integer> idGenreTab = new ArrayList<>();
        try { //Recherche le genre en BDD
            ResultSet rsGenre = DB.getInstance().getStatement().executeQuery("SELECT * FROM genre;");
            while (rsGenre.next()) {
                idGenreTab.add(rsGenre.getInt("id_genre"));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        if(idGenreTab.isEmpty()) { //si jamais le genre n'existe pas en BD --> on throw une exception
            throw new Exception("Impossible de supprimer en BDD le genre pour la musique \""+this.getMusicName()+"\" : ce genre n'est pas créé en BDD");
        }
        ArrayList<Integer> idMusicTab = new ArrayList<>();
        try {
            ResultSet rsIdMusic = DB.getInstance().getStatement().executeQuery("SELECT id_musique FROM musique WHERE libelle_musique='" + this.musicName + "';");
            while (rsIdMusic.next()) {
                idMusicTab.add(rsIdMusic.getInt("id_musique"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(idMusicTab.isEmpty()) {
            throw new Exception("Impossible de supprimer en BDD le genre pour la musique \""+this.getMusicName()+"\" car la musique n'est pas trouvée");
        }
        ResultSet rsMusicKind = null;
        //Recherche si une musique est déjà liée à ce genre --> suppression si oui
        for(int idMusic : idMusicTab) {
            for(int idGenre : idGenreTab) {
                try {
                    DB.getInstance().getStatement().executeUpdate("DELETE FROM musique_genre WHERE id_musique=" + idMusic + " AND id_genre=" + idGenre + ";");
                } catch (SQLException e) {
                    e.printStackTrace();
                    throw new Exception("Impossible de supprimer en BDD le genre pour la musique \""+this.getMusicName()+"\" : " + e.getMessage());
                }
            }
        }
    }

    public void removeMusicKind(EnumMusicKind musicKind) {
        this.musicKindList.remove(musicKind);
        try {
            Music.getIdMusicInBDD(this.getMusicName());
            try{
                this.removeMusicKindInBDD(musicKind);
            }catch(Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {}
    }

    public void removeMusicKind() {
        if(!this.musicKindList.isEmpty()) {
            try {
                Music.getIdMusicInBDD(this.getMusicName());
                try {
                    this.removeAllMusicKindInBDD();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
            }
        }
        this.musicKindList.clear();
    }

    public List<Artist> getArtistList() {
        return artistList;
    }

    private void addArtistInBDD(Artist artist) {
        try {
            ResultSet rsMusic = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique WHERE libelle_musique='"+this.musicName+"';"); //Selectionne la musique courrante
            ArrayList<Integer> idArtistFromMusicTab = new ArrayList();
            while(rsMusic.next()) {
                if(rsMusic.getInt("id_artiste") != 0) { //Selectionne l'artiste de la musique courrante
                    idArtistFromMusicTab.add(rsMusic.getInt("id_artiste"));
                }
            }
            ResultSet rsArtist = null;
            for(int idArtistFromMusic : idArtistFromMusicTab) { //Musique qui à ce nom et qui a un artiste
                rsArtist = DB.getInstance().getStatement().executeQuery("SELECT * FROM artiste WHERE id_artiste="+idArtistFromMusic+";");
                if(!rsArtist.isClosed() && artist.getName().equals(rsArtist.getString("nom_artiste"))) {
                    return;
                }
            }
            int idArtist = artist.createInDBIfNotExist();
            DB.getInstance().getStatement().executeUpdate("UPDATE musique SET id_artiste = " + idArtist + " WHERE libelle_musique='"+this.musicName+"';");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addArtist(Artist artist) {
        if(!this.artistList.contains(artist)) {
            this.artistList.add(artist);
            try {
                Music.getIdMusicInBDD(this.getMusicName());
                try {
                   this.addArtistInBDD(artist);
                }catch(Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {}
        }
    }

    private void removeArtistInBDD(Artist artist) {
        try {
            ResultSet rsMusic = DB.getInstance().getStatement().executeQuery("SELECT * FROM musique WHERE libelle_musique='"+this.musicName+"';");
            ArrayList<Integer> idArtistFromMusicTab = new ArrayList();
            while(rsMusic.next()) {
                if(rsMusic.getInt("id_artiste") != 0) {
                    idArtistFromMusicTab.add(rsMusic.getInt("id_artiste"));
                }
            }
            ResultSet rsArtist = null;
            for(int idArtistFromMusic : idArtistFromMusicTab) {
                rsArtist = DB.getInstance().getStatement().executeQuery("SELECT * FROM artiste WHERE id_artiste="+idArtistFromMusic+";");
                if(!rsArtist.isClosed() && artist.getName().equals(rsArtist.getString("nom_artiste"))) {
                    DB.getInstance().getStatement().executeUpdate("UPDATE musique SET id_artiste = NULL WHERE libelle_musique='"+this.musicName+"';");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeArtist(Artist artist) {
        this.artistList.remove(artist);
        try {
            Music.getIdMusicInBDD(this.getMusicName());
            try{
                this.removeArtistInBDD(artist);
            }catch(Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {}
    }

    public void removeArtist() {
        if(!this.getArtistList().isEmpty()) {
            try {
                Music.getIdMusicInBDD(this.getMusicName());
                try {
                    this.removeArtistInBDD(this.getArtistList().get(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
            }
        }
        this.artistList.clear();
    }

    public int getMusicDurationSeconds() {
        return musicDurationSeconds;
    }

    private void modifyMusicDurationSecondsInBDD(int musicDurationSeconds) throws Exception {
        int idMusic = 0;
        try {
            idMusic = Music.getIdMusicInBDD(this.musicName);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier la durée de la musique en BDD : la musique n'existe pas en BDD --> " + e.getMessage());
        }
        try {
            DB.getInstance().getStatement().executeUpdate("UPDATE musique SET duree_musique = "+musicDurationSeconds+" WHERE id_musique = " + idMusic + ";");
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier la durée de la musique en BDD : " + e.getMessage());
        }
    }

    public void setMusicDurationSeconds(int musicDurationSeconds) {
        this.musicDurationSeconds = musicDurationSeconds;
        try {
            Music.getIdMusicInBDD(this.getMusicName());
            try {
                this.modifyMusicDurationSecondsInBDD(musicDurationSeconds);
            }catch(Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {}
        this.setModifiedMusicDurationSeconds(musicDurationSeconds);
    }

    public void setModifiedMusicDurationSeconds(int initialMusicDurationSeconds) {
        this.modifiedMusicDurationSeconds = initialMusicDurationSeconds;
        this.endAtInSeconds = initialMusicDurationSeconds;
        this.startAtInSeconds = 0;
    }

    public int getModifiedMusicDurationSeconds() {
        return modifiedMusicDurationSeconds;
    }

    public String getMusicName() {
        return musicName;
    }

    private void modifyMusicNameInBDD(String oldMusicName, String newMusicName) throws Exception {
        int idMusic = 0;
        try {
            idMusic = Music.getIdMusicInBDD(oldMusicName);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier le nom de la musique en BDD : la musique n'existe pas en BDD --> " + e.getMessage());
        }
        try {
            DB.getInstance().getStatement().executeUpdate("UPDATE musique SET libelle_musique = '"+newMusicName+"' WHERE id_musique = " + idMusic + ";");
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier le nom de la musique en BDD : " + e.getMessage());
        }
    }

    public void setMusicName(String musicName) throws Exception {
        if(!musicName.equals("")) {
            String oldMusicName = this.musicName;
            this.musicName = musicName;
            try {
                Music.getIdMusicInBDD(oldMusicName);
                try{
                    this.modifyMusicNameInBDD(oldMusicName, musicName);
                }catch(Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {}
        }
        else {
            throw new Exception("Le nom ne doit pas être vide !");
        }
    }

    public int getStartAtInSeconds() {
        return startAtInSeconds;
    }

    public void setStartAtInSeconds(int newStartAtInSeconds) throws Exception {
        if(newStartAtInSeconds >= 0 && newStartAtInSeconds <= this.musicDurationSeconds) {
            this.modifyStartAtInSecondsInBDD(newStartAtInSeconds);
            this.startAtInSeconds = newStartAtInSeconds;
            this.modifiedMusicDurationSeconds = this.endAtInSeconds - newStartAtInSeconds;
        }
        else {
            throw new Exception("Le temps saisi auquel la musique doit commencée est non valide");
        }
    }

    public void modifyStartAtInSecondsInBDD(int newStartAtInSeconds) throws Exception {
        int idMusic = 0;
        try {
            idMusic = Music.getIdMusicInBDD(this.musicName);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier le temps auquel la musique doit commencer en BDD : la musique n'existe pas en BDD --> " + e.getMessage());
        }
        try {
            DB.getInstance().getStatement().executeUpdate("UPDATE musique SET startAt_musique = "+newStartAtInSeconds+" WHERE id_musique = " + idMusic + ";");
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier le temps auquel la musique doit commencer en BDD : " + e.getMessage());
        }
    }

    public int getEndAtInSeconds() {
        return endAtInSeconds;
    }

    public void setEndAtInSeconds(int newEndAtInSeconds) throws Exception {
        if(newEndAtInSeconds >= 0 && newEndAtInSeconds <= this.musicDurationSeconds) {
            this.modifyEndAtInSecondsInBDD(newEndAtInSeconds);
            this.endAtInSeconds = newEndAtInSeconds;
            this.modifiedMusicDurationSeconds = newEndAtInSeconds - this.startAtInSeconds;
        }
        else {
            throw new Exception("Le temps saisi auquel la musique doit terminer est non valide");
        }
    }

    private void modifyEndAtInSecondsInBDD(int newEndAtInSeconds) throws Exception {
        int idMusic = 0;
        try {
            idMusic = Music.getIdMusicInBDD(this.musicName);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier le temps auquel la musique doit terminer en BDD : la musique n'existe pas en BDD --> " + e.getMessage());
        }
        try {
            DB.getInstance().getStatement().executeUpdate("UPDATE musique SET endAt_musique = "+newEndAtInSeconds+" WHERE id_musique = " + idMusic + ";");
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier le temps auquel la musique doit terminer en BDD : " + e.getMessage());
        }
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(int liked) throws Exception {
        if(liked == 0) {
            setLiked(false);
        }
        else if(liked == 1) {
            setLiked(true);
        }
        else {
            throw new Exception("La valeur pour savoir si la musique est likée est incorrecte");
        }
    }

    public void setLiked(boolean liked) {
        try {
            setLikedInDB(liked);
            isLiked = liked;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setLikedInDB(boolean liked) throws Exception {
        int idMusic = 0;
        try {
            idMusic = Music.getIdMusicInBDD(this.musicName);
        } catch (Exception e) {
            throw new Exception("Impossible de modifier l'état like de la musique : la musique n'existe pas --> " + e.getMessage());
        }
        try {
            int isLiked = 1;
            if(!liked) {
                isLiked = 0;
            }
            DB.getInstance().getStatement().executeUpdate("UPDATE musique SET isLiked = "+isLiked+" WHERE id_musique = " + idMusic + ";");
        } catch (SQLException e) {
            throw new Exception("Impossible de modifier l'état like de la musique en BDD : " + e.getMessage());
        }    }

    public String toString() {
        String tostring = this.getMusicName() + " - " + this.getMusicDurationSeconds() + "s";
        for(Artist artist : this.getArtistList()) {
            tostring += "\n\t- "+artist.toString();
        }
        for(Album album : this.getAlbumList()) {
            tostring += "\n\t- "+album.toString();
        }
        tostring+="\n\t- ";
        for(EnumMusicKind emk : this.getMusicKindList()) {
            tostring += emk.toString() + " - ";
        }
        return tostring;
    }

}
