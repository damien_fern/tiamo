package model;

/**
 * Tous les status de lecture d'une oeuvre
 * @author Flo
 *
 */
public enum PlayerState {

	PLAY,PAUSE,STOP,RESUME;
}
