package model;

import javazoom.jl.decoder.JavaLayerException;
import model.mp3Player.StreamPlayerException;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 
 * @author Flo
 * Sert à lancer la musique en fonction du type. Donc autant de FileStrategy que d'extension de musique
 */
public interface FileStrategy {

	/**
	 * Lance la lecture de l'oeuvre
	 */
	void play() throws Exception;

	/**
	 * Joue la musique mp3 au temps spécifié
	 */
	public void play(int startInSeconds, int musicDurationSeconds) throws Exception;

	/**
	 * Reprend la lecture de l'oeuvre
	 */
	void resume();

	/**
	 * Met pause à l'oeuvre
	 */
	void pause();

	/**
	 * Set the video to the number of seconds specified in param
	 * @param timeToGoToInSeconds
	 * @param musicDurationSeconds
	 * @throws StreamPlayerException
	 */
	void goTo(int timeToGoToInSeconds, int musicDurationSeconds) throws Exception;

	/**
	 * Stop l'oeuvre
	 */
	void stop();

	/**
	 * return true if the music is finished
	 * @return isFinished
	 */
	public boolean isFinished();

	/**
	 * return true if the music is playing
	 * @return boolean
	 */
	public boolean isPlayling();

	/**
	 * return true if the music is paused
	 * @return boolean
	 */
	public boolean isPaused();

	/**
	 * Fichier à lire
	 * @param file
	 */
	void setFile(File file) throws FileNotFoundException, JavaLayerException;
	
	String toString();

	/**
	 * Défini le volume de lecture de l'oeuvre
	 * @param vgain
	 */
	void setVolume(double vgain);

	/**
	 * Retourne le volume de lecture maximum
	 */
	float getMaxVolume() throws Exception;

	/**
	 * Retourne le volume de lecture minimum
	 */
	float getMinimumVolume() throws Exception;

	int getMusicDurationInSeconds() throws UnsupportedAudioFileException, IOException;

	public int getCurrentTimeSeconds(int musicDurationSeconds);
}
