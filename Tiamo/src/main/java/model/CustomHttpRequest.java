package model;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class CustomHttpRequest {

    private HttpURLConnection con;
    private URL url;
    private final String URL = "http://tiamo.benjamin.lahouze.com";

    public CustomHttpRequest() {}

    private void init(String urlToPoint){
       try {
           url = new URL(urlToPoint);
           this.con = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JSONObject httpGet(APIEndPoint endPoint)
    {
        return httpGet(endPoint, new HashMap<String, String>());
    }
    public JSONObject httpGet(APIEndPoint endPoint, Map<String, String> headerHTTP)
    {
        return httpGet(endPoint, headerHTTP, "");
    }
    public JSONObject httpGet(APIEndPoint endPoint, Map<String, String> headerHTTP, int idEntity){
        return httpGet(endPoint, headerHTTP, ""+idEntity);
    }

    public JSONObject httpGet(APIEndPoint endPoint, Map<String, String> headerHTTP, String lastString){
        JSONObject json = new JSONObject();
        try {

            String urlTo = URL + endPoint.toString();
            if(lastString != ""){
                urlTo+="/"+lastString;
            }
            init(urlTo);
            if(!endPoint.getMethod().equals("GET"))
            {
                throw new Exception("endPoint not a GET METHOD");
            }
            con.setRequestMethod("GET");
            if(!headerHTTP.isEmpty())
            {
                System.out.println("header HTTP not empty");
                headerHTTP.forEach(
                        (headerTitle, headerValue) ->
                        {
                            con.setRequestProperty(headerTitle, headerValue);
                        }
                );
            }
            json = getData();
            System.out.println(json);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            con.disconnect();
        }
        return json;
    }

    public JSONObject httpPost(APIEndPoint endPoint)
    {
        return httpPost(endPoint, new HashMap<String, String>());
    }
    public JSONObject httpPost(APIEndPoint endPoint, Map<String, String> headerHTTP)
    {
        return httpPost(endPoint, headerHTTP, 0);
    }
    public JSONObject httpPost(APIEndPoint endPoint, Map<String, String> headerHTTP, int idEntity){
        JSONObject json = new JSONObject();
        try {

            String urlTo = URL + endPoint.toString();
            if(idEntity != 0)
            {
                urlTo += "/"+idEntity;
            }
            init(urlTo);
            if(!endPoint.getMethod().equals("POST"))
            {
                throw new Exception("endPoint not a POST METHOD");
            }
            con.setRequestMethod("POST");
            if(!headerHTTP.isEmpty())
            {
                System.out.println("header HTTP not empty");
                headerHTTP.forEach(
                        (headerTitle, headerValue) ->
                        {
                            con.setRequestProperty(headerTitle, headerValue);
                        }
                );
            }
            json = getData();
            System.out.println(json);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            con.disconnect();
        }
        return json;
    }

    public JSONObject httpPost(APIEndPoint endPoint, Map<String, String> headerHTTP, String lastString){
        JSONObject json = new JSONObject();
        try {

            String urlTo = URL + endPoint.toString()+"/"+lastString;
            init(urlTo);
            if(!endPoint.getMethod().equals("POST"))
            {
                throw new Exception("endPoint not a POST METHOD");
            }
            con.setRequestMethod("POST");
            if(!headerHTTP.isEmpty())
            {
                System.out.println("header HTTP not empty");
                headerHTTP.forEach(
                        (headerTitle, headerValue) ->
                        {
                            con.setRequestProperty(headerTitle, headerValue);
                        }
                );
            }
            json = getData();
            System.out.println(json);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            con.disconnect();
        }
        return json;
    }

    public String httpGetNoJson(APIEndPoint endPoint, Map<String, String> headerHTTP){
        String string = new String();
        try {


            String urlTo = URL + endPoint.toString();
            init(urlTo);
            if(!endPoint.getMethod().equals("GET"))
            {
                throw new Exception("endPoint not a GET METHOD");
            }
            con.setRequestMethod("GET");
            if(!headerHTTP.isEmpty())
            {
                System.out.println("header HTTP not empty");
                headerHTTP.forEach(
                        (headerTitle, headerValue) ->
                        {
                            con.setRequestProperty(headerTitle, headerValue);
                        }
                );
            }
            string = getDataNoJson();
            System.out.println(string);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            con.disconnect();
        }
        return string;
    }

    private String getDataNoJson(){

        StringBuilder response = new StringBuilder();

        try {
            int responseCode = con.getResponseCode();
            InputStream inputStream;
            if (200 <= responseCode && responseCode <= 299) {
                inputStream = con.getInputStream();
            } else {
                inputStream = con.getErrorStream();
            }

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            inputStream));

            String currentLine;

            while ((currentLine = in.readLine()) != null)
                response.append(currentLine);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

        String string = response.toString().replaceAll("^\"|\"$", "");

        return string;
    }

    private JSONObject getData(){
        JSONObject json;

        StringBuilder response = new StringBuilder();

        try {
            int responseCode = con.getResponseCode();
            InputStream inputStream;
                if (200 <= responseCode && responseCode <= 299) {
                inputStream = con.getInputStream();
            } else {
                inputStream = con.getErrorStream();
            }

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            inputStream));

            String currentLine;

            while ((currentLine = in.readLine()) != null)
                response.append(currentLine);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

        String string = response.toString().replaceAll("^\"|\"$", "");

        json = new JSONObject(string);

        try {
            json.put("code", con.getResponseCode());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }
}