package model;


public enum APIEndPoint {
    ROOT("/", "GET"),
    CREATE_USER("/user", "POST"),
    GET_USER("/user/profile", "GET"),
    NEW_FRIENDSHIP("/user/befriend", "POST"),
    GET_FRIENDSHIPS("/user/friends", "GET"),
    CREATE_TOKEN("/user/token", "GET"),
    LIKE_MUSIC("/music/like" , "POST"),
    UNLIKE_MUSIC("/music/unlike", "POST"),
    GET_MUSIC_INFO("/music", "GET"),
    GET_MUSIC_ID("/music/search", "GET");

    private final String toString;
    private final String method;

    private APIEndPoint(String path, String method) {
        this.toString = path;
        this.method = method;
    }

    public String getMethod() throws Exception {
        if(
                method != "GET"
                && method != "POST"
                && method != "PUT"
                && method != "DELETE"
        ){
            throw new Exception("Pas une méthode HTTP");
        }
        else
        {
            return method;
        }

    }

    public String toString(){
        return toString;
    }
}
