package model;

/**
 * 
 * @author Flo
 * Pile pour stocker les musiques lues
 */
public class FixedOeuvreStack
{
    private Oeuvre[] stack;
    private int indexMax;
    private int top;

    public FixedOeuvreStack(int size) throws Exception
    {
    	if(size < 1) {
    		throw new Exception("La taille de la pile doit-être supérieure à 0");
    	}
        this.stack = new Oeuvre[size];
        this.top = -1;
        this.indexMax = size-1;
    }

    /**
     * Ajoute l'oeuvre dans la pile
     * @param oeuvre à ajouter à la pile
     */
    public void push(Oeuvre oeuvre)
    {
    	if(oeuvre == null) {
    		throw new NullPointerException("Impossible d'ajouter une oeuvre null");
    	}
    	top +=1;
        if (top > indexMax) {
    		Oeuvre[] newStack = new Oeuvre[indexMax+1];
    		if(indexMax > 1) {
	            for(int i = 1 ; i < top ; i++) {
	            	newStack[i-1] = stack[i];
	            }
    		}
    		this.stack = null;
    		this.stack = newStack;
    		top--;
        }
        stack[top] = oeuvre;
    }

    /**
     * Enlève la dernière oeuvre ajoutée à la pile et la return
     * @return Oeuvre : Dernière oeuvre ajoutée
     */
    public Oeuvre pop()
    {
        if (top < 0) throw new IndexOutOfBoundsException("La pile est vide");
        Oeuvre obj = stack[top--];
        stack[top + 1] = null;
        return obj;
    }

    /**
     * Retourne le dernier index de la pile : taille max de la pile - 1
     * @return int : Dernier index de la pile
     */
    public int getIndexMax()
    {
        return indexMax;
    }

    /**
     * Retourne le nombre d'éléments présents dans la pile
     * @return int : nombre éléments dans la pile
     */
    public int nbElements()
    {
        return top + 1;
    }
    
    /**
     * Retourne la dernière oeuvre ajoutée à la pile. Si aucune oeuvre, une exception est levée
     * @return Oeuvre : dernier oeuvre de la pile
     */
    public Oeuvre getLastOeuvre() throws Exception { //TODO: vérifier de partout où utilisé
    	if(this.top > -1) {
    		return this.stack[this.top];
    	}
		throw new Exception("Impossible de retourner la dernière oeuvre, la pile est vide");

    }

    /**
     * Vide le tableau
     */
    public void clearTab() {
        for(int i = 0 ; i < nbElements() ; i++) {
            pop();
        }
    }

    public String toString() {
        String oeuvreInStack = "";
        for(int i = 0 ; i < this.nbElements() ; i++) {
            oeuvreInStack += i + ": \"" + this.stack[i].getFile().getName() + "\"";
            if(i < this.nbElements()-1) {
                oeuvreInStack += " - ";
            }
        }
        return oeuvreInStack;
    }
}
