package view;

import eu.hansolo.enzo.notification.Notification;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

public class TiamoApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        SearchOeuvres so = SearchOeuvres.getInstance();
        try {
            //Music.updateMusic();
            Playlist.createAllPlaylistFromDB();
        } catch (Exception e) {
            System.out.println("PAS DE REPERTOIRE");
            //e.printStackTrace();
        }

        Parent root = null;

        URL url = null;
        try {
            String sceneFile = "/fxml/global_window.fxml";
            url = getClass().getResource( sceneFile );
            root = FXMLLoader.load( url );
        } catch (IOException e) {
            throw e;
        }
        primaryStage.initStyle(StageStyle.DECORATED);
        primaryStage.setTitle("TIAMO - TIAMO Is Another Media Organizer");
        primaryStage.setScene(new Scene(root, 900, 600));
        primaryStage.getIcons().add(new Image(new FileInputStream("Tiamo/src/main/resources/images/Squirrel.png")));
        primaryStage.show();
        Notification.Notifier.setPopupLocation(primaryStage, Pos.TOP_RIGHT);
        Notification.Notifier.setWidth(401);
        Notification.Notifier.setOffsetX(8);
        Notification.Notifier.setOffsetY(32);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("Stage is closing");
        MusicPlayer.getInstance().stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
